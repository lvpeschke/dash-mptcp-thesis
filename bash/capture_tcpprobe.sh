#!/usr/bin/env bash
# Get /proc/net/tcpprobe output to logfile
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

PORT=$1  # port number to listen on
FULL=1  # 1 for every ack, 0 for cwnd change
# BUFSIZE # log buffer size in packets
LOGFILE=$2

sudo modprobe tcp_probe port=${PORT} full=${FULL}  # hook tcpprobe
sudo chmod 444 /proc/net/tcpprobe  # make tcpprobe output readable
sudo cat /proc/net/tcpprobe > ${LOGFILE} &  # redirect tcpprobe to file
echo $!  # return PID of cat process
