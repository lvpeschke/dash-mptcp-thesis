#!/usr/bin/env bash
#
# Start the VMs from the host.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

hostname=`hostname -s`
case ${hostname} in
    barebone*) : ;;
    *) echo "Unknown hostname"; exit 1 ;;
esac


client_name=NewClient
server_name=NewServer
rival_name=RivalClient

function start() {
    set -x

    vboxmanage startvm $1 --type headless

    { set +x; } 2>/dev/null
}


if [[ $# -eq 0 ]] ; then
    echo "Choose which VM to start: client, server, rival."
    exit 1
fi

machines=$@
for m in ${machines}
do
    case ${m} in
        client) start ${client_name} ;;
        server) start ${server_name} ;;
        rival)  start ${rival_name} ;;
        basic)  start ${client_name} ; start ${server_name} ;;
        all)    start ${client_name} ; start ${server_name} ; start ${rival_name} ; break ;;
        *)      echo "Usage `basename $0` {client | server | rival | all | basic}" ; exit 1 ;;
    esac
done

sleep 2
set -x
vboxmanage list runningvms
