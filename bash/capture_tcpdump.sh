#!/usr/bin/env bash
# Run tcpdump as a background process
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

KWARGS="$@"  # all arguments for tcpdump

nohup sudo /usr/sbin/tcpdump ${KWARGS} &>/dev/null &
PID=$!  # remember tcpdump PID
sleep 5  # give tcpdump time to launch
echo ${PID}  # return PID
