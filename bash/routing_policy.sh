#!/usr/bin/env bash
#
# Configure the routing policy with two interfaces on the VMs.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

hostname=`hostname -s`
case ${hostname} in
    newclient*)	    id=24; server=1 ;;
    newserver*)	    id=42; server=1 ;;
    rivalclient*)	id=60; server=1 ;;  # ADDED
    *) echo "Unknown hostname"; exit 1 ;;
esac

function flush_tcp_metrics() {
    set -x

    ip tcp_metrics flush

    { set +x; } 2>/dev/null
}

function up() {
    set -x

    # INTERFACES
    # Bridge 11
    ip link set up dev eth1
    ip addr add 10.0.11.${id}/24 dev eth1
    #ip route add 10.0.11.1 dev eth1

    # Bridge 22
    ip link set up dev eth2
    ip addr add 10.0.22.${id}/24 dev eth2
    #ip route add 10.0.22.1 dev eth2

    # ROUTING POLICY
    # This creates two different routing tables, that we use based on the source-address.
    ip rule add from 10.0.11.${id} table 1
    ip rule add from 10.0.22.${id} table 2

    # Configure the two different routing tables
    ip route add 10.0.11.0/24 dev eth1 scope link table 1
    ip route add default via 10.0.11.${server} dev eth1 table 1

    ip route add 10.0.22.0/24 dev eth2 scope link table 2
    ip route add default via 10.0.22.${server} dev eth2 table 2

    # default route for the selection process of normal internet-traffic
    # ip route add default scope global nexthop via 192.168.57.1 dev eth1
    # KEEP THE NAT DEFAULT FOR NORMAL INTERNET TRAFFIC

    { set +x; } 2>/dev/null
}

function down() {
    set -x

    ip link set down dev eth1
    ip addr flush eth1
    ip link set down dev eth2
    ip addr flush eth2

    { set +x; } 2>/dev/null
}

#######################
# --- EXECUTION --- ###
#######################

case $1 in
    up)
        up
        flush_tcp_metrics
        ;;
    down)
        down
        flush_tcp_metrics
        ;;
    *)  echo "Usage: `basename $0` {up | down}"; exit 1 ;;
esac
