#!/usr/bin/env bash
#
# Create two network bridges, add ips, and iptables forwarding entries.
#
# See: https://www.virtualbox.org/manual/ch06.html#network_bridged
# See: https://www.virtualbox.org/wiki/Advanced_Networking_Linux
#
# Copyright (C) 2017  Lena Victoria Peschke
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

hostname=`hostname -s`
case ${hostname} in
    barebone*) : ;;
    *) echo "Unknown hostname"; exit 1 ;;
esac


# Bridge 11
bridge1=vbr1
network11=10.0.11.0/24
h1addr=10.0.11.1
tapc1=tapc1
#c1addrhost=10.0.11.2
c1addr=10.0.11.24
taps1=taps1
s1addr=10.0.11.42
#s1addrhost=10.0.11.4
tapr1=tapr1
r1addr=10.0.11.60

# Bridge 22
bridge2=vbr2
network22=10.0.22.0/24
h2addr=10.0.22.1
tapc2=tapc2
#c2addrhost=10.0.22.2
c2addr=10.0.22.24
taps2=taps2
s2addr=10.0.22.42
#s2addrhost=10.0.22.4
tapr2=tapr2
r2addr=10.0.22.60


function forward() {
    echo "add forwarding 11 <-> 11, 22 <-> 22, 11 <-/-> 22"
    set -x
    iptables -A FORWARD -s ${network11} -d ${network11} -j ACCEPT
    iptables -A FORWARD -s ${network22} -d ${network22} -j ACCEPT
    iptables -A FORWARD -s ${network11} -d ${network22} -j DROP
    iptables -A FORWARD -s ${network22} -d ${network11} -j DROP
}

function unforward() {
    echo "delete forwarding 11 <-> 11, 22 <-> 22, 11 <-/-> 22"
    set -x
    iptables -D FORWARD -s ${network11} -d ${network11} -j ACCEPT
    iptables -D FORWARD -s ${network22} -d ${network22} -j ACCEPT
    iptables -D FORWARD -s ${network11} -d ${network22} -j DROP
    iptables -D FORWARD -s ${network22} -d ${network11} -j DROP
}


function tap() {
    # $1 is tap_name
    echo "creating tap"
    set -x
    ip tuntap add dev $1 mode tap
    ip link set dev $1 up
    { set +x; } 2>/dev/null
}

function untap() {
    # $1 is tap_name
    echo "deleting tap"
    set -x
    ip link set dev $1 down
    ip tuntap del dev $1 mode tap
    # ip link delete $1
    { set +x; } 2>/dev/null
}

function bridge2taps() {
    # $1 is bridge_name, $2 is tap1_name, $3 is tap2_name
    echo "creating a bridge with two taps"
    set -x
    brctl addbr $1
    brctl addif $1 $2
    brctl addif $1 $3
    ip link set dev $1 up
    { set +x; } 2>/dev/null
}

function unbridge2taps() {
    # $1 is bridge_name, $2 is tap1_name, $3 is tap2_name
    echo "deleting a bridge with two taps"
    set -x
    brctl delif $1 $2
    brctl delif $1 $3
    ip link set dev $1 down
    brctl delbr $1
    { set +x; } 2>/dev/null
}

function bridge3taps() {
    # $1 is bridge_name, $2 is tap1_name, $3 is tap2_name, $4 is tap3_name
    echo "creating a bridge with three taps"
    set -x
    brctl addbr $1
    brctl addif $1 $2
    brctl addif $1 $3
    brctl addif $1 $4
    ip link set dev $1 up
    { set +x; } 2>/dev/null
}

function unbridge3taps() {
    # $1 is bridge_name, $2 is tap1_name, $3 is tap2_name, $4 is tap4_name
    echo "deleting a bridge with three taps"
    set -x
    brctl delif $1 $2
    brctl delif $1 $3
    brctl delif $1 $4
    ip link set dev $1 down
    brctl delbr $1
    { set +x; } 2>/dev/null
}

function addressandrouting() {  # CHANGED
    # $1 is bridge_name, $2 is bridge_addr_host
    # $3 is tap1_name, $4 is tap1_addr_guest
    # $5 is tap2_name, $6 is tap2_addr_guest
    # $7 is tap3_name, $8 is tap3_addr_guest  # ADDED
    echo "setting ip addresses and the routing"
    set -x
    # ip addr add $2/24 dev $1
    # ip route add $2 dev $1
    ip route add $4 dev $3
    ip route add $6 dev $5
    ip route add $8 dev $7  # ADDED
    { set +x; } 2>/dev/null
}

function dobridgetaps() {  # CHANGED
    # $1 is bridge_name, $2 is bridge_addr_host
    # $3 is tap1_name, $4 is tap1_addr_guest
    # $5 is tap2_name, $6 is tap2_addr_guest
    # $7 is tap3_name, $8 is tap3_addr_guest  # ADDED
    echo "setting up 2 bridges with 6 taps"  # CHANGED
    tap $3
    tap $5
    tap $7  # ADDED

    # bridge2taps $1 $3 $5
    bridge3taps $1 $3 $5 $7  # CHANGED

    # addressandrouting $1 $2 $3 $4 $5 $6
    addressandrouting $1 $2 $3 $4 $5 $6 $7 $8  # CHANGED
}

function undobridgetaps() {  # CHANGED
    # $1 is bridge_name, $2 is tap1_name, $3 is tap2_name, $4 is tap3_name
    echo "unsetting bridge with three taps"
    # unbridge2taps $1 $2 $3
    unbridge3taps $1 $2 $3 $4
    untap $2
    untap $3
    untap $4
}


#######################
# --- EXECUTION --- ###
#######################

case $1 in
    #bridge)     bridge ;;
    #unbridge)   unbridge ;;
    forward)    forward ;;
    unforward)  unforward ;;
    bridgetaps)
        dobridgetaps ${bridge1} ${h1addr} ${tapc1} ${c1addr} ${taps1} ${s1addr} ${tapr1} ${r1addr}
        dobridgetaps ${bridge2} ${h2addr} ${tapc2} ${c2addr} ${taps2} ${s2addr} ${tapr2} ${r2addr}
        ;;
    unbridgetaps)
	    undobridgetaps ${bridge1} ${tapc1} ${taps1} ${tapr1}
        undobridgetaps ${bridge2} ${tapc2} ${taps2} ${tapr2}
        ;;
    *)          echo "Usage: `basename $0` {bridgetaps | unbridgetaps | forward | unforward}"; exit 1 ;;
esac


# Bridge 11
#h11addr=10.0.11.1
#c11addr=10.0.11.24
#s11addr=10.0.11.42

# Bridge 22
#h22addr=10.0.22.1
#c22addr=10.0.22.24
#s22addr=10.0.22.42

#function tapbridge() {
#    echo "creating bridge"
#    brctl addbr br11
#
#    echo "creating tap"
#    ip tuntap add dev tap1 mode tap  # add user ?
#    ip link set dev tap1 up
#
#    echo "insert tap into bridge"
#    brctl addif br11 tap1
#
#    echo "set the IP address and routing"
#    ip link set up dev br11
#    ip addr add 10.0.11.1/24 dev br0
#    ip route add 10.0.11.0/24 dev br0
#
#    ip link show dev tap1
#
#    # iptables (forwarding & masquerading)
#}


#function bridge() {
#    echo "creating bridge 11, setting ip and route"
#    set -x
#
#    brctl addbr $bridge11
#    ip link set up dev $bridge11
#
#    ip addr add ${h11addr}/24 dev $bridge11
#    ip route add ${network11} dev $bridge11  #  useful?
#
#    ip link show dev $bridge11
#
#    { set +x; } 2>/dev/null
#
#    echo "creating bridge 22, setting ip and route"
#    set -x
#
#    brctl addbr $bridge22
#    ip link set up dev $bridge22
#
#    ip addr add ${h22addr}/24 dev $bridge22
#    ip route add ${network22} dev $bridge22  #  useful?
#
#    ip link show dev $bridge22
#
#    { set +x; } 2>/dev/null
#
#    #iptables (forwarding)
#}

#function unbridge() {
#    echo "deleting bridge 11"
#    set -x
#
#    ip link set down dev $bridge11
#    brctl delbr $bridge11
#
#    { set +x; } 2>/dev/null
#
#    echo "deleting bridge 22"
#    set -x
#
#    ip link set down dev $bridge22
#    brctl delbr $bridge22
#
#    { set +x; } 2>/dev/null
#
#    # iptables (unforwarding)
#}