#!/usr/bin/env bash
# Stop the VMs from the host.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

hostname=`hostname -s`
case ${hostname} in
    barebone*) : ;;
    *) echo "Unknown hostname"; exit 1 ;;
esac


client_name=NewClient
server_name=NewServer
rival_name=RivalClient

function stop() {
    set -x

    vboxmanage controlvm $1 poweroff

    { set +x; } 2>/dev/null
}


if [[ $# -eq 0 ]] ; then
    echo "Choose which VM to stop: client, server, rival."
    exit 1
fi

machines=$@
for m in ${machines}
do
    case ${m} in
        client) stop ${client_name} ;;
        server) stop ${server_name} ;;
        rival)  stop ${rival_name} ;;
        basic)  stop ${client_name} ; stop ${server_name} ;;
        all)    stop ${client_name} ; stop ${server_name} ; stop ${rival_name} ; break ;;
        *)      echo "Usage `basename $0` {client | server | rival | all | basic}" ; exit 1 ;;
    esac
done

set -x
vboxmanage list runningvms
