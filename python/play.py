#!/usr/bin/env python3.5
#
# Script to play a DASH video with VLC.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse  # command line arguments and exception handling
import logging
import os
import subprocess  # spawn new subprocesses and obtain input/output/error pipes
import sys
import time

from xvfbwrapper import Xvfb  # wrapper for running a display inside X virtual frame buffer

import config
import helper
import vlc

description = """
Play a DASH video with VLC inside Xvfb.
Choose between log and timeout mode."""
epilog = """
See also: config.py, vlc.py
"""


def build_vlc_command(url, logic, width, height, verbosity=0, cvlc=True):
    """Create a command to launch VLC with parameter."""

    if cvlc:
        programme = config.CLIENT_CVLC
    else:
        programme = config.CLIENT_VLC
    command = [programme,
               url,
               '--no-loop',
               '--play-and-exit'
               ]
    if logic:  # if none, default is 0 (currently chooses predictive)
        assert logic in vlc.ADAPTIVE_LOGICS
        command.append('--adaptive-logic={}'.format(logic))
    if width:
        assert helper.check_positive_num(width)
        command.append('--adaptive-width={}'.format(width))
    if height:
        assert helper.check_positive_num(height)
        command.append('--adaptive-height={}'.format(height))
    assert verbosity in vlc.VERBOSITY_LEVELS
    command.append('--verbose={}'.format(verbosity))

    return command


def run_vlc_log(datetime,
                url, width, height, logic=vlc.RATE_LOGIC, cvlc=True,
                logfile=None, verbosity=0):
    """Run VLC with one video streamed from a remote server.

    Flags: --adaptive-logic=logic, --no-loop, --play-and-exit
           --adaptive-width=width, --adaptive-height=height
    grep "TFE" from stdout and stderr to file.
    :param datetime: timestamp
    :param url: video url
    :param width: --adaptive-width option value
    :param height: --adaptive-height option value
    :param logic: --adaptive-logic option value
    :param cvlc: choose between cvlc (no interface, default) and vlc
    :param logfile: log file name
    :param verbosity: --verbose option value
    :return: --
    """
    assert helper.check_positive_num(width)
    assert helper.check_positive_num(height)
    assert logic in vlc.ADAPTIVE_LOGICS
    assert verbosity in vlc.VERBOSITY_LEVELS

    logging.info('Start of run_vlc_log, arguments checked...')

    command = build_vlc_command(url=url, logic=logic, width=width, height=height, cvlc=cvlc, verbosity=verbosity)
    logging.info('VLC command... %s', ' '.join(command))

    if logfile is None:
        logfile, _ = name, _ = os.path.splitext(os.path.basename(url))  # get video's basename

    with Xvfb(width=width, height=height):  # start virtual display
        with open('{dt}_{log}{ext}'.format(dt=datetime, log=logfile, ext=helper.DOT_VLCLOG), 'w') as vlclog:
            logging.info('*---*   VLC launched at   %s   *---*', time.strftime('%X %x %Z'))
            p_grep = subprocess.Popen(['grep', 'TFE'], stdin=subprocess.PIPE, stdout=vlclog)
            p_vlc = subprocess.run(command,
                                   stdout=p_grep.stdin, stderr=subprocess.STDOUT,
                                   universal_newlines=True)
            p_vlc.check_returncode()
            logging.info('*---*   VLC exited at   %s   *---*', time.strftime('%X %x %Z'))

    logging.info('End of run_vlc_log...')


def run_vlc_timeout(url, width, height, logic, timeout=30):
    """Run VLC with one video streamed from a remote server until the timeout.

    :param url: video url
    :param width: --adaptive-width option value
    :param height: --adaptive-height option value
    :param logic: --adaptive-logic option value
    :param timeout: when to stop the video
    :return: --
    """
    assert helper.check_positive_num(width)
    assert helper.check_positive_num(height)
    assert logic in vlc.ADAPTIVE_LOGICS
    assert helper.check_positive_num(timeout)

    logging.info('Start of run_vlc_timeout, arguments checked...')

    command = build_vlc_command(url=url, width=width, height=height, logic=logic, cvlc=True, verbosity=0)
    logging.info('VLC command... %s', ' '.join(command))

    with Xvfb(width=width, height=height):  # start virtual display
        logging.info('*---*   VLC (timeout=%d) launched at   %s   *---*', timeout, time.strftime('%X %x %Z'))
        try:
            subprocess.run(command, timeout=timeout, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            logging.info('VLC exited before the end of the timeout...')
        except subprocess.TimeoutExpired as err:
            logging.info(err)
        logging.info('*---*   VLC (timeout=%d) exited at   %s   *---*', timeout, time.strftime('%X %x %Z'))

    logging.info('End of run_vlc_timeout...')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-v', '--version',
                        action='version',
                        version=__file__ + ' version 21 December 2016')
    subparsers = parser.add_subparsers(help='choose to log or to timeout')
    # option 1) log VLC to file...
    parser_log = subparsers.add_parser('log', help='save VLC log to file')
    parser_log.set_defaults(func='log')
    parser_log.add_argument('--datetime',
                            type=str,
                            default=str(time.strftime('%y%m%d-%H%M')),
                            help='timestamp')
    parser_log.add_argument('--file',
                            type=str,
                            help='logfile name (without extension), default="URL base"')
    # option 2) or time out VLC
    parser_timeout = subparsers.add_parser('timeout', help='kill VLC after timeout')
    parser_timeout.set_defaults(func='timeout')
    parser_timeout.add_argument('--time',
                                type=int,
                                default=30,
                                help='playback time before VLC is killed (seconds), default=30')
    # positional & global optional args
    parser.add_argument('url',
                        type=str,
                        help='url of the video')
    parser.add_argument('width',
                        type=int,
                        help='--adaptive-width for VLC')
    parser.add_argument('height',
                        type=int,
                        help='--adaptive-height for VLC')
    parser.add_argument('--logic',
                        type=str,
                        default=vlc.LOGIC_DEFAULT,
                        choices=vlc.ADAPTIVE_LOGICS,
                        help='--adaptive-logic for VLC')
    parser.add_argument('--verbosity',
                        type=int,
                        default=0,
                        choices=vlc.VERBOSITY_LEVELS,
                        help='--verbose for VLC: default 0 (info)')
    args = parser.parse_args()
    logging.info('Call from shell... %s arguments are... %s', __file__, str(args))

    if not helper.check_positive_num(args.width):
        raise ValueError('args.width that is not > 0')
    if not helper.check_positive_num(args.height):
        raise ValueError('args.height that is not > 0')

    if 'func' in args and args.func is 'log':
        sys.exit(run_vlc_log(datetime=args.datetime,
                             url=args.url, width=args.width, height=args.height, logic=args.logic,
                             logfile=args.file, verbosity=args.verbosity))
    elif 'func' in args and args.func is 'timeout':
        if not helper.check_positive_num(args.time):
            raise ValueError('args.time is not > 0')
        sys.exit(run_vlc_timeout(url=args.url, width=args.width, height=args.height, logic=args.logic,
                                 timeout=args.time))
    else:  # should never happen
        raise ValueError('func must be "log" or "timeout"')
