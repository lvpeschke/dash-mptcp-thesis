#!/usr/bin/env python3.5
#
# Script to plot bandwidth estimations of different setups.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import glob
import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import config
import helper
import qoe
import vlc

description = """
Plot bandwidth estimations from different scenarios with the same bandwidth.
Requires files from do_extract_qoe.py to have been created.
"""
epilog = """
See also: config.py, do_extract_qoe.py, qoe.py, vlc.py
"""

# -- LaTeX settings -- #
plt.rc('font', **{'family': 'serif', 'serif': ['Palatino']}) # 'size': 13
plt.rc('text', usetex=True)
plt.rc('legend', fontsize='large')
plt.rc('xtick', labelsize='large')
plt.rc('ytick', labelsize='large')
plt.rc('axes', labelsize='x-large')


def plot_bandwidth_estimation(total_bw, df_qoe: pd.DataFrame, plot_dir, extra, override=False):
    """Plot a bandwidth estimation comparison with all test results in df_qoe."""

    assert isinstance(df_qoe, pd.DataFrame)
    if not os.path.isdir(plot_dir):
        helper.create_folder_in_path(path=os.getcwd(), folder=plot_dir, override=override)

    with helper.cd(plot_dir):
        # Check if files exist
        figure_rel = '{name}_{extra}_rel{ext}'.format(name='qoe_bw_estim', extra=extra, ext=helper.DOT_PDF)
        figure_abs = '{name}_{extra}_abs{ext}'.format(name='qoe_bw_estim', extra=extra, ext=helper.DOT_PDF)
        if not override and any((os.path.isfile(f)) for f in [figure_rel, figure_abs]):
            print('QoE bandwidth estimation file(s) exist(s) and will not be overridden...')
            return

        # Get all mean estimated bandwidths by bw1 and bw2 if mptcp and if not mptcp, not the averages
        df_qoe_Mbps = df_qoe[df_qoe.index != 'average'].copy()
        df_qoe_Mbps['mean bw estimation (Mbps)'] = df_qoe_Mbps['mean bw estimation (bps)'].mul(helper.MICRO)

        # MPTCP
        df_mptcp_on = df_qoe_Mbps[df_qoe_Mbps['mptcp enabled'] == 1]
        # Strong, weak, heavy, light
        df_mptcp_strong = df_mptcp_on[
            np.logical_and(df_mptcp_on['link 1 rate (Mbps)'] >= df_mptcp_on['link 2 rate (Mbps)'],
                           df_mptcp_on['link 1 delay (ms)'] < df_mptcp_on['link 2 delay (ms)'])]
        df_mptcp_weak = df_mptcp_on[
            np.logical_and(df_mptcp_on['link 1 rate (Mbps)'] <= df_mptcp_on['link 2 rate (Mbps)'],
                           df_mptcp_on['link 1 delay (ms)'] > df_mptcp_on['link 2 delay (ms)'])]
        df_mptcp_heavy = df_mptcp_on[
            np.logical_and(df_mptcp_on['link 1 rate (Mbps)'] > df_mptcp_on['link 2 rate (Mbps)'],
                           df_mptcp_on['link 1 delay (ms)'] > df_mptcp_on['link 2 delay (ms)'])]
        df_mptcp_light = df_mptcp_on[
            np.logical_and(df_mptcp_on['link 1 rate (Mbps)'] < df_mptcp_on['link 2 rate (Mbps)'],
                           df_mptcp_on['link 1 delay (ms)'] < df_mptcp_on['link 2 delay (ms)'])]

        # TCP
        df_mptcp_off = df_qoe_Mbps[df_qoe_Mbps['mptcp enabled'] == 0]

        for i in df_mptcp_off.index:
            print(df_mptcp_off.loc[i]['link 1 rate (Mbps)'], df_mptcp_off.loc[i]['link 2 rate (Mbps)'], total_bw)

        if not all(df_mptcp_off.loc[i]['link 1 rate (Mbps)'] == df_mptcp_off.loc[i]['link 2 rate (Mbps)'] == total_bw
                   for i in df_mptcp_off.index):
            raise ValueError(
                    'bandwidths for TCP: link 1 {}, link 2 {}, total {}'.format(df_mptcp_off['link 1 rate (Mbps)'],
                                                                                df_mptcp_off['link 2 rate (Mbps)'],
                                                                                total_bw))

        # Plot pdf to file
        mptcp_color_strong = '#1f78b4'
        mptcp_color_weak = '#33a02c'
        mptcp_color_heavy = '#a6cee3'
        mptcp_color_light = '#b2df8a'
        tcp_color = '#c1494b'  # red plum
        size = 150

        # PERCENTAGE FIG
        plt.figure(1)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.scatter(x=df_mptcp_strong['link 1 rate (Mbps)'] / total_bw,
                   y=df_mptcp_strong['mean bw estimation (Mbps)'] / total_bw,
                   label=r'strong',
                   marker='1', color=mptcp_color_strong, edgecolor=mptcp_color_strong, s=size, clip_on=True)
        ax.scatter(x=df_mptcp_weak['link 1 rate (Mbps)'] / total_bw,
                   y=df_mptcp_weak['mean bw estimation (Mbps)'] / total_bw,
                   label=r'weak',
                   marker='2', color=mptcp_color_weak, edgecolor=mptcp_color_weak, s=size, clip_on=True)
        ax.scatter(x=df_mptcp_heavy['link 1 rate (Mbps)'] / total_bw,
                   y=df_mptcp_heavy['mean bw estimation (Mbps)'] / total_bw,
                   label=r'heavy',
                   marker='3', color=mptcp_color_heavy, edgecolor=mptcp_color_heavy, s=size, clip_on=True)
        ax.scatter(x=df_mptcp_light['link 1 rate (Mbps)'] / total_bw,
                   y=df_mptcp_light['mean bw estimation (Mbps)'] / total_bw,
                   label=r'light',
                   marker='4', color=mptcp_color_light, edgecolor=mptcp_color_light, s=size, clip_on=True)
        for idx, tcp in enumerate(df_mptcp_off['mean bw estimation (Mbps)'] / total_bw):
            if idx == 0:
                label = r'TCP'
            else:
                label = None
            ax.axhline(y=tcp, label=label, color=tcp_color, clip_on=True)

        ax.legend(loc='lower left', frameon=True)
        ax.set_xlabel(r'bandwidth share on the primary link')
        ax.set_ylabel(r'mean estimated bandwidth / configured bandwidth')

        ax.set_xlim(0, 1)
        ax.set_xticks(np.arange(0, 1.1, 0.1))
        ax.set_yticks(np.arange(0, 1.1, 0.05))
        ax.set_ylim(0.5, 0.8)

        ax.grid()
        plt.savefig(figure_rel, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # ABSOLUTE FIG
        plt.figure(2)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.scatter(x=df_mptcp_strong['link 1 rate (Mbps)'] / total_bw,
                   y=df_mptcp_strong['mean bw estimation (Mbps)'],
                   label=r'strong',
                   marker='1', color=mptcp_color_strong, edgecolor=mptcp_color_strong, s=size, clip_on=True)
        ax.scatter(x=df_mptcp_weak['link 1 rate (Mbps)'] / total_bw,
                   y=df_mptcp_weak['mean bw estimation (Mbps)'],
                   label=r'weak',
                   marker='2', color=mptcp_color_weak, edgecolor=mptcp_color_weak, s=size, clip_on=True)
        ax.scatter(x=df_mptcp_heavy['link 1 rate (Mbps)'] / total_bw,
                   y=df_mptcp_heavy['mean bw estimation (Mbps)'],
                   label=r'heavy',
                   marker='3', color=mptcp_color_heavy, edgecolor=mptcp_color_heavy, s=size, clip_on=True)
        ax.scatter(x=df_mptcp_light['link 1 rate (Mbps)'] / total_bw,
                   y=df_mptcp_light['mean bw estimation (Mbps)'],
                   label=r'light',
                   marker='4', color=mptcp_color_light, edgecolor=mptcp_color_light, s=size, clip_on=True)
        for idx, tcp in enumerate(df_mptcp_off['mean bw estimation (Mbps)']):
            if idx == 0:
                label = r'TCP'
            else:
                label = None
            ax.axhline(y=tcp, label=label, color=tcp_color, clip_on=True)

        ax.axhline(y=total_bw, label=r'configured', color='black', clip_on=True)

        ax.legend(loc='best', frameon=True)
        ax.set_xlabel(r'bandwidth share on the primary link')
        ax.set_ylabel(r'mean estimated bandwidth (Mbps)')

        ax.set_xlim(0, 1)
        ax.set_xticks(np.arange(0, 1.1, 0.1))
        ax.set_yticks(np.arange(0, 20, 1))
        ax.set_ylim(5, 12)

        ax.grid()
        plt.savefig(figure_abs, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()


def load_data(rate, delay, video: config.DASHVideo, logic):
    """Load the comparison QoE into a pandas.DataFrame."""

    filename = qoe.QoEComparison.QOE_COMPARISON_RECORD.format(dt='*', video=video.id, logic=logic,
                                                              rate='{:.2f}'.format(rate),
                                                              delay='{:.1f}'.format(delay),
                                                              ext=qoe.QoEComparison.ext)
    fs = glob.glob(filename)
    return pd.DataFrame.from_csv(path=fs[0], header=0)


def main(directory, folder=None, extra=None, rate=None, delay=None, video: config.DASHVideo = None, logic=None,
         override=False):
    # variation is constant
    with helper.cd(directory):
        # load into df
        # '{dt}_qoe_{video}_{logic}_{rate}Mpbs_{delay}ms{ext}'
        folder = directory if folder is None else folder
        if rate is delay is video is logic is None:
            for e, r, d, v in zip(['case1', 'case2', 'case3', 'case4'],
                                  [10, 11.7, 8, 9],
                                  [25, 50, 100, 150],
                                  [config.VIDEO_BBB2_SIMPLE, config.VIDEO_BBB2_SIMPLE,
                                   config.VIDEO_BBB10_SIMPLE, config.VIDEO_BBB10_SIMPLE]):
                df = load_data(rate=r, delay=d, video=v, logic=vlc.LOGIC_DEFAULT)
                plot_bandwidth_estimation(total_bw=r, df_qoe=df, plot_dir=folder, extra=e, override=override)

        else:
            df = load_data(rate=rate, delay=delay, video=video, logic=logic)
            plot_bandwidth_estimation(total_bw=rate, df_qoe=df, plot_dir=folder, extra=extra, override=override)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-v', '--version',
                        action='version',
                        version=__file__ + ' version 5 January 2016')

    parser.add_argument('directory',
                        type=str,
                        help='super-directory where to find the files')
    parser.add_argument('--folder',
                        type=str,
                        help='folder where to save the graphs')
    parser.add_argument('--name',
                        type=str,
                        help='name addition for the graphs')
    parser.add_argument('--rate',
                        type=float,
                        help='baseline bandwidth')
    parser.add_argument('--delay',
                        type=float,
                        help='baseline delay')
    parser.add_argument('--video',
                        type=str,
                        choices=config.ALL_VIDEOS,
                        help='chosen video (see config.py)')
    parser.add_argument('--logic',
                        type=str,
                        choices=vlc.ADAPTIVE_LOGICS,
                        help='choose adaptation logic for VLC')
    parser.add_argument('--override',
                        action='store_true',
                        default=False,
                        help='override already existing QoE comparison file')
    args = parser.parse_args()

    if args:
        args.video = config.ALL_VIDEOS.get(args.video) if args.video else None
        sys.exit(main(directory=args.directory, folder=args.folder,
                      video=args.video, logic=args.logic, rate=args.rate, delay=args.delay,
                      override=args.override))
