#!/usr/bin/env python3.5
#
# Script to automate several calibration executions.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
from time import strftime

import sh

import config
import do_calibration
import do_test
import helper
import vlc

# SETUP HERE
calib_video = config.VIDEO_DEFAULT
assert isinstance(calib_video, config.DASHVideo)
assert calib_video in config.ALL_VIDEOS.values()
calib_logic = vlc.RATE_LOGIC
calib_delays = config.RTT_DELAYS  # list RTT ms
calib_cong_ctrl = 'config.CONG_CTRL_TCP_DEFAULT'
calib_min_Mbps = [21] * 4  # list Mbps
calib_max_Mbps = [50.4] * 4 # list Mbps
calib_samples = 100
calib_repetitions = 2

calib_log = 'calibration-{cong_ctrl}-{delay}ms-{logic}-{dt}.txt'
original_stdout = sys.stdout
original_stderr = sys.stderr
with helper.cd(config.HOST_PATH_TESTS_CALIBRATION):
    do_test.play_and_kill(link=1, video=calib_video, seconds=30, verbose=True)  # avoid swapping
    for delay, min_Mbps, max_Mbps in zip(calib_delays, calib_min_Mbps, calib_max_Mbps):
        filename = calib_log.format(cong_ctrl=calib_cong_ctrl, delay=delay, logic=calib_logic,
                                    dt=str(strftime('%y%m%d-%H%M')))
        folder_pattern = str(strftime('%y%m%d-*ms'))
        with open(filename, 'w') as sys.stdout:
            do_calibration.main(video=calib_video, logic=calib_logic, delay=delay,
                                min_Mbps=min_Mbps, max_Mbps=max_Mbps,
                                samples=calib_samples, repetition=calib_repetitions,
                                cong_ctrl=calib_cong_ctrl,
                                verbose=True)
            sys.stdout.flush()
        sh.ssh(config.CLIENT_SSH, 'sudo', 'rm', '-r', folder_pattern,
               _in='/dev/null', _out='/dev/null', _err='/dev/null')
        sh.ssh(config.SERVER_SSH, 'sudo', 'rm', '-r', folder_pattern,
               _in='/dev/null', _out='/dev/null', _err='/dev/null')
sys.stdout = original_stdout
sys.stderr = original_stderr

print('End of ' + __file__)
