# Parameters to store on each machine per test.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import abc
import contextlib
import datetime
import mmap
import operator
import os
import re
import glob
import logging

import sh

import config
import helper
from vlc import VLC_VERSION

LOG_BEFORE = 'before'
LOG_AFTER = 'after'


class WrongMachineException(Exception):
    pass


def _get_kernel(ubuntu):
    """Get Linux kernel parameters.
    :param ubuntu: if True, get Ubuntu specific info.
    """
    kernel = dict()

    uname = helper.run_command(command='uname -a',
                               err_msg='could not retrieve uname.',
                               must_succeed=True).stdout
    kernel['uname'] = uname

    proc_version = helper.run_command(command='cat /proc/version',
                                      err_msg='could not retrieve /proc/version.',
                                      must_succeed=True).stdout
    kernel['proc-version'] = proc_version

    if ubuntu:
        lsb_release = helper.run_command(command='lsb_release -ds',
                                         err_msg='could not retrieve the lsb release description.',
                                         must_succeed=True).stdout
        kernel['lsb_release'] = lsb_release

        dpkg = helper.run_command(command='dpkg --list | grep "linux-image" | grep "mptcp"',
                                  err_msg='could not retrieve installed kernel.',
                                  must_succeed=True).stdout
        kernel['dpkg-linux-image-mptcp'] = dpkg

        aptitude = helper.run_command(command='aptitude versions linux-mptcp | grep "[[:digit:]]"',
                                      err_msg='could not retrieve aptitude version of linux-mptcp.',
                                      must_succeed=True).stdout
        kernel['aptitude-version_linux-mptcp'] = aptitude

    return kernel


def get_ubuntu_kernel():
    """Get Ubuntu kernel parameters."""
    return _get_kernel(ubuntu=True)


def get_fedora_kernel():
    """Get Fedora kernel parameters."""
    return _get_kernel(ubuntu=False)


def _get_keys_then_values(keys_command, values_command):
    """
    Helper method to get sysctl **.**.*** parameters (first get the keys, then the values).
    :param keys_command: function to get the keys
    :param values_command: funtion to get the values
    :return: a dict of the results
    """
    key_string = helper.run_command(command=keys_command,
                                    err_msg='could not retrieve variable names.',
                                    must_succeed=True).stdout
    keys = key_string.splitlines()
    value_string = helper.run_command(command=values_command,
                                      err_msg='could not retrieve variable values.',
                                      must_succeed=True).stdout
    values = value_string.splitlines(keepends=True)

    if len(keys) != len(values):
        logging.info('CAUTION! _get_keys_then_values found different amount of keys and values')
        logging.info('>>> %d keys found, %d values found <<<', len(keys), len(values))

    return dict(zip(keys, values))


def get_ipv4_kernel_parameters():
    """Get the sysctl net.ipv4 parameters."""
    return _get_keys_then_values(keys_command='sudo sysctl net.ipv4 -N', values_command='sudo sysctl net.ipv4 -n')


def get_mptcp_kernel_parameters():
    """Get the sysctl net.mptcp parameters."""
    return _get_keys_then_values(keys_command='sysctl net.mptcp -N', values_command='sysctl net.mptcp -n')


def get_git_version(directory, entry_key):
    """Get the version of the file in the directory (git commit).

    :param entry_key: key value to use for the return dict entry
    :param directory: git directory
    """
    if directory != config.VM_PATH_SCRIPTS and directory != config.HOST_PATH_SCRIPTS:
        raise ValueError('unrecognised directory.')

    with helper.cd(directory):
        script_version = helper.run_command(command='git log --pretty=format:"%h" -n 1',
                                            err_msg='could not retrieve the git version in {}.'.format(directory),
                                            must_succeed=True).stdout
    return {entry_key: script_version}


def get_vlc_version():
    """Get the version of VLC (git commit)."""

    version = '{vlc} --version 2>&1'.format(vlc=config.CLIENT_VLC)
    vlc_version = helper.run_command(command=version,
                                     err_msg='could not retrieve VLC version.',
                                     must_succeed=True).stdout
    match = re.search(VLC_VERSION, vlc_version)
    if match:
        vlc_version_git = match.group(0)
    else:
        raise RuntimeError('get_vlc_version: did not find a matching VLC version line.')
    return {'vlc-version': vlc_version_git}


def get_apache2_version():
    """Get the version of Apache2."""

    apache2_version = helper.run_command(command='apache2 -v | head -1',
                                         err_msg='could not retrieve apache2 version',
                                         must_succeed=True).stdout
    return {'apache2-version': apache2_version}


def get_apache2_conf():
    """Get the KeepAlive parameters of Apache2."""

    err_msg = 'could not retrieve apache2 configuration'
    timeout = helper.run_command('cat /etc/apache2/apache2.conf | grep "^Timeout "',
                                 err_msg=err_msg,
                                 must_succeed=True).stdout
    ka_enabled = helper.run_command(command='cat /etc/apache2/apache2.conf | grep "^KeepAlive "',
                                    err_msg=err_msg,
                                    must_succeed=True).stdout
    ka_timeout = helper.run_command(command='cat /etc/apache2/apache2.conf | grep "^KeepAliveTimeout "',
                                    err_msg=err_msg,
                                    must_succeed=True).stdout
    max_ka_requests = helper.run_command(command='cat /etc/apache2/apache2.conf | grep "^MaxKeepAliveRequests "',
                                         err_msg=err_msg,
                                         must_succeed=True).stdout
    return {'apache2-timeout': timeout,
            'apache2-keepalive-enabled': ka_enabled,
            'apache2-keepalive-timeout': ka_timeout,
            'apache2-keepalive-maxrequests': max_ka_requests}


def print_ifconfig(filename):
    sh.ifconfig('-a', _out=filename)


def print_ip_tcp_metrics(filename):
    """Print `ip tcp_metrics` into a file."""
    sh.ip('tcp_metrics', _out=filename)


def print_brctl(filename):
    """
    Print bridge info in one file.
    Interfaces are the TAPs from the config file.
    """
    for link in config.ALL_TRIPLE_LINKS.values():
        vbr = link.host_bridge
        with open(filename.format(vbr=vbr), 'a') as file:
            # local function to write to file incrementally
            def write_down(arg):
                file.write(arg)

            sh.brctl('show', vbr, _out=write_down)
            sh.brctl('showmacs', vbr, _out=write_down)
            sh.brctl('showstp', vbr, _out=write_down)


def print_tc_info(filename_qdisc, filename_class, filename_filter):
    """
    Print tc network info in different files per interface and parameter.
    Interfaces are the TAPs from the config file, parameters are: qdisc, class, filter.
    """
    for link in config.ALL_TRIPLE_LINKS.values():
        # for intf in [link.host_tap_client, link.host_tap_server]:
        for intf in link.tap_list:
            what = ['qdisc', 'class', 'filter']
            fs = [f.format(intf=intf) for f in [filename_qdisc, filename_class, filename_filter]]
            [sh.tc(w, 'show', 'dev', intf, _out=f) for w, f in zip(what, fs)]


class AbstractParameters(object):
    """Abstract superclass to store parameters in a log file."""
    __metaclass__ = abc.ABCMeta

    # class attribute
    @abc.abstractproperty
    @property
    def logfile(self):
        return ''

    @abc.abstractmethod
    def create_file(self):
        """Create the log file, failing if the file already exists."""
        if len(self.logfile) == 0:
            raise ValueError('Cannot create a file without a name.')
        f = open(self.logfile, 'x')
        f.close()

    @abc.abstractmethod
    def store_start(self):
        """Store parameters to file before the event."""

    @abc.abstractmethod
    def store_end(self):
        """Store parameters to file after the event."""

    def store(self, parameters, when):
        """Store parameters to file.

        :param parameters: dictionary
        :param when: before or after the test
        """
        assert isinstance(parameters, dict)
        assert when is None or isinstance(when, str)
        # AbstractParameters._store_param_from_dic(filename=self.logfile, keys_and_values=parameters, when=when)
        if not os.path.isfile(self.logfile):
            raise FileNotFoundError('file "{}" does not exist'.format(self.logfile))
        with open(self.logfile, 'a') as fd:
            for key, val in sorted(parameters.items(), key=operator.itemgetter(0)):
                stripped_val = val.rstrip()
                if when:
                    fd.write('{key}, {value}, (when={when})\n'.format(key=key, value=stripped_val, when=when))
                else:
                    fd.write('{key}, {value}\n'.format(key=key, value=stripped_val))


class HostParameters(AbstractParameters):
    """Class to store parameters on the host VM."""

    HOST_LOGFILE = 'parameters_host.log'

    IFCONFIG_LOGFILE = 'parameters_host_ifconfig.log'
    BRCTL_LOGFILE = 'parameters_host_brctl-{vbr}.log'
    TCP_METRICS_LOGFILE_START = 'parameters_host_ip_tcp_metrics_start.log'
    TCP_METRICS_LOGFILE_STOP = 'parameters_host_ip_tcp_metrics_stop.log'
    TC_QDISC_LOGFILE = 'parameters_host_tc-qdisc-{intf}.txt'
    TC_CLASS_LOGFILE = 'parameters_host_tc-class-{intf}.txt'
    TC_FILTER_LOGFILE = 'parameters_host_tc-filter-{intf}.txt'

    @property
    def logfile(self):
        return HostParameters.HOST_LOGFILE

    def create_file(self):
        super(HostParameters, self).create_file()

    def store_start(self):
        p = dict()
        p.update(get_fedora_kernel())
        p.update(get_ipv4_kernel_parameters())
        # No mptcp

        p.update(get_git_version(config.HOST_PATH_SCRIPTS, 'script-version'))  # Version of the scripts (git commit).
        # No vlc git

        super(HostParameters, self).store(parameters=p, when='start')

        # interfaces, bridges
        print_ifconfig(filename=HostParameters.IFCONFIG_LOGFILE)
        print_brctl(filename=HostParameters.BRCTL_LOGFILE)
        print_ip_tcp_metrics(filename=HostParameters.TCP_METRICS_LOGFILE_START)

        # tc parameters in different files
        print_tc_info(filename_qdisc=HostParameters.TC_QDISC_LOGFILE,
                      filename_class=HostParameters.TC_CLASS_LOGFILE,
                      filename_filter=HostParameters.TC_FILTER_LOGFILE)

    def store_end(self):
        print_ip_tcp_metrics(filename=HostParameters.TCP_METRICS_LOGFILE_STOP)


class ClientParameters(AbstractParameters):
    """Class to store parameters on one of the client VMs."""
    # /!\ edited to fit both VMs
    # nodename = os.uname()[1]  # uname -n
    # if nodename != config.CLIENT_HOSTNAME:
    #     raise WrongMachineException('uname -n name is {}'.format(nodename))
    CLIENT_LOGFILE = 'parameters_client_{machine}.log'
    IFCONFIG_LOGFILE = 'parameters_client_{machine}_ifconfig.log'
    TCP_METRICS_LOGFILE_START = 'parameters_client_{machine}_ip_tcp_metrics_start.log'
    TCP_METRICS_LOGFILE_STOP = 'parameters_client_{machine}_ip_tcp_metrics_stop.log'

    PARAM_START = 'start-date'
    PARAM_STOP = 'stop-date'

    def __init__(self, machine):
        self.machine = machine
        self.CLIENT_LOGFILE = ClientParameters.CLIENT_LOGFILE.format(machine=machine)
        self.IFCONFIG_LOGFILE = ClientParameters.IFCONFIG_LOGFILE.format(machine=machine)
        self.TCP_METRICS_LOGFILE_START = ClientParameters.TCP_METRICS_LOGFILE_START.format(machine=machine)
        self.TCP_METRICS_LOGFILE_STOP = ClientParameters.TCP_METRICS_LOGFILE_STOP.format(machine=machine)

    @property
    def logfile(self):
        return self.CLIENT_LOGFILE

    def create_file(self):
        super(ClientParameters, self).create_file()

    def store_start(self):
        p = dict()

        p.update(get_ubuntu_kernel())
        p.update(get_ipv4_kernel_parameters())
        p.update(get_mptcp_kernel_parameters())  # Check whether mptcp is enabled (on the client).

        p.update(get_git_version(config.VM_PATH_SCRIPTS, 'script-version'))  # Version of the scripts (git commit).
        p.update(get_vlc_version())  # Version of VLC (git commit).

        super(ClientParameters, self).store(parameters=p, when='start')

        print_ifconfig(filename=self.IFCONFIG_LOGFILE)
        print_ip_tcp_metrics(filename=self.TCP_METRICS_LOGFILE_START)

    def store_end(self):
        p = dict()

        p.update(get_ipv4_kernel_parameters())
        p.update(get_mptcp_kernel_parameters())

        super(ClientParameters, self).store(parameters=p, when='stop')

        print_ip_tcp_metrics(filename=self.TCP_METRICS_LOGFILE_STOP)

    def store_vlc_before(self, url: str, segment_time: int, width: int, height: int, logic: int):
        """Store parameters right before VLC is launched."""

        if not isinstance(segment_time, (int, str)):
            raise TypeError('segment_size is not an int or str')

        p = dict()
        p.update({'url': url,
                  'segment-size': str(segment_time),
                  'video-width': str(width),
                  'video-height': str(height)})  # video characteristics
        p.update({'adaptation-logic': str(logic)})  # adaptation logic
        p.update({ClientParameters.PARAM_START: datetime.datetime.now().isoformat()})  # start video

        super(ClientParameters, self).store(parameters=p, when=None)

    def store_vlc_after(self):
        """Store parameters right after VLC exits. """

        p = dict()
        p.update({ClientParameters.PARAM_STOP: datetime.datetime.now().isoformat()})  # stop video
        super(ClientParameters, self).store(parameters=p, when=None)

    @staticmethod
    def check_play_and_log_exit():
        """Check whether the 'playandlog.py' script reached the end.
        :return: True if the PARAM_STOP is present in the file, False otherwise
        """
        filename = ClientParameters.CLIENT_LOGFILE.format(machine='*client')
        f = glob.glob(filename)
        if len(f) <= 0:
            raise FileNotFoundError('file {} does not exist'.format(f))
        if len(f) > 1:
            raise helper.TooManyFilesException('{} client log files found at {}'.format(len(f), os.getcwd()))
        if not (os.path.isfile(f[0])):
            raise FileNotFoundError('file {} does not exist'.format(f[0]))
        if not os.stat(f[0]).st_size > 0:
            raise helper.EmptyFileException('file {} is empty'.format(f[0]))

        with open(f[0], 'r+b') as fd:  # 'r' or 'r+b'
            with contextlib.closing(mmap.mmap(fileno=fd.fileno(), length=0, access=mmap.ACCESS_READ)) as mm:
                bparamstop = ClientParameters.PARAM_STOP.encode()  # can raise unicode error
                if mm.find(bparamstop):
                    return True
        return False


class ServerParameters(AbstractParameters):
    """Class to store parameters on the server."""

    SERVER_LOGFILE = 'parameters_server.log'

    IFCONFIG_LOGFILE = 'parameters_server_ifconfig.log'
    TCP_METRICS_LOGFILE_START = 'parameters_server_ip_tcp_metrics_start.log'
    TCP_METRICS_LOGFILE_STOP = 'parameters_server_ip_tcp_metrics_stop.log'

    @property
    def logfile(self):
        return self.SERVER_LOGFILE

    def create_file(self):
        super(ServerParameters, self).create_file()

    def store_start(self):
        p = dict()
        p.update(get_ubuntu_kernel())
        p.update(get_ipv4_kernel_parameters())
        p.update(get_mptcp_kernel_parameters())  # Check whether mptcp is enabled (on the client).

        p.update(get_git_version(config.VM_PATH_SCRIPTS, 'script-version'))  # Version of the scripts (git commit).
        p.update(get_apache2_version())
        p.update(get_apache2_conf())
        # No vlc git

        super(ServerParameters, self).store(parameters=p, when='start')

        print_ifconfig(filename=ServerParameters.IFCONFIG_LOGFILE)
        print_ip_tcp_metrics(filename=ServerParameters.TCP_METRICS_LOGFILE_START)

    def store_end(self):
        p = dict()

        p.update(get_ipv4_kernel_parameters())
        p.update(get_mptcp_kernel_parameters())

        super(ServerParameters, self).store(parameters=p, when='stop')

        print_ip_tcp_metrics(filename=ServerParameters.TCP_METRICS_LOGFILE_STOP)


def get_machine_param_object() -> AbstractParameters:
    """
    Identify the machine and return the matching AbstractParameters object.
    :return: the correct *Parameters for the machine (Host, Client or Server)
    """
    nodename = os.uname()[1]  # uname -n
    if nodename == config.CLIENT_HOSTNAME:
        return ClientParameters(machine=config.CLIENT_HOSTNAME)
    if nodename == config.RIVAL_HOSTNAME:
        return ClientParameters(machine=config.RIVAL_HOSTNAME)
    elif nodename == config.SERVER_HOSTNAME:
        return ServerParameters()
    elif nodename == config.HOSTNAME:
        return HostParameters()
    else:
        raise WrongMachineException('get_machine_param_object: uname -n name is {}'.format(nodename))
