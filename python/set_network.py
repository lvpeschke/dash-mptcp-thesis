#!/usr/bin/env python3.5
#
# Set the network parameters (delay and bandwidth).
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import math
import sys

import sh

import config
import helper

# Make sure it's only called on the host.
config.check_machine_is_host()

description = """
Network settings and traffic control for the links between the VMs.
"""
epilog = """
See also: test_network.py
"""

CONFIG_NO_HZ = True  # CONFIG_NO_HZ=y
HIGH_RES_TIMERS = True  # CONFIG_HIGH_RES_TIMERS=y
HZ = 1000  # CONFIG_HZ_1000=y
MTU = 1500  # ifconfig | grep MTU  # TODO right one?
# PACKET_SIZE_BYTES = 1514


def _ms_to_s(time):
    return time * 10 ** (-3)


def _Mbps_to_bps(rate):
    return rate * 10 ** 6


def _bits_to_bytes(value):
    return value / 8


def _configs(device):
    print(sh.tc('qdisc', 'show', 'dev', device))


def _stats(device):
    print(sh.tc('-s', '-d', '-p', 'qdisc', 'show', 'dev', device))


def _stop(device, verbose):
    if verbose:
        print('tc qdisc del dev', device, 'root', end=' ... ', flush=True)
    sh.sudo('tc', 'qdisc', 'del', 'dev', device, 'root', _fg=True)
    if verbose:
        print('done.', flush=True)


def _delay_and_rate(device, delay_if, rate, verbose=False):
    """
    Configure tc on a network interface with one netem qdisc for delay and rate control.
    :param device: interface where to attach the qdiscs
    :param delay_if: delay (ms)
    :param rate: bandwidth (Mbps)
    :param verbose: toggle on/off verbose
    :return: --
    """
    assert delay_if > 0, rate > 0

    burst = max(MTU, math.ceil(_bits_to_bytes(_Mbps_to_bps(rate) / HZ)))  # bytes
    netem_queue = 2 * _bits_to_bytes(_ms_to_s(delay_if) * _Mbps_to_bps(rate))  # BDP in bytes (RTT (= 2xdelay) x rate)
    # tbf_queue = 0  # bytes
    limit = math.ceil((burst + 2 * netem_queue) / MTU)  # queue size in packets (burst = 1 packet, 2xBDP)
    # limit = math.ceil((burst + 2 * netem_queue) / PACKET_SIZE_BYTES)

    if verbose:
        print('tc qdisc replace dev', device, 'root',
              'netem', 'limit', limit, 'rate', '{:.3f}mbit'.format(rate), 'delay', '{:.1f}ms'.format(delay_if),
              end=' ... ', flush=True)

    sh.sudo('tc', 'qdisc', 'replace', 'dev', device, 'root',
            'netem',
            'limit', limit,
            'rate', '{:.3f}mbit'.format(rate),
            'delay', '{:.1f}ms'.format(delay_if),
            _fg=True)

    if verbose:
        print('done.', flush=True)


def configs_all_links():
    for link in list(config.ALL_TRIPLE_LINKS.values()):
        _configs(device=link.host_tap_client)
        _configs(device=link.host_tap_server)


def configs_all_triple_links():
    for link in config.ALL_TRIPLE_LINKS.values():
        _configs(device=link.host_tap_client)
        _configs(device=link.host_tap_server)
        _configs(device=link.host_tap_rival)


def stats_all_links():
    for link in list(config.ALL_TRIPLE_LINKS.values()):
        _stats(device=link.host_tap_client)
        _stats(device=link.host_tap_server)


def stats_all_triple_links():
    for link in config.ALL_TRIPLE_LINKS.values():
        _stats(device=link.host_tap_client)
        _stats(device=link.host_tap_server)
        _stats(device=link.host_tap_rival)


def stop_all_links(verbose=False):
    for link in list(config.ALL_TRIPLE_LINKS.values()):
        try:
            _stop(device=link.host_tap_client, verbose=verbose)
            _stop(device=link.host_tap_server, verbose=verbose)
        except Exception as err:
            print(err)
            pass


def stop_all_triple_links(verbose=False):
    for link in config.ALL_TRIPLE_LINKS.values():
        try:
            _stop(device=link.host_tap_client, verbose=verbose)
            _stop(device=link.host_tap_server, verbose=verbose)
            _stop(device=link.host_tap_rival, verbose=verbose)
        except Exception as err:
            print(err)
            pass


def set_link(link: config.Link, delay_rtt, rate, verbose=True):  # For single
    """
    Configure the RTT rtt_delay and rate on one link.
    One qdisc is added on each of the two TAP interfaces in the bridge with rtt_delay/2 and rate.
    :param link:
    :param delay_rtt: RTT rtt_delay on the link
    :param rate:
    :param verbose:
    :return:
    """
    if link not in list(config.ALL_TRIPLE_LINKS.values()):
        raise ValueError('link must be one of {}'.format(str(list(config.ALL_TRIPLE_LINKS.keys()))))
    if isinstance(delay_rtt, str):
        delay_rtt = helper.convert_to_num(delay_rtt)
    assert delay_rtt > 0
    if isinstance(rate, str):
        rate = helper.convert_to_num(rate)
    assert rate > 0

    # RTT on link -> RTT/2 on interface
    _delay_and_rate(device=link.host_tap_client, delay_if=delay_rtt / 2, rate=rate, verbose=verbose)
    _delay_and_rate(device=link.host_tap_server, delay_if=delay_rtt / 2, rate=rate, verbose=verbose)


def set_triple_link(link: config.TripleLink, delay_rtt, rate, verbose=True):
    """
    Configure the RTT rtt_delay and rate on one triple link (3 interfaces).
    One qdisc is added on each of the two TAP interfaces in the bridge with rtt_delay/2 and rate.
    :param link:
    :param delay_rtt: RTT rtt_delay on the link
    :param rate:
    :param verbose:
    :return:
    """
    if link not in config.ALL_TRIPLE_LINKS.values():
        raise ValueError('link must be one of {}'.format(str(list(config.ALL_TRIPLE_LINKS.keys()))))
    delay_rtt = helper.convert_to_num(delay_rtt)
    assert delay_rtt > 0
    rate = helper.convert_to_num(rate)
    assert rate > 0

    # RTT on link -> RTT/2 on interface
    _delay_and_rate(device=link.host_tap_client, delay_if=delay_rtt / 2, rate=rate, verbose=verbose)
    _delay_and_rate(device=link.host_tap_server, delay_if=delay_rtt / 2, rate=rate, verbose=verbose)
    _delay_and_rate(device=link.host_tap_rival, delay_if=delay_rtt / 2, rate=rate, verbose=verbose)


def set_all_links(delays_rtt: list, rates: list, verbose=False):  # For single
    """
    Configure the RTT delay and rate on each link interface (from config).
    For each link, one qdisc is added on each of the two TAP interfaces in the bridge.
    :param delays_rtt: list of delays (ms) for the links, one entry per link
    :param rates: list of rates (Mbps) for the links, one entry per link
    :param verbose: toggle on/off verbose mode
    :return: --
    """
    if len(delays_rtt) != len(rates) != len(config.ALL_TRIPLE_LINKS):
        raise helper.ArgumentLengthMismatchException('RTT delays and rates must be lists of {} elements'
                                                     .format(len(config.ALL_TRIPLE_LINKS)))
    helper.check_positive_num(delays_rtt)
    helper.check_positive_num(rates)

    for d, r, link in zip(delays_rtt, rates, list(config.ALL_TRIPLE_LINKS.values())):
        set_link(link=link, delay_rtt=d, rate=r, verbose=verbose)

    configs_all_links()


def set_all_triple_links(delays_rtt: list, rates: list, verbose=False):
    """
    Configure the RTT delay and rate on each link interface (from config).
    For each link, one qdisc is added on each of the three TAP interfaces in the bridge.
    :param delays_rtt: list of delays (ms) for the links, one entry per link
    :param rates: list of rates (Mbps) for the links, one entry per link
    :param verbose: toggle on/off verbose mode
    :return: --
    """
    if len(delays_rtt) != len(rates) != len(config.ALL_TRIPLE_LINKS):
        raise helper.ArgumentLengthMismatchException('RTT delays and rates must be lists of {} elements'
                                                     .format(len(config.ALL_TRIPLE_LINKS)))
    helper.check_positive_num(delays_rtt)
    helper.check_positive_num(rates)

    for d, r, link in zip(delays_rtt, rates, list(config.ALL_TRIPLE_LINKS.values())):
        set_triple_link(link=link, delay_rtt=d, rate=r, verbose=verbose)

    configs_all_triple_links()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=description,
                                     epilog=epilog,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-v', '--version',
                        action='version',
                        version=__file__ + ' version 20 December 2016')
    subparsers = parser.add_subparsers(help='choose an action')

    parser_set = subparsers.add_parser('set', help='set rates and delays')
    parser_set.add_argument('delays', type=float, nargs=len(config.ALL_TRIPLE_LINKS),
                            help='RTT delays for tapc1/taps1 and tapc2/taps2')
    parser_set.add_argument('rates', type=float, nargs=len(config.ALL_TRIPLE_LINKS),
                            help='rates for tapc1/taps1 and tapc2/taps2')
    parser_set.set_defaults(func=set_all_triple_links)  # /!\ changed for rival

    parser_stop = subparsers.add_parser('stop', help='reset the rates and delays')
    parser_stop.set_defaults(func=stop_all_triple_links)  # /!\ changed for rival

    parser_configs = subparsers.add_parser('configs', help='show current qdisc configuration')
    parser_configs.set_defaults(func=configs_all_triple_links)  # /!\ changed for rival

    parser_stats = subparsers.add_parser('stats', help='show statistics about the current qdisc configuration')
    parser_stats.set_defaults(func=stats_all_triple_links)  # /!\ changed for rival

    args = parser.parse_args()
    print(__file__, 'arguments are...', str(args))

    if args and 'func' in args:
        if args.func is not set_all_triple_links:  # /!\ changed for rival
            args.func()
        else:
            set_all_triple_links(delays_rtt=args.delays, rates=args.rates, verbose=True)

    sys.exit(0)
