#!/usr/bin/env python3.5
#
# Script to automate several comparison executions.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
from time import strftime

import sh

import config
import do_comparison
import do_test
import helper
import vlc

# SETUP HERE
comp_videos = [config.VIDEO_BBB2_SIMPLE, config.VIDEO_BBB2_SIMPLE,
               config.VIDEO_BBB10_SIMPLE, config.VIDEO_BBB10_SIMPLE]  # list videos
assert all(isinstance(e, config.DASHVideo) for e in comp_videos)
assert all(e in config.ALL_VIDEOS.values() for e in comp_videos)
comp_logic = vlc.RATE_LOGIC
comp_delays = config.RTT_DELAYS  # list RTT ms,
comp_bws = [10, 11.7, 8, 9]  # list Mbps, calibration results [10.00, 11.70, 8, 9]
comp_bw_ratios = config.BW_RATIOS
comp_rtt_var = config.RTT_DELAY_VAR
comp_cong_ctrl_tcp = config.CONG_CTRL_TCP_DEFAULT
comp_cong_ctrl_mptcp = config.CONG_CTRL_MPTCP_DEFAULT
comp_repetitions = 1
comp_rival = False  # single or congestion

comp_log = 'comparison-{video}_{cong_ctrl_tcp}_{cong_ctrl_mptcp}-{bw}Mbps-{delay}ms-{logic}-{dt}.txt'
with helper.cd(config.HOST_PATH_TESTS_COMPARISON):
    for video, bw, delay in zip(comp_videos, comp_bws, comp_delays):
        do_test.play_and_kill(link=1, video=video, seconds=30, verbose=True)  # avoid swapping
        filename = comp_log.format(video=video.id,
                                   cong_ctrl_tcp=comp_cong_ctrl_tcp, cong_ctrl_mptcp=comp_cong_ctrl_mptcp,
                                   bw=bw, delay=delay, logic=comp_logic, dt=str(strftime('%y%m%d-%H%M')))
        folder_pattern = str(strftime('%y%m%d-*ms'))

        original_stdout = sys.stdout
        original_stderr = sys.stderr

        with open(filename, 'w') as sys.stdout:
            do_comparison.main(bandwidth=bw, delay=delay, video=video, rival=comp_rival,
                               repetitions=comp_repetitions,
                               logic=comp_logic,
                               ratios=comp_bw_ratios, delay_var=comp_rtt_var,
                               cong_ctrl_tcp=comp_cong_ctrl_tcp, cong_ctrl_mptcp=comp_cong_ctrl_mptcp,
                               do_extract=False, verbose=True)
            sys.stdout.flush()

        sys.stdout = original_stdout
        sys.stderr = original_stderr

        sh.ssh(config.CLIENT_SSH, 'sudo', 'rm', '-r', folder_pattern,
               _in='/dev/null', _out='/dev/null', _err='/dev/null')
        sh.ssh(config.SERVER_SSH, 'sudo', 'rm', '-r', folder_pattern,
               _in='/dev/null', _out='/dev/null', _err='/dev/null')

print('End of ' + __file__)
