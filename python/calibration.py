# Calibration quality record (CQR) used for TCP calibration.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import glob
import os

import pandas as pd

import config
import helper
import vlc
from helper import p2f


class CQR:
    """Class representing a TCP calibration result."""

    QUALITY_RECORD = '{dt}_cqr_{video}_{logic}_{cc}_{delay}ms{ext}'
    ext = helper.DOT_CSV

    col_bw = 'bandwidth (Mbps)'
    col_delay = 'delay (ms)'
    col_bitrate_success_rate = 'max. bitrate success rate'
    col_bitrate_count = 'count bitrate'
    col_bitrate_mean = 'mean bitrate (bps)'
    col_bitrate_std = 'std bitrate (bps)'
    col_bitrate_min = 'min bitrate (bps)'
    col_bitrate_max = 'max bitrate (bps)'
    col_bitrate_25 = '25 percentile bitrate (bps)'
    col_bitrate_50 = '50 percentile bitrate (bps)'
    col_bitrate_75 = '75 percentile bitrate (bps)'
    col_downs = 'down'
    col_drops = 'drop'
    col_headers = [col_bw, col_delay,
                   col_bitrate_success_rate,
                   col_bitrate_count, col_bitrate_mean, col_bitrate_std, col_bitrate_min, col_bitrate_max,
                   col_bitrate_25, col_bitrate_50, col_bitrate_75,
                   col_downs, col_drops]

    def __init__(self, video, logic, delay, cong_ctrl):
        assert isinstance(video, config.DASHVideo)
        assert video in config.ALL_VIDEOS.values()
        assert logic in vlc.ADAPTIVE_LOGICS
        helper.check_positive_num(delay)
        assert cong_ctrl in config.CONG_CTRL_ALGOS
        self.video = video
        self.logic = logic
        self.delay = delay
        self.cong_ctrl = cong_ctrl
        self.filename = None

    def create_file(self, datetime):
        """Create an empty calibration quality record in a new file.

        :param datetime: timestamp of creation
        :return: new filename
        """
        filename = CQR.QUALITY_RECORD.format(dt=datetime, video=self.video.id, logic=self.logic,
                                             cc=self.cong_ctrl, delay=self.delay, ext=CQR.ext)
        with open(filename, 'x') as fd:
            fd.write(', '.join(CQR.col_headers) + '\n')
        self.filename = filename
        return filename

    def write_entry(self, bw, delay, success_rate,
                    bitrate_count, bitrate_mean, bitrate_std, bitrate_min, bitrate_max,
                    bitrate_lower, bitrate_fifty, bitrate_upper,
                    down_switches, drops):
        """Write one QoE entry to file.

        The entry has the parameter values and the file is this object's record file.
        """
        # helper.check_positive_num(bw)
        # helper.check_positive_num(delay)
        # helper.check_positive_num(get_success_rate)
        # helper.check_positive_num(bitrate_count)
        # helper.check_positive_num(bitrate_mean)
        # helper.check_positive_num(bitrate_std)
        # helper.check_positive_num(bitrate_min)
        # helper.check_positive_num(bitrate_max)
        # helper.check_positive_num(bitrate_lower)
        # helper.check_positive_num(bitrate_fifty)
        # helper.check_positive_num(bitrate_upper)
        # helper.check_positive_num(down_switches)
        # helper.check_positive_num(drops)
        if self.filename is None:
            raise RuntimeError('no file has been created for this cqr')
        # if not helper.check_is_file(self.filename):
        if not os.path.isfile(self.filename):
            raise FileNotFoundError('could not find cqr {} at {}'.format(self.filename, os.getcwd()))
        if not os.stat(self.filename).st_size > 0:
            raise helper.EmptyFileException('cqr {} at {} is empty'.format(self.filename, os.getcwd()))

        with open(self.filename, 'a') as fd:
            fd.write('{:.4f}, {}, {:.4f}'.format(bw, delay, success_rate))
            fd.write(', ')
            fd.write('{}, {:.4f}, {:.4f}, {:.4f}, {:.4f}'.format(bitrate_count, bitrate_mean, bitrate_std,
                                                                 bitrate_min, bitrate_max))
            fd.write(', ')
            fd.write('{:.4f}, {:.4f}, {:.4f}'.format(bitrate_lower, bitrate_fifty, bitrate_upper))
            fd.write(', ')
            fd.write('{}, {}'.format(down_switches, drops))
            fd.write('\n')

    @staticmethod
    def load_data_df(directory, video, logic, delay, cong_ctrl, verbose=False):
        """ Load all CQR files with the matching infos into one pandas.DataFrame.

        Infos are read from file names in the directory: video id, logic, delay, and congestion control.
        """
        assert isinstance(video, config.DASHVideo)
        assert video in config.ALL_VIDEOS.values()
        assert logic in vlc.ADAPTIVE_LOGICS
        helper.check_positive_num(delay)
        assert cong_ctrl in config.CONG_CTRL_ALGOS
        with helper.cd(directory):
            string = CQR.QUALITY_RECORD.format(dt='*', video=video.id, logic=logic,
                                               cc=cong_ctrl, delay=delay, ext=CQR.ext)
            fs = glob.glob(string)
            if len(fs) <= 0:
                print('No "{}" file(s) found...'.format(string))
                return None

            for f in fs:
                if not (os.path.isfile(f)):
                    raise FileNotFoundError('file {} does not exist'.format(f))
                if not os.stat(f).st_size > 0:
                    raise helper.EmptyFileException('file {} is empty'.format(f))

        if verbose:
            print('{} file(s) found for {}...'.format(len(fs), string))

        df_0 = pd.read_csv(filepath_or_buffer=fs[0], header=0, skipinitialspace=True,
                           converters={CQR.col_bitrate_success_rate: p2f},
                           verbose=verbose)
        frames = [df_0]
        for f in fs[1:]:
            df_plus = pd.read_csv(filepath_or_buffer=f, header=0, skipinitialspace=True,
                                  converters={CQR.col_bitrate_success_rate: p2f},
                                  verbose=verbose)
            frames.append(df_plus)
        return pd.concat(frames, ignore_index=True)
