# Functions to launch actions on the VMs from the server.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import logging

import helper


def ssh_execute(machine, command, err_msg, sudo=False, python=None, must_succeed=False):
    """Run command or Python script on remote machine via ssh."""

    command_list = ['ssh', machine]
    if sudo:
        command_list.append('sudo')
    if python:
        command_list.append(python)
    command_list.append(command)
    command = ' '.join(command_list)
    logging.info('ssh command is... %s', command)

    result = helper.run_command(command=command,
                                err_msg='ssh_execute: {}'.format(err_msg),
                                must_succeed=must_succeed)
    logging.info('ssh on %s return code... %d', machine, result.returncode)
    logging.info('ssh on %s stdout start...', machine)
    logging.info(result.stdout.rstrip())
    logging.info('ssh on %s stdout end...', machine)

    return result


def rsync_from_remote(remote_machine, remote_path, local_folder, local_directory, err_msg):
    """
    Copy files with rsync from a remote machine to the current one."""

    local_path = os.path.join(local_directory, local_folder)
    if not os.path.isdir(local_path):
        message = 'directory {} does not exist on the source machine.'.format(local_path)
        raise FileNotFoundError(message)

    remote = '{machine}:{folder}/'.format(machine=remote_machine, folder=remote_path)

    command = 'rsync -ahvzP {remote} {local}'.format(remote=remote, local=local_path)

    logging.info('rsync command is... %s', command)

    result = helper.run_command(command=command,
                                err_msg='rsync_from_remote: {}'.format(err_msg),
                                must_succeed=False)
    logging.info('rsync on %s return code... %d', remote_machine, result.returncode)
    logging.info('rsync on %s stdout start...', remote_machine)
    logging.info(result.stdout.rstrip())
    logging.info('rsync on %s stdout end...', remote_machine)

    return result
