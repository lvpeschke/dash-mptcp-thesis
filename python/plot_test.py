#!/usr/bin/env python3.5
#
# Script to compute, save, and plot single test results.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import multiprocessing as mp
import os
import sys
import traceback

import matplotlib.pyplot as plt
import pandas as pd

import config
import helper
import vlc
from graphs import vlc_http_connections, vlc_http_requests, vlc_logic, vlc_timing, vlc_vhf, tcpprobe_cwnd, \
    tcpprobe_srtt, tcpprobe_cdf

description = """
Compute, save, and plot results for a single test.
"""
epilog = """
See also: all files in graphs/
"""

# -- LaTeX settings -- #
plt.rc('font', **{'family': 'serif', 'serif': ['Palatino']}) # 'size': 13
plt.rc('text', usetex=True)
plt.rc('legend', fontsize='large')
plt.rc('xtick', labelsize='large')
plt.rc('ytick', labelsize='large')
plt.rc('axes', labelsize='x-large')


def convert(vector: pd.Series, to_subtract, to_multiply):
    """Convert a pd.Series (or one col of a pd.DataFrame).

    :param vector:
    :param to_subtract: scalar or pandas.Series
    :param to_multiply: scalar or pandas.Series
    :return:
    """
    if not isinstance(vector, pd.Series):
        raise TypeError(__file__ + ': vector is not of type pandas.Series')

    return vector.sub(to_subtract).mul(to_multiply)


def find_time_limit(directory, verbose=False):
    """Find start end end time of VLC.

    :param directory:
    :param verbose:
    :return: (start offset, stop offset - start offset)
    """
    df_start = vlc.START.load_data_df(directory=directory, verbose=verbose)
    df_stop = vlc.STOP.load_data_df(directory=directory, verbose=verbose)
    found_start = df_start.iloc[0, 0]
    found_stop = df_stop.iloc[0, 0] - found_start
    if verbose:
        print('Real start at...', found_start)
        print('Virtual stop at...', found_stop)

    return found_start, found_stop


def normalise_for_cdf(vector: pd.Series, unit: str):
    """Compute a weighted normalisation.

    :param vector: pandas.Series
    :param unit: name of the column containing the data of vector in the result
    :return: pandas.DataFrame
    """
    if not isinstance(vector, pd.Series):
        raise TypeError(__file__ + ': vector is not of type pandas.Series')
    if not isinstance(unit, str):
        raise TypeError(__file__ + ': unit is not of type str')

    v_groups = vector.value_counts().sort_index()  # count occurrence of each value, sort by value
    v_sizes = v_groups.values * v_groups.index
    v_shares = v_sizes / vector.sum()

    data = {unit: v_groups.index.values,
            'count': v_groups.values,
            'size': v_sizes,
            'share': v_shares.values}

    return pd.DataFrame(data=data)


def check_streams(directory, logic, verbose=False):
    """Verify properties of the media streams to ensure the graphs have meaning.

    :param directory:
    :param logic:
    :param verbose:
    :return:
    """
    assert logic in vlc.ADAPTIVE_LOGICS

    # Check that the video is single streamed
    if logic == vlc.RATE_LOGIC:
        df0_rep = vlc.RBLOGIC_REPRESENTATION.load_data_df(directory=directory, verbose=verbose)
        df0_switch = vlc.RBLOGIC_SWITCH.load_data_df(directory=directory, verbose=verbose)
        df0_bufstate = vlc.RBLOGIC_BUFSTATE.load_data_df(directory=directory, verbose=verbose)
        df0_buflevel = vlc.RBLOGIC_BUFLEVEL.load_data_df(directory=directory, verbose=verbose)

        for df0, msg in zip([df0_rep, df0_switch, df0_bufstate, df0_buflevel],
                            ['representation', 'switch', 'bufstate', 'buflevel']):
            n = df0[vlc.STREAM_ID].nunique(dropna=False)
            if n != 1:
                print('/!\ {} streams found for rblogic/{}'.format(n, msg))
                return False

    elif logic == vlc.PREDICTIVE_LOGIC:
        df0_streams = vlc.PLOGIC_NUMBER_STREAMS.load_data_df(directory=directory, verbose=verbose)
        df0_highest1 = vlc.PLOGIC_HIGHEST1.load_data_df(directory=directory, verbose=verbose)
        df0_highest2 = vlc.PLOGIC_HIGHEST2.load_data_df(directory=directory, verbose=verbose)
        df0_stats = vlc.PLOGIC_STATS.load_data_df(directory=directory, verbose=verbose)
        df0_used = vlc.PLOGIC_BPS_USED.load_data_df(directory=directory, verbose=verbose)
        df0_update = vlc.PLOGIC_UPDATE_RATE.load_data_df(directory=directory, verbose=verbose)
        df0_switch = vlc.PLOGIC_SWITCH.load_data_df(directory=directory, verbose=verbose)
        df0_bufstate = vlc.PLOGIC_BUFSTATE.load_data_df(directory=directory, verbose=verbose)
        df0_buflevel = vlc.PLOGIC_BUFLEVEL.load_data_df(directory=directory, verbose=verbose)

        if not (df0_streams['streams'].nunique() == 1 and 1 in df0_streams['streams'].unique()):
            for df0, msg in zip([df0_highest1, df0_highest2, df0_stats, df0_used, df0_update,
                                 df0_switch, df0_bufstate, df0_buflevel],
                                ['highest1', 'highest2', 'stats', 'bps_used', 'update',
                                 'switch', 'bufstate', 'buflevel']):
                n = df0[vlc.STREAM_ID].nunique(dropna=False)
                if n != 1:
                    print('/!\ {} streams found for plogic/{}'.format(n, msg))
                    return False

    else:
        print('unknown logic: {}'.format(logic))
        return False

    return True


def find_folders(directory, find_file_func=vlc.find_vlclog, verbose=False):
    """Find folders where there is data to plot."""

    result = list()
    with helper.cd(directory):
        if verbose:
            print('Trying in main directory: {} (provided)...'.format(directory))

        filename = find_file_func()
        if filename:
            result.append(os.getcwd())
        else:
            if verbose:
                print('No files found in {}, trying subfolders...'.format(directory))
                # try subfolders
            for subdir in [subdir for subdir in os.listdir(os.getcwd())
                           if os.path.isdir(os.path.abspath(os.path.expanduser(subdir)))]:
                # if os.path.isdir(os.path.join(directory, subdir))]:
                full_subpath = os.path.abspath(os.path.expanduser(subdir))  # os.path.join(directory, subdir)
                with helper.cd(full_subpath):
                    if verbose:
                        print('Trying the subdirectory: {}...'.format(subdir), end=' ', flush=True)
                    filename = find_file_func()
                    if filename:
                        result.append(os.getcwd())
                    elif verbose:
                        print('nothing found, pass...')
    print('folders:', result)
    return result


if __name__ == '__main__':
    print('plot is __main__')
    parser = argparse.ArgumentParser(description=description, epilog=epilog)
    parser.add_argument('directory',
                        type=str,
                        help='directory for the new files')
    parser.add_argument('logic',
                        type=str,
                        choices=vlc.ADAPTIVE_LOGICS,
                        help='adaptation logic')
    parser.add_argument('--video',
                        type=str,
                        default=config.VIDEO_DEFAULT.id,
                        choices=config.ALL_VIDEOS,
                        help='which video was calibrated (see config.py)')
    parser.add_argument('--override',
                        action='store_true',
                        default=False,
                        help='override already existing plot folder(s) or not')
    parser.add_argument('-v', '--version',
                        action='version',
                        version=__file__ + ' version December 2016')
    args = parser.parse_args()

    args.video = config.ALL_VIDEOS.get(args.video)
    folder = config.HOST_FOLDER_PLOTS  # SETUP
    verb = True  # SETUP

    # Find where there are vlclogs (here or subdirs)
    data_dirs = find_folders(directory=args.directory, find_file_func=vlc.find_vlclog, verbose=verb)

    # MULTITHREADING
    mp.set_start_method('forkserver')
    with mp.Pool(processes=10) as pool:
        for data_dir in data_dirs:
            if verb:
                print('Trying to plot in: {} (provided)...'.format(data_dir))

            # check if plot folder needs to be created (do not override)
            folder_path = os.path.join(data_dir, folder)
            if os.path.isdir(folder_path):
                plot_dir = folder_path
            else:
                plot_dir = helper.create_folder_in_path(path=data_dir, folder=folder, override=args.override)

            single_stream = check_streams(directory=data_dir, logic=args.logic, verbose=False)
            if not single_stream:
                print('Multiple streams in vlc logs!')

            start, stop = find_time_limit(directory=data_dir, verbose=verb)  # milliseconds
            plt.close('all')

            if args.logic == vlc.RATE_LOGIC:
                logic_func = vlc_logic.rblogic
            elif args.logic == vlc.PREDICTIVE_LOGIC:
                logic_func = vlc_logic.plogic
            else:
                print('unknown logic: {}'.format(args.logic))
                sys.exit(1)

            results = [
                pool.apply_async(func=vlc_timing.timing,
                                 args=(data_dir, plot_dir, start, stop, args.override, verb)),

                pool.apply_async(func=logic_func,
                                 args=(data_dir, plot_dir, start, stop, args.override, verb)),

                pool.apply_async(func=vlc_vhf.vhf,
                                 args=(data_dir, plot_dir, start, stop, args.override, verb)),

                pool.apply_async(func=vlc_http_requests.requests,
                                 args=(data_dir, plot_dir, start, stop,
                                       args.video.segment_size, args.video.segment_amount, args.video.max_bitrate,
                                       args.override, verb)),

                pool.apply_async(func=vlc_http_connections.connections,
                                 args=(data_dir, plot_dir, start, stop, args.override, verb)),

                pool.apply_async(func=tcpprobe_cwnd.cwnd_ssthresh,
                                 args=(data_dir, plot_dir, args.override, verb)),

                pool.apply_async(func=tcpprobe_srtt.srtt,
                                 args=(data_dir, plot_dir, args.override, verb)),

                pool.apply_async(func=tcpprobe_cdf.tcpprobe_cdf,
                                 args=(data_dir, plot_dir, args.override, verb)),

            ]  # TODO count how many succeed, try catch in functions

            print('Ordered results using pool.apply_async():')
            for r in results:
                try:
                    print('\t', r.get())
                except Exception as e:
                    traceback.print_tb(e.__traceback__)
                    print(e)
            print()
            plt.close('all')
    # End of mp

    print('End of plot!')
    sys.exit()
