#!/usr/bin/env python3.5
#
# Scripts to automate a test to assess Linux's TCP optimisations:
# tcp_no_metrics_save & slow_start_after_idle
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import itertools
import sys
from time import strftime, sleep

import sh

import config
import do_test
import helper
import vlc
import vm_settings

# SETUP HERE
# for test_network
network_test_delays = config.RTT_DELAYS  # list RTT ms, same as in test_network
network_test_bws = [10, 20, 50]   # test_network.BANDWIDTHS  # list Mbps, same as in test_network
network_test_cong_ctrl = config.CONG_CTRL_TCP_DEFAULT  # default TCP
network_test_log = 'network_tcp_optimisations-{cc}-{dt}.txt'
# for do_test
network_test_videos = [config.VIDEO_BBB10_SIMPLE] # [config.VIDEO_BBB2_SIMPLE]  # list
network_test_video_logics = [vlc.RATE_LOGIC]  # list
network_test_video_cong_ctrls = [network_test_cong_ctrl]  # list
network_test_video_delays = [25, 100]  # list RTT ms
network_test_video_bws = [6.5, 10]  # [10, 20]  # list Mbps

# Check setup
for e in [network_test_delays, network_test_bws]:
    assert isinstance(e, (list, set, dict, map))
assert len(network_test_video_delays) == len(network_test_video_bws)
assert network_test_cong_ctrl in config.CONG_CTRL_ALGOS
for e in [network_test_videos, network_test_video_logics,
          network_test_video_cong_ctrls,
          network_test_video_delays, network_test_video_bws]:
    assert isinstance(e, (list, set, dict, map))
for video in network_test_videos:
    assert video in config.ALL_VIDEOS.values()
for cc in network_test_video_cong_ctrls:
    assert cc in config.CONG_CTRL_ALGOS

tcp_no_save_metrics = [0, 1]  # default 0 (save enabled)
tcp_slow_start_after_idle = [0, 1]  # default 1 (enabled)
print('all parameters ok')
print('used SUDO here before? or wait for a very long time... :)')
print('sleeping for 5 seconds, abort now if necessary...')
sleep(5)
print('resuming...')

original_stdout = sys.stdout
original_stderr = sys.stderr
with helper.cd(config.HOST_PATH_TESTS_NETWORK):
    filename = network_test_log.format(cc=network_test_cong_ctrl, dt=str(strftime('%y%m%d-%H%M')))
    with open(filename, 'w') as sys.stdout:
        # Enter double nested loop
        for no_save_metrics, ss_after_idle in itertools.product(tcp_no_save_metrics, tcp_slow_start_after_idle):
            print('iteration {}-{}'.format(no_save_metrics, ss_after_idle), file=sys.stderr, flush=True)
            # Test the network with the standard tools, store result in standard folder
            print('\n * Test case: net.ipv4.tcp_congestion_control {}, '
                  'net.ipv4.tcp_no_metrics_save {}, '
                  'net.ipv4.tcp_slow_start_after_idle {}'.format(
                    network_test_cong_ctrl, no_save_metrics, ss_after_idle))
            # Set network for the test, symmetric on both VMs
            vm_settings.set_tcp_no_save_metrics(machine=config.CLIENT_SSH, value=no_save_metrics)
            vm_settings.set_tcp_slow_start_after_idle(machine=config.CLIENT_SSH, value=ss_after_idle)
            vm_settings.set_tcp_no_save_metrics(machine=config.SERVER_SSH, value=no_save_metrics)
            vm_settings.set_tcp_slow_start_after_idle(machine=config.SERVER_SSH, value=ss_after_idle)

            print('after setting tcp optis', file=sys.stderr, flush=True)

            # No network test
            # test_network.main(delays=network_test_delays, bandwidths=network_test_bws,
            #                   cong_ctrl=network_test_cong_ctrl)

            # Play the videos + anti swapping
            for video, logic, cc in itertools.product(network_test_videos,
                                                      network_test_video_logics,
                                                      network_test_video_cong_ctrls):
                print('video {}-{}-{}'.format(video.id, logic, cc), file=sys.stderr, flush=True)
                do_test.play_and_kill(link=1, video=video, seconds=30, verbose=True)  # avoid swapping
                for bw, d in zip(network_test_video_bws, network_test_video_delays):
                    print('video {}-{}-{}, {}Mbps-{}ms'.format(video.id, logic, cc, bw, d), file=sys.stderr, flush=True)
                    print(' * Video test case: rate {}Mbps, delay {}ms, congestion control {}, '
                          'video {}, logic "{}", directory {}'.format(
                            bw, d, cc, video.id, logic, config.HOST_PATH_TESTS_NETWORK))
                    folder_pattern = str(strftime('%y%m%d-*ms'))
                    do_test.main(rates=[bw] * 2, delays=[d] * 2,
                                 link=1, mptcp=0,
                                 cong_ctrl=cc, video=video, logic=logic,
                                 directory=config.HOST_PATH_TESTS_NETWORK,
                                 do_extract=True, verbose=True)
                    print('end of video {}-{}-{}, {}Mbps-{}ms, before remote rm'.format(video.id, logic, cc, bw, d),
                          file=sys.stderr, flush=True)
                    sh.ssh(config.CLIENT_SSH, 'sudo', 'rm', '-r', folder_pattern,
                           _in='/dev/null', _out='/dev/null', _err='/dev/null')
                    sh.ssh(config.SERVER_SSH, 'sudo', 'rm', '-r', folder_pattern,
                           _in='/dev/null', _out='/dev/null', _err='/dev/null')
                    print(' * End of this video test case...')
                print(' * End of all video test cases.')
            print(' * End of this test case...')
            sys.stdout.flush()
        print(' * End of all test cases.')
sys.stdout = original_stdout
sys.stderr = original_stderr

# Restore TCP parameters on the VMs
vm_settings.set_tcp_no_save_metrics(machine=config.CLIENT_SSH, value=0)
vm_settings.set_tcp_slow_start_after_idle(machine=config.CLIENT_SSH, value=1)
vm_settings.set_tcp_no_save_metrics(machine=config.SERVER_SSH, value=0)
vm_settings.set_tcp_slow_start_after_idle(machine=config.SERVER_SSH, value=1)

print('End of ' + __file__)
