# Infrastructure configuration.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import os
import re
import logging

import isodate
import sh

# Logging.
logging.basicConfig(level=logging.INFO, format='%(asctime)s:%(levelname)s:%(name)s: %(message)s')


############
# MACHINES #
############
# On the host machine.
HOSTNAME = 'bareboneStud.info.ucl.ac.be'
CLIENT_VM = 'NewClient'
SERVER_VM = 'NewServer'
RIVAL_VM = 'RivalClient'
CLIENT_SSH = 'myclientssh'  # ssh through the nat: -p 3024 user@127.0.0.1
SERVER_SSH = 'myserverssh'  # ssh through the nat: -p 3042 user@127.0.0.1
RIVAL_SSH = 'myrivalssh'    # ssh through the nat: -p 3060 user@127.0.0.1
NET1 = '10.0.11.0/24'
NET2 = '10.0.22.0/24'

# On the VMs.
CLIENT_HOSTNAME = 'newclient'   # hostname client
SERVER_HOSTNAME = 'newserver'   # hostname server
RIVAL_HOSTNAME = 'rivalclient'  # hostname rival client
CLIENT1 = 'myclient1'   # hosts file
CLIENT2 = 'myclient2'   # hosts file
SERVER1 = 'myserver1'   # hosts file
SERVER2 = 'myserver2'   # hosts file
RIVAL1 = 'myrival1'     # hosts file
RIVAL2 = 'myrival2'     # hosts file

CONG_CTRL_ALGOS = {'cubic', 'reno', 'lia', 'olia', 'wvegas',
                   'balia'}  # sysctl net.ipv4.tcp_available_congestion_control
CONG_CTRL_MPTCP_DEFAULT = 'olia'
CONG_CTRL_TCP_DEFAULT = 'reno'


def check_machine_is_host():
    hostname = (sh.hostname()).rstrip()
    if hostname != HOSTNAME:
        raise OSError('Unknown hostname "{}", should be "{}"!\nCheck the config.py file.'.format(hostname, HOSTNAME))


#########
# LINKS #
#########
class Link:
    """
    A link on the host connecting the Client VM and the Server VM.
    It is set up as 2 TAP interfaces joined in a bridge.
    """

    INTF_ETH1 = 'eth1'
    INTF_ETH2 = 'eth2'

    def __init__(self, host_bridge,
                 host_tap_client, host_tap_server,
                 client_addr, client_intf,
                 server_addr, server_intf,
                 net):
        self.host_bridge = host_bridge
        self.host_tap_client = host_tap_client
        self.host_tap_server = host_tap_server
        self.client_addr = client_addr
        self.client_intf = client_intf
        self.server_addr = server_addr
        self.server_intf = server_intf
        self.net = net


class TripleLink(Link):
    """
    A link on the host connecting the Client VM, the Server VM, and the Rival VM.
    It is set up as 3 TAP interfaces joined in a bridge.
    """

    def __init__(self, host_bridge,
                 host_tap_client, host_tap_server, host_tap_rival,
                 client_addr, client_intf,
                 server_addr, server_intf,
                 rival_addr, rival_intf,
                 net):
        super().__init__(host_bridge=host_bridge,
                         host_tap_client=host_tap_client, host_tap_server=host_tap_server,
                         client_addr=client_addr, client_intf=client_intf,
                         server_addr=server_addr, server_intf=server_intf,
                         net=net)
        self.host_tap_rival = host_tap_rival
        self.rival_addr = rival_addr
        self.rival_intf = rival_intf

        self.tap_list = [self.host_tap_client, self.host_tap_server, self.host_tap_rival]

TRIPLE_LINK1 = TripleLink(host_bridge='vbr1',
                          host_tap_client='tapc1', host_tap_server='taps1', host_tap_rival='tapr1',
                          client_addr=CLIENT1, client_intf=TripleLink.INTF_ETH1,
                          server_addr=SERVER1, server_intf=TripleLink.INTF_ETH1,
                          rival_addr=RIVAL1, rival_intf=TripleLink.INTF_ETH1,
                          net=NET1)

TRIPLE_LINK2 = TripleLink(host_bridge='vbr2',
                          host_tap_client='tapc2', host_tap_server='taps2', host_tap_rival='tapr2',
                          client_addr=CLIENT2, client_intf=TripleLink.INTF_ETH2,
                          server_addr=SERVER2, server_intf=TripleLink.INTF_ETH2,
                          rival_addr=RIVAL2, rival_intf=TripleLink.INTF_ETH2,
                          net=NET2)

ALL_TRIPLE_LINKS = {1: TRIPLE_LINK1, 2: TRIPLE_LINK2}
LINK_DEFAULT = 1

#########
# Paths #
#########
VM_PYTHON = '/home/user/miniconda3/bin/python3.5'

HOST_PATH_TESTS = '/home/user/gdrive2/tfe-results-rival'
HOST_PATH_TESTS_SINGLE = os.path.join(HOST_PATH_TESTS, 'single')
HOST_PATH_TESTS_DOUBLE = os.path.join(HOST_PATH_TESTS, 'double')
HOST_PATH_TESTS_CALIBRATION = os.path.join(HOST_PATH_TESTS, 'calibration')
HOST_PATH_TESTS_COMPARISON = os.path.join(HOST_PATH_TESTS, 'comparison')
HOST_PATH_TESTS_COMPARISON2 = os.path.join(HOST_PATH_TESTS, 'comparison2')
HOST_PATH_TESTS_NETWORK = os.path.join(HOST_PATH_TESTS, 'network')
HOST_PATH_TESTS_NETWORK2 = os.path.join(HOST_PATH_TESTS, 'network2')
HOST_FOLDER_PLOTS = 'plots'

VM_PATH_SCRIPTS = "/home/user/git-repos/tfe-scripts/"
HOST_PATH_SCRIPTS = "/home/user/tfe-scripts/"

CLIENT_CVLC = '/home/user/here/bin/cvlc'
CLIENT_VLC = '/home/user/here/bin/vlc'


#################
# VIDEO DATASET #
#################
class DASHVideo:
    """A DASH video."""

    def __init__(self, path_on_server, name, video_id,
                 segment_size, segment_amount,
                 max_width, max_height, max_bitrate,
                 min_init_bytes, max_init_bytes,
                 duration, min_buffer_time,
                 num_adaptation_sets):
        self.path = path_on_server
        self.name = name
        self.id = video_id

        self.segment_size = segment_size
        self.segment_amount = segment_amount
        self.max_width = max_width
        self.max_height = max_height
        self.max_bitrate = max_bitrate

        self.min_init_bytes = min_init_bytes
        self.max_init_bytes = max_init_bytes

        self.duration = duration
        self.min_buffer_time = min_buffer_time

        self.num_adaptation_sets = num_adaptation_sets

    def get_duration_s(self):
        """Get the video duration in seconds."""
        duration = isodate.parse_duration(self.duration)
        if isinstance(duration, isodate.Duration):
            duration = duration.totimedelta(start=0)
        if isinstance(duration, datetime.timedelta):
            return duration.total_seconds()

    def get_max_bitrate_Mbps(self):
        """Get the maximum bitrate in Mbps."""
        return self.max_bitrate / 10 ** 6

    def get_url(self, link_nr):
        """Get a valid URL.

        Construct an URL for the video, given a specific link to the server.
        :param link_nr: number corresponding to the link to the server (1st, 2nd, etc.)
        :return: an URL to the video
        """
        if link_nr not in ALL_TRIPLE_LINKS:
            raise ValueError('link_nr is {} but must be in {}'.format(link_nr, str(list(ALL_TRIPLE_LINKS.keys()))))
        return 'http://{server}/{path}'.format(server=ALL_TRIPLE_LINKS[link_nr].server_addr, path=self.path)


VIDEO_BBB2_OD = DASHVideo(
        path_on_server='videos/uniat/DASHDataset2014/BigBuckBunny/2sec/BigBuckBunny_2s_onDemand_2014_05_09.mpd',
        name='BigBuckBunny_2s_onDemand_2014_05_09', video_id='bbb2od',
        segment_size=2, segment_amount=299,
        max_width=1920, max_height=1080,
        max_bitrate=4219897,
        min_init_bytes=877,
        # 877, 879, 879, 880, 880, 880, 880, 880, 888, 888, 880, 882, 882, 882, 883, 883, 883, 883, 883, 883
        max_init_bytes=888,
        duration='PT0H9M56.46S',
        min_buffer_time='PT1.500000S',
        num_adaptation_sets=1  # video
)

VIDEO_BBB2_SIMPLE = DASHVideo(
        path_on_server='videos/uniat/DASHDataset2014/BigBuckBunny/2sec/BigBuckBunny_2s_simple_2014_05_09.mpd',
        name='BigBuckBunny_2s_simple_2014_05_09', video_id='bbb2s',
        segment_size=2, segment_amount=299,
        max_width=1920, max_height=1080,
        max_bitrate=4219897,
        min_init_bytes=877,
        # 877, 879, 879, 880, 880, 880, 880, 880, 888, 888, 880, 882, 882, 882, 883, 883, 883, 883, 883, 883
        max_init_bytes=888,
        duration='PT0H9M56.46S',
        min_buffer_time='PT1.500000S',
        num_adaptation_sets=1  # video
)

VIDEO_BBB6_SIMPLE = DASHVideo(
        path_on_server='videos/uniat/DASHDataset2014/BigBuckBunny/6sec/BigBuckBunny_6s_simple_2014_05_09.mpd',
        name='BigBuckBunny_6s_simple_2014_05_09', video_id='bbb6s',
        segment_size=6, segment_amount=100,
        max_width=1920, max_height=1080,
        max_bitrate=3858484,
        min_init_bytes=877,
        # 877, 879, 879, 880, 880, 880, 880, 880, 888, 888, 880, 882, 882, 882, 883, 883, 883, 883, 883, 883
        max_init_bytes=888,
        duration='PT0H9M56.46S',
        min_buffer_time='PT1.500000S',
        num_adaptation_sets=1  # video
)

VIDEO_BBB10_SIMPLE = DASHVideo(
        path_on_server='videos/uniat/DASHDataset2014/BigBuckBunny/10sec/BigBuckBunny_10s_simple_2014_05_09.mpd',
        name='BigBuckBunny_10s_simple_2014_05_09', video_id='bbb10s',
        segment_size=10, segment_amount=60,
        max_width=1920, max_height=1080,
        max_bitrate=3792491,
        min_init_bytes=879,
        # 879, 881, 881, 882, 882, 882, 882, 882, 888, 888, 882, 884, 884, 884, 885, 885, 885, 885, 885, 885
        max_init_bytes=888,
        duration='PT0H9M56.46S',
        min_buffer_time='PT1.500000S',
        num_adaptation_sets=1  # video
)

VIDEO_BBB15_SIMPLE = DASHVideo(
        path_on_server='videos/uniat/DASHDataset2014/BigBuckBunny/15sec/BigBuckBunny_15s_simple_2014_05_09.mpd',
        name='BigBuckBunny_15s_simple_2014_05_09', video_id='bbb15s',
        segment_size=15, segment_amount=40,
        max_width=1920, max_height=1080,
        max_bitrate=3748236,
        min_init_bytes=879,
        # 879, 881, 881, 882, 882, 882, 882, 882, 888, 888, 882, 884, 884, 884, 885, 885, 885, 885, 885, 885
        max_init_bytes=888,
        duration='PT0H9M56.46S',
        min_buffer_time='PT1.500000S',
        num_adaptation_sets=1  # video
)

VIDEO_ENVIVIO = DASHVideo(
        path_on_server='videos/dashif/dash264/TestCases/1c/envivio/manifest.mpd',
        name='Envivio test 1C', video_id='test',
        segment_size=2, segment_amount=13,
        max_width=640, max_height=360,
        max_bitrate=2000000,
        min_init_bytes=1000,
        max_init_bytes=257000,
        duration='PT25.959S',
        min_buffer_time='PT2.001S',
        num_adaptation_sets=2  # video + audio
)

ALL_VIDEOS = {VIDEO_BBB2_OD.id: VIDEO_BBB2_OD,
              VIDEO_BBB2_SIMPLE.id: VIDEO_BBB2_SIMPLE,
              VIDEO_BBB6_SIMPLE.id: VIDEO_BBB6_SIMPLE,
              VIDEO_BBB10_SIMPLE.id: VIDEO_BBB10_SIMPLE,
              VIDEO_BBB15_SIMPLE.id: VIDEO_BBB15_SIMPLE,
              VIDEO_ENVIVIO.id: VIDEO_ENVIVIO}
VIDEO_TEST = VIDEO_ENVIVIO
VIDEO_DEFAULT = VIDEO_BBB2_SIMPLE

##################################
# Single test log folder & files #
##################################
# infos: datetime, video.id, logic, 'm' if mptcp else 't', cong_ctrl, rate0, delay0, rate1, delay1
SINGLE_TEST_FOLDER = '{dt}_{video.id}_{logic}_{mptcp}_{cong_ctrl}' \
                     '_{rates[0]:.2f}Mbps{delays[0]}ms_{rates[1]:.2f}Mbps{delays[1]}ms'
SINGLE_TEST_FOLDER_REVERSE = re.compile('(?P<datetime>\d{6}-\d{4})_(?P<video_id>.+)_(?P<logic>.+)'
                                        '_(?P<mptcp>[mt])_(?P<cong_ctrl>\w+)'
                                        '_(?P<rate0>\d+\.\d{2})Mbps(?P<delay0>\d+(\.\d+)?)ms'
                                        '_(?P<rate1>\d+\.\d{2})Mbps(?P<delay1>\d+(\.\d+)?)ms')
TCPPROBE_SERVER_LOGFILE = '{dt}_server_tcpprobe'
TCPDUMP_LOGFILE = '{dt}_{machine}_any{ext}'

##################################
# Double test log folder & files #
##################################
# infos: datetime, (double), video.id, logic, 'm' if mptcp else 't', cong_ctrl, rate0, delay0, rate1, delay1
DOUBLE_TEST_FOLDER = '{dt}_double_{video.id}_{logic}_{mptcp}_{cong_ctrl}' \
                     '_{rates[0]:.2f}Mbps{delays[0]}ms_{rates[1]:.2f}Mbps{delays[1]}ms'
DOUBLE_TEST_FOLDER_REVERSE = re.compile('(?P<datetime>\d{6}-\d{4})_double_(?P<video_id>.+)_(?P<logic>.+)'
                                        '_(?P<mptcp>[mt])_(?P<cong_ctrl>\w+)'
                                        '_(?P<rate0>\d+\.\d{2})Mbps(?P<delay0>\d+(\.\d+)?)ms'
                                        '_(?P<rate1>\d+\.\d{2})Mbps(?P<delay1>\d+(\.\d+)?)ms')

######################################
# TCP vs MPTCP comparison parameters #
######################################
BW_RATIOS = [0.8, 0.6, 0.5, 0.4, 0.2]
RTT_DELAYS = [25, 50, 100, 150]
RTT_DELAY_VAR = 15

###########
# tcpdump #
###########
TCPDUMP_SNAPLEN = 100  # 1024
