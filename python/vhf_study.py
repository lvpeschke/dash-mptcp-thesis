#!/usr/bin/env python3.5
#
# Script to measure VHF performance.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import collections
import os
import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import helper

# -- LaTeX settings -- #
plt.rc('font', **{'family': 'serif', 'serif': ['Palatino']}) # 'size': 13
plt.rc('text', usetex=True)
plt.rc('legend', fontsize='large')
plt.rc('xtick', labelsize='large')
plt.rc('ytick', labelsize='large')
plt.rc('axes', labelsize='x-large')


class Observations:
    """Class reproducing VLC's VHF."""

    def __init__(self, n):
        self.n = n
        self.obs = collections.deque(maxlen=n)

        self.deltamax = 0.0
        self.diffsums = 0.0
        self.alpha = 0.0

        self.avg = 0.0
        self.i = 0

    def _sd_i(self, previous):
        add = 0
        add += sum(abs(self.obs[i + 1] - self.obs[i]) for i in range(0, len(self.obs) - 1))
        add += abs(previous - self.obs[0])  # add difference between previous and new last
        return add

    def push(self, o):
        # pop the oldest entry if maximum window
        if len(self.obs) == self.n:
            previous = self.obs.popleft()
        else:
            previous = 0
        self.obs.append(o)
        assert o == self.obs[-1]

        self.deltamax = max(self.obs) - min(self.obs)
        self.diffsums = self._sd_i(previous=previous)
        self.alpha = 0.33 * self.deltamax / self.diffsums if self.diffsums > 0 else 0.5
        self.avg = self.alpha * self.avg + (1.0 - self.alpha) * self.obs[-1]

        self.i += 1


def simulation(n, observations):
    """Simulate a VHF evolution with a list of n observations."""

    deltamax = []
    diffsums = []
    alpha = []
    avg = []
    i = []

    my_obj = Observations(n)
    for o in observations:
        my_obj.push(o)
        deltamax.append(my_obj.deltamax)
        diffsums.append(my_obj.diffsums)
        alpha.append(my_obj.alpha)
        avg.append(my_obj.avg)
        i.append(my_obj.i)

    df = pd.DataFrame({'observations': observations,
                       'deltamax': deltamax,
                       'diffsums': diffsums,
                       'alpha': alpha,
                       'avg': avg,
                       'i': i})
    return df


# SETUP
T = 60
N = 10
MIN_O = 1
MAX_O = 20
majorLocatorX = mpl.ticker.MultipleLocator(10)
minorLocatorX = mpl.ticker.MultipleLocator(1)
majorLocatorY = mpl.ticker.MultipleLocator(5)
minorLocatorY = mpl.ticker.MultipleLocator(1)

CASES = [
    ([10] * T, 'constant'),

    (np.linspace(start=MIN_O, stop=MAX_O, num=T, endpoint=True), 'up'),
    (np.linspace(start=5, stop=15, num=T, endpoint=True), 'upp'),

    (np.linspace(start=MAX_O, stop=MIN_O, num=T, endpoint=True), 'down'),
    (np.linspace(start=15, stop=5, num=T, endpoint=True), 'downn'),

    ([5, 15] * int(T / 2), 'cst1'),
    ([5, 5, 5, 15, 15, 15] * int(T / 6), 'cst3'),
    ([5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15] * int(T / 20), 'cst10'),
    ([5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15] * int(
            T / 30), 'cst15'),
    ([10, 12, 14, 16, 14, 12] * int(T / 6), 'lin3'),
    ([5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6] * int(T / 20), 'lin10'),
    ([10, 10, 10, 10, 10, 15] * int(T / 6), 'up6'),
    ([10, 10, 10, 10, 10, 5] * int(T / 6), 'down6'),
    ([10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 15] * int(T / 20), 'up20'),
    ([10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 5] * int(T / 20), 'down20'),
    ([10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
      15, 10, 10, 10, 10, 10, 10, 5, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
      11, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 11, 10, 10, 9, 10, 10, 10, 10, 10], 'nonpersistent'),
]


def plot_simulation(n, observations, directory, fig_name, override=False):
    sim_df = simulation(n, observations)
    print('* sim_df is:')
    print(sim_df)

    with helper.cd(directory):
        if not override and any(os.path.isfile(f) for f in [fig_name + '-obs.pdf',
                                                            fig_name + '-vhf.pdf',
                                                            fig_name + '-alpha.pdf',
                                                            fig_name + '-parts.pdf']):
            print('vhf_study file(s) exist(s) and will not be overridden...')
            return

        # OBSERVATIONS + ESTIMATION
        plt.figure(1)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        sim_df.plot(ax=ax, x='i', y='observations', label=r'$O$', marker='.', color='#2561B2')
        sim_df.plot(ax=ax, x='i', y='avg', label=r'$E$', marker='.', color='#FF6426')
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'$t$')
        ax.xaxis.set_major_locator(majorLocatorX)
        ax.xaxis.set_minor_locator(minorLocatorX)
        ax.set_xlim(0, T)

        ax.set_ylabel(r'unit')
        ax.yaxis.set_major_locator(majorLocatorY)
        ax.yaxis.set_minor_locator(minorLocatorY)
        ax.set_ylim(0, MAX_O)

        ax.grid()
        plt.savefig(fig_name + '-obs.pdf', format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # VHF INTERNAL
        plt.figure(2)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        sim_df.plot(ax=ax, x='i', y='deltamax', label=r'$\Delta_{\max}$', marker='.')
        sim_df.plot(ax=ax, x='i', y='diffsums', label=r'$SD$', marker='.')
        ax.legend(loc='best', frameon=True)

        ax.set_xlabel(r'$t$')
        ax.xaxis.set_major_locator(majorLocatorX)
        ax.xaxis.set_minor_locator(minorLocatorX)
        ax.set_xlim(0, T)

        ax.set_ylabel(r'unit')
        ax.set_ylim(0, MAX_O)

        ax.grid()
        plt.savefig(fig_name + '-vhf.pdf', format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # ALPHA
        plt.figure(3)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        sim_df.plot(ax=ax, x='i', y='alpha', label=r'$\alpha$', marker='.', color='green')
        ax.legend(loc='best', frameon=True)

        ax.set_xlabel(r'$t$')
        ax.xaxis.set_major_locator(majorLocatorX)
        ax.xaxis.set_minor_locator(minorLocatorX)
        ax.set_xlim(0, T)

        ax.set_yticks(np.arange(0, 1.1, 0.1))
        ax.set_ylim(0, 1)

        ax.grid()
        plt.savefig(fig_name + '-alpha.pdf', format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # PARTS
        plt.figure(4)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        # compute
        observation_part = (np.ones(len(sim_df['i'])) - sim_df['alpha']) * sim_df['observations']
        previous_estimation_part = sim_df['avg'] - observation_part

        # plot
        ax.plot(sim_df['i'], observation_part, label=r'$(1-\alpha)O$', marker='.', color='steelblue')
        ax.plot(sim_df['i'], previous_estimation_part, label=r'$\alpha E_{-1}$', marker='.', color='salmon')
        sim_df.plot(ax=ax, x='i', y='avg', label=r'$E$', marker='.', color='#FF6426')
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'$t$')
        ax.xaxis.set_major_locator(majorLocatorX)
        ax.xaxis.set_minor_locator(minorLocatorX)
        ax.set_xlim(0, T)

        ax.set_ylabel(r'unit')
        ax.yaxis.set_major_locator(majorLocatorY)
        ax.yaxis.set_minor_locator(minorLocatorY)
        ax.set_ylim(0, MAX_O)

        ax.grid()
        plt.savefig(fig_name + '-parts.pdf', format='pdf', bbox_inches='tight', transparent=True)
        plt.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="VHF study")
    parser.add_argument('directory',
                        type=str,
                        help='directory where to find the files')
    parser.add_argument('--override',
                        action='store_true',
                        default=False,
                        help='override already existing result folder(s)')
    parser.add_argument('-v', '--version',
                        action='version',
                        version=__file__ + ' version 1 January 2017')
    args = parser.parse_args()

    for obs, figname in CASES:
        plot_simulation(n=N, observations=obs,
                        directory=args.directory, fig_name=figname, override=args.override)
    sys.exit(0)
