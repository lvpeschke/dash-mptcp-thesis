#!/usr/bin/env python3.5
#
# Script to launch a comparison between a TCP baseline and 11 MPTCP setups.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse  # command line arguments and exception handling
import sys
import time

import config
import do_extract_vlc
import do_test
import do_test2
import helper
import vlc

description = """
Compare VLC with TCP and with MPTCP: for a given bandwidth and delay,
test TCP against MPTCP for streaming a video with VLC.
"""
epilog = """
See also: config.py, do_extract_vlc.py, do_test.py, vlc.py
"""


def ratio_test(repetitions, bw_total, cong_ctrl, ratio, delay, delay_var=config.RTT_DELAY_VAR,
               video=config.VIDEO_DEFAULT, logic=vlc.RATE_LOGIC, rival=False,
               directory=config.HOST_PATH_TESTS_COMPARISON, verbose=False):
    """Perform two tests with VLC and MPTCP, varying the delay.

    Given a constant baseline bandwidth and delay, perform 4 tests with MPTCP and two links.
    The link properties (bandwidth, delay), primary & secondary, are:
        - (bw_total * ratio, delay + delay_var) & (bw_total * (1-ratio), delay - delay_var)
        - (bw_total * ratio, delay - delay_var) & (bw_total * (1-ratio), delay + delay_var)
    :param repetitions: how often to repeat each setup for realiability
    :param bw_total: baseline bandwidth
    :param cong_ctrl: TCP/MPTCP congestion control on both VMs
    :param ratio: bandwidth share of the primary link
    :param delay: baseline delay
    :param delay_var: delay variation on both links compared to the baseline delay
    :param video: which video to use
    :param logic: rate or predictive (VLC)
    :param rival: use additional rival client or not
    :param directory: where to store the results
    :param verbose: toggle verbose console output
    :return: a list of result folders, one for each test
    """
    assert bw_total > 0
    assert cong_ctrl in config.CONG_CTRL_ALGOS
    assert delay - abs(delay_var) > 0
    assert 0.0 <= ratio <= 1.0
    assert isinstance(video, config.DASHVideo)
    assert video in config.ALL_VIDEOS.values()
    assert logic in vlc.ADAPTIVE_LOGICS

    folders = list()
    for var in [delay_var, -delay_var]:
        if verbose:
            print('Test case is: link 1 ratio {}, {}ms - link 2 ratio {}, {}ms, {} repetititons'
                  .format(ratio, delay + var, 1.0 - ratio, delay - var, repetitions))
        for _ in range(repetitions):
            if rival:  # double
                func = do_test2.main
            else:  # simple
                func = do_test.main
            folder = func(rates=[bw_total * ratio, bw_total * (1.0 - ratio)],
                          delays=[delay + var, delay - var],
                          link=1, mptcp=1, cong_ctrl=cong_ctrl,
                          video=video, logic=logic,
                          directory=directory, do_extract=False, verbose=verbose)
            folders.append(folder)
            time.sleep(30)

    if verbose:
        print('Test folders ({}) are...'.format(len(folders)), ' '.join(folders))

    return folders


def main(bandwidth, delay, cong_ctrl_tcp, cong_ctrl_mptcp,
         repetitions,
         ratios=config.BW_RATIOS, delay_var=config.RTT_DELAY_VAR,
         logic=vlc.RATE_LOGIC, video=config.VIDEO_DEFAULT, rival=False,
         directory=config.HOST_PATH_TESTS_COMPARISON, do_extract=True, verbose=False):
    assert bandwidth > 0
    assert delay - abs(delay_var) > 0
    assert cong_ctrl_tcp in config.CONG_CTRL_ALGOS
    assert cong_ctrl_mptcp in config.CONG_CTRL_ALGOS
    assert repetitions > 0
    for r in ratios:
        assert 0.0 <= r <= 1.0
    assert logic in vlc.ADAPTIVE_LOGICS
    assert isinstance(video, config.DASHVideo)
    assert video in config.ALL_VIDEOS.values()

    # empty result folder list
    folders = list()

    # TCP test (baseline)
    for _ in range(repetitions):
        # TODO if rival
        if rival:  # double
            func = do_test2.main
        else:  # simple
            func = do_test.main
        full_tcp = func(rates=[bandwidth] * 2, delays=[delay] * 2, link=1, mptcp=0, cong_ctrl=cong_ctrl_tcp,
                        video=video, logic=logic,
                        directory=directory, do_extract=False, verbose=verbose)
        folders.append(full_tcp)
        time.sleep(30)

    # MPTCP tests
    for r in ratios:
        mptcp_case = ratio_test(repetitions=repetitions,
                                bw_total=bandwidth, ratio=r, delay=delay, delay_var=delay_var,
                                cong_ctrl=cong_ctrl_mptcp,
                                video=video, logic=logic, rival=rival,
                                directory=directory, verbose=verbose)
        folders.extend(mptcp_case)

    # Extract data (optional)
    if do_extract:
        for folder in folders:
            do_extract_vlc.main(path=folder)

    if verbose:
        print('End of mptcp_against_tcp, success!')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('bandwidth',
                        type=float,
                        help='bandwidth in Mbps')
    parser.add_argument('delay',
                        type=float,
                        help='RTT delay in ms')
    # TODO add cong ctrl (2 --> TCP & MPTCP)
    parser.add_argument('--ratios',
                        type=float,
                        nargs='+',
                        default=config.BW_RATIOS,
                        help='ratio between link 1 and 2 for mptcp (default=[0.8, 0.6, 0.5, 0.4, 0.2])')
    parser.add_argument('--delay-var',
                        type=float,
                        default=config.RTT_DELAY_VAR,
                        help='delay variation in ms')
    parser.add_argument('--video',
                        type=str,
                        default=config.VIDEO_DEFAULT.id,
                        choices=config.ALL_VIDEOS,
                        help='which video to play (see config.py)')
    parser.add_argument('--logic',
                        type=str,
                        default=vlc.RATE_LOGIC,
                        choices=vlc.ADAPTIVE_LOGICS,
                        help='adaptation logic for VLC')
    parser.add_argument('--repetitions',
                        type=int,
                        default=3,
                        help='number of times a case is tested')
    parser.add_argument('--no-extract',
                        action='store_true',
                        default=False,
                        help='choose not to extract data')
    parser.add_argument('--rival',
                        action='store_true',
                        default=False,
                        help='use 2 clients in parallel')
    parser.add_argument('-v', '--version',
                        action='version',
                        version=__file__ + ' version 20 December 2016')
    args = parser.parse_args()

    if not helper.check_positive_num(args.bandwidth):
        raise ValueError('args.bandwidth < 0')
    if args.ratios and not (0.0 <= r <= 1.0 for r in args.ratios):
        raise ValueError('all ratios must be 0 <= ratio <= 1.0, not {}'.format(args.ratios))
    if not helper.check_positive_num(args.delay - args.delay_var):
        raise ValueError('delay - delay_var must be a positive number')
    if args.repetitions < 1:
        raise ValueError('args.repetitions < 1')

    args.video = config.ALL_VIDEOS.get(args.video)

    do_test.play_and_kill(link=1, video=args.video, seconds=30, verbose=True)  # avoid swapping
    sys.exit((main(bandwidth=args.bandwidth, delay=args.delay,
                   repetitions=args.repetitions,
                   cong_ctrl_tcp=config.CONG_CTRL_TCP_DEFAULT, cong_ctrl_mptcp=config.CONG_CTRL_MPTCP_DEFAULT,
                   ratios=args.ratios, delay_var=args.delay_var,
                   logic=args.logic, video=args.video, rival=args.rival,
                   do_extract=(not args.no_extract), verbose=True)))
