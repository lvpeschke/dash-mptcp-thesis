# VLC internals for DASH.
# To be used with the VLC fork at https://github.com/lvpeschke/vlc.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import glob
import os
import re

import pandas as pd

import helper

RATE_LOGIC = 'rate'
PREDICTIVE_LOGIC = 'predictive'
ADAPTIVE_LOGICS = {RATE_LOGIC, PREDICTIVE_LOGIC} # predictive,rate,fixedrate,lowest,highest
LOGIC_DEFAULT = RATE_LOGIC
VERBOSITY_LEVELS = [0, 1, 2]

# Regex
VLC_VERSION = re.compile('VLC version.*\\n')
VLC_DEBUG_BEFORE_TFE = re.compile('\[[\w\d]+\][\w\d ]+: (?=TFE)')
TFE_ID = 'TFE'
TFE_SEP = ','
# /!\ monitor how many digits (9, 10, 11, 12, 13)
TFEREGEX = re.compile('TFE [\w><=:.!?&_/ ]+, \d{9,13}(, [\w#/?_.+\-\d ]*)*\\n')  # correct log entry
TFEREGEX_NONEWLINE = re.compile('TFE [\w><=:.!?&_/ ]+, \d{9,13}(, [\w#/?_.+\-\d ]*)*')  # log entry with missing \n
TFEREGEX_DESCRIPTION = re.compile('TFE [\w><=:.!?&_/ ]+, ')  # log entry, part 1
TFEREGEX_DATA = re.compile('\d{9,13}(, [\w#/?_.+\-\d ]*)*')  # log entry, part 2

# Errors in the log file.
ERROR_FATALIO = 'fatal IO error'


def find_vlclog():
    """
    Search for a .vlclog file in the current working directory.
    :return: the .vlclog file name, if found and if only one in the path
    :raise TooManyFilesException: if > 1 .vlclog file is found
    """
    vlclog = glob.glob('*{ext}'.format(ext=helper.DOT_VLCLOG))
    if len(vlclog) > 1:
        raise helper.TooManyFilesException('multiple vlclog files detected in {}'.format(os.getcwd()))
    elif len(vlclog) < 1:
        return None
    else:  # len(vlclog) == 1
        return vlclog[0]


class VLCInfo:
    """Class representing an entry in the tfelog."""

    ext = helper.DOT_CSV

    def __init__(self, vlc_file, description, col_headers, file_addition):
        self.vlc_file = vlc_file
        self.description = description
        self.col_no = len(col_headers)
        self.col_headers = col_headers
        self.file_addition = file_addition

    def create_csv_file(self, tfelog, override):
        filename = helper.create_file_from_file(filename=tfelog, dotextension=self.ext, addition=self.file_addition,
                                                override=override)
        with open(filename, 'w') as fd:
            fd.write(', '.join(self.col_headers) + '\n')
        return filename

    def load_data_df(self, directory, verbose=False):
        df = None
        with helper.cd(directory):
            fs = glob.glob('*{}{}'.format(self.file_addition, self.ext))
            if len(fs) == 0:
                raise FileNotFoundError('could not find {} file at {}'.format(self.file_addition, os.getcwd()))
            if len(fs) > 1:
                raise helper.TooManyFilesException('found {} {} files at {}'
                                                   .format(len(fs), self.file_addition, os.getcwd()))
            # if not helper.check_is_file(fs[0]):
            #   raise FileNotFoundError('{} is not a file at {}'.format(fs[0], directory))
            if not os.stat(fs[0]).st_size > 0:
                raise helper.EmptyFileException('{} at {} is empty'.format(fs[0], os.getcwd()))

            df = pd.read_csv(filepath_or_buffer=fs[0], header=0, skipinitialspace=True, verbose=verbose)
        return df


# Metrics captured with the forked VLC

MDATE = 'mdate (micros)'
STREAM_MIME_TYPE = 'stream (mime type)'
STREAM_ID = 'stream (id)'

HTTP_READ = VLCInfo(vlc_file='demux/adaptive/http/HTTPConnection.cpp',
                    description='TFE read HTTP response done, ', # contentLength == bytesRead
                    col_headers=[MDATE,
                                 'content-length (bytes)'],
                    file_addition='vlc_http-read')
HTTP_SEND_SOCKET = VLCInfo(vlc_file='demux/adaptive/http/HTTPConnection.cpp',
                           description='TFE send HTTP on socket, ',
                           col_headers=[MDATE],
                           file_addition='vlc_http-sendsock')
HTTP_REPLYCODE = VLCInfo(vlc_file='demux/adaptive/http/HTTPConnection.cpp',
                         description='TFE HTTP replycode, ',
                         col_headers=[MDATE,
                                      'replycode'],
                         file_addition='vlc_http-replycode')

HTTPCONMAN_UPDATE_RATE = VLCInfo(vlc_file='demux/adaptive/http/HTTPConnectionManager.cpp',
                                 description='TFE updateDownloadRate in HTTPConnectionManager, ',
                                 col_headers=[MDATE],
                                 file_addition='vlc_httpconman-update-rate')
HTTPCONMAN_NEW_CONN = VLCInfo(vlc_file='demux/adaptive/http/HTTPConnectionManager.cpp',
                              description='TFE new connection in HTTPConnectionManager, ',
                              col_headers=[MDATE,
                                           'connection pool size'],
                              file_addition='vlc_httpconman-new-conn')
HTTPCONMAN_REUSE_CONN = VLCInfo(vlc_file='demux/adaptive/http/HTTPConnectionManager.cpp',
                                description='TFE connection reused in HTTPConnectionManager, ',
                                col_headers=[MDATE,
                                             'connection pool size'],
                                file_addition='vlc_httpconman-reuse-conn')

HTTP_NEW_SOCKET = VLCInfo(vlc_file='demux/adaptive/http/Sockets.cpp',
                          description='TFE socket connected, ',
                          col_headers=[MDATE,
                                       'port'],
                          file_addition='vlc_http-socket')
HTTP_NEW_TLSSOCKET = VLCInfo(vlc_file='demux/adaptive/http/Sockets.cpp',
                             description='TFE TLS socket connected, ',
                             col_headers=[MDATE,
                                          'port'],
                             file_addition='vlc_http-tlssocket')

PLOGIC_NUMBER_STREAMS = VLCInfo(vlc_file='demux/adaptive/logic/PredictiveAdaptationLogic.cpp',
                                description='TFE predictive number of streams, ',
                                col_headers=[MDATE,
                                             'streams'],
                                file_addition='vlc_plogic-num-streams')
PLOGIC_HIGHEST1 = VLCInfo(vlc_file='demux/adaptive/logic/PredictiveAdaptationLogic.cpp',
                          description='TFE predictive streams end rep highest, ',
                          col_headers=[MDATE,
                                       STREAM_ID,
                                       STREAM_MIME_TYPE],
                          file_addition='vlc_plogic-highest1')
PLOGIC_HIGHEST2 = VLCInfo(vlc_file='demux/adaptive/logic/PredictiveAdaptationLogic.cpp',
                          description='TFE predictive stats starting rep highest, ',
                          col_headers=[MDATE,
                                       STREAM_ID,
                                       STREAM_MIME_TYPE],
                          file_addition='vlc_plogic-highest2')
PLOGIC_STATS = VLCInfo(vlc_file='demux/adaptive/logic/PredictiveAdaptationLogic.cpp',
                       description='TFE predictive stats, ',
                       col_headers=[MDATE,
                                    STREAM_ID,
                                    STREAM_MIME_TYPE,
                                    'current buffer level (micros)',
                                    'target buffer level (micros)',
                                    'ratio buffer level',
                                    'mininmum buffer level (micros)',
                                    'maximum bitrate (bps)'],
                       file_addition='vlc_plogic-stats')
PLOGIC_BPS_AVAIL = VLCInfo(vlc_file='demux/adaptive/logic/PredictiveAdaptationLogic.cpp',
                           description='TFE predictive availableBw, ',
                           col_headers=[MDATE,
                                        'available bandwidth (bps)'],
                           file_addition='vlc_plogic-bps-avail')
PLOGIC_BPS_USED = VLCInfo(vlc_file='demux/adaptive/logic/PredictiveAdaptationLogic.cpp',
                          description='TFE predictive bandwidth usage bps, ',
                          col_headers=[MDATE,
                                       STREAM_ID,
                                       STREAM_MIME_TYPE,
                                       'representation (bps)'],
                          file_addition='vlc_plogic-bps-used')
PLOGIC_UPDATE_RATE_INPUT = VLCInfo(vlc_file='demux/adaptive/logic/PredictiveAdaptationLogic.cpp',
                                   description='TFE predictive update last download rate input, ',
                                   col_headers=[MDATE,
                                                STREAM_ID,
                                                'input time (micros)',
                                                'input size (Bytes)'],
                                   file_addition='vlc_plogic-update-rate-input')
PLOGIC_UPDATE_RATE = VLCInfo(vlc_file='demux/adaptive/logic/PredictiveAdaptationLogic.cpp',
                             description='TFE predictive update last download rate, ',
                             col_headers=[MDATE,
                                          STREAM_ID,
                                          'download rate (bps)'],
                             file_addition='vlc_plogic-update-rate')
PLOGIC_SWITCH = VLCInfo(vlc_file='demux/adaptive/logic/PredictiveAdaptationLogic.cpp',
                        description='TFE predictive new bps, ',
                        col_headers=[MDATE,
                                     STREAM_ID,
                                     STREAM_MIME_TYPE,
                                     'bandwidth usage (bps)'],
                        file_addition='vlc_plogic-segtracker-switch')  # segment tracker
PLOGIC_BUFSTATE = VLCInfo(vlc_file='demux/adaptive/logic/PredictiveAdaptationLogic.cpp',
                          description='TFE predictive SegmentTrackerEvent BUFFERING_STATE bool, ',
                          col_headers=[MDATE,
                                       STREAM_ID,
                                       'buffer enabled (bool)'], # segment tracker
                          file_addition='vlc_plogic-segtracker-bufstate')
PLOGIC_BUFLEVEL = VLCInfo(vlc_file='demux/adaptive/logic/PredictiveAdaptationLogic.cpp',
                          description='TFE predictive SegmentTrackerEvent BUFFERING_LEVEL_CHANGE, ',
                          col_headers=[MDATE,
                                       STREAM_ID,
                                       'current buffer level (micros)',
                                       'target buffer level (micros)'],
                          file_addition='vlc_plogic-segtracker-buflevel')  # segment tracker

RBLOGIC_REPRESENTATION = VLCInfo(vlc_file='demux/adaptive/logic/RateBasedAdaptionLogic.cpp',
                                 description='TFE rblogic base representation, ',
                                 col_headers=[MDATE,
                                              STREAM_ID,
                                              STREAM_MIME_TYPE,
                                              'representation (bps)'],
                                 file_addition='vlc_rblogic-representation')
RBLOGIC_UPDATE_RATE_INPUT = VLCInfo(vlc_file='demux/adaptive/logic/RateBasedAdaptionLogic.cpp',
                                    description='TFE rblogic update download rate input, ',
                                    col_headers=[MDATE,
                                                 'input time (micros)',
                                                 'input size (Bytes)',
                                                 'observation window time (micros)',
                                                 'observation window size (Bytes)',
                                                 'observation window threshold (micros)'],
                                    file_addition='vlc_rblogic-update-rate-input')
RBLOGIC_UPDATE_RATE = VLCInfo(vlc_file='demux/adaptive/logic/RateBasedAdaptionLogic.cpp',
                              description='TFE rblogic update download rate, ',
                              col_headers=[MDATE,
                                           'observed bandwidth (bps)',
                                           'VHF bandwidth (bps)',
                                           'current bandwidth threshold (bps)',
                                           'previous representation (bps)'],
                              file_addition='vlc_rblogic-update-rate')
RBLOGIC_SWITCH = VLCInfo(vlc_file='demux/adaptive/logic/RateBasedAdaptionLogic.cpp',
                         description='TFE rblogic new bps, ',
                         col_headers=[MDATE,
                                      STREAM_ID,
                                      STREAM_MIME_TYPE,
                                      'representation (bps)'],
                         file_addition='vlc_rblogic-segtracker-switch')  # segtracker
RBLOGIC_BUFSTATE = VLCInfo(vlc_file='demux/adaptive/logic/RateBasedAdaptionLogic.cpp',
                           description='TFE rblogic BUFFERING_STATE bool, ',
                           col_headers=[MDATE,
                                        STREAM_ID,
                                        'buffer enabled (bool)'],
                           file_addition='vlc_rblogic-segtracker-bufstate')  # segtracker
RBLOGIC_BUFLEVEL = VLCInfo(vlc_file='demux/adaptive/logic/RateBasedAdaptionLogic.cpp',
                           description='TFE rblogic BUFFERING_LEVEL_CHANGE, ',
                           col_headers=[MDATE,
                                        STREAM_ID,
                                        'current buffer level (micros)',
                                        'target buffer level (micros)'],
                           file_addition='vlc_rblogic-segtracker-buflevel')  # segtracker

MOVAVG = VLCInfo(vlc_file='demux/adaptive/tools/MovingAverage.hpp',
                 description='TFE moving average push, ',
                 col_headers=[MDATE,
                              'diffsums',
                              'deltamax',
                              'alpha',
                              'avg'],
                 file_addition='vlc_movavg')

START = VLCInfo(vlc_file='demux/adaptive/PlaylistManager.cpp',
                description='TFE new playlist manager, ',
                col_headers=[MDATE],
                file_addition='vlc_start')  # PlaylistManager::PlaylistManager()
STOP = VLCInfo(vlc_file='demux/adaptive/PlaylistManager.cpp',
               description='TFE delete playlist manager, ',
               col_headers=[MDATE],
               file_addition='vlc_stop')  # PlaylistManager::~PlaylistManager()
PLAYBACK_TIME = VLCInfo(vlc_file='demux/adaptive/PlaylistManager.cpp',
                        description='TFE PlayListManager getCurrentPlaybackTime, ',
                        col_headers=[MDATE, 'playback time (micros)'],
                        file_addition='vlc_playback-time')  # PlaylistManager::getCurrentPlaybackTime()

STREAM_INFO = VLCInfo(vlc_file='demux/adaptive/Streams.cpp',
                      description='TFE stream, ',
                      col_headers=[MDATE,
                                   'description',
                                   'pcr (micros)',
                                   'dts (micros)',
                                   'buffer level (micros)',
                                   'nz deadline (micros)', # ?
                                   'demuxed amount (micros)'], # = bufferinglevel - getFirstDTS
                      file_addition='vlc_stream')


# DASHManager is useless
# DASHMAN_SCHEDULE_MINBUF = VLCInfo(vlc_file='demux/dash/DASHManager.cpp',
#                                   description='TFE DASHManager scheduleNextUpdate real minbuffer, ',
#                                   col_headers=[MDATE,
#                                                'minimum buffer level (micros)'], # stream->getMinAheadTime() ... rep.
#                                   file_addition='vlc_dashman-schedule-minbuf')
# DASHMAN_SCHEDULE = VLCInfo(vlc_file='demux/dash/DASHManager.cpp',
#                            description='TFE DASHManager scheduleNextUpdate, ',
#                            col_headers=[MDATE,
#                                         'now (micros)',
#                                         'next playlist update (micros)'],  # timestamp, not relative
#                            file_addition='vlc_dashman-schedule')
# DASHMAN_UPDATE = VLCInfo(vlc_file='demux/dash/DASHManager.cpp',
#                          description='TFE DASHManager updatePlaylist, ',
#                          col_headers=[MDATE],
#                          file_addition='vlc_dashman-update')  # if nextPlaylistupdate


class VLCInfoSet:
    """Set of VLCInfo objects."""

    def __init__(self):
        self.set = set()

    def __len__(self):
        return len(self.set)

    def __iter__(self):
        return iter(self.set)

    def __contains__(self, item):
        return self.set.__contains__(item)

    def __repr__(self):
        return repr(self.set)

    def add(self, vlc_info):
        if isinstance(vlc_info, (list, set, dict)):
            if not all(isinstance(e, VLCInfo) for e in vlc_info):
                raise TypeError('only VLCInfo objects can be added to a VLCInfoSet')
            self.set.update(vlc_info)

        else:
            if not isinstance(vlc_info, VLCInfo):
                raise TypeError('only VLCInfo objects can be added to a VLCInfoSet, found: {}'.format(type(vlc_info)))
            self.set.add(vlc_info)


CURRENT_VLCINFOSET = VLCInfoSet()
CURRENT_VLCINFOSET.add({
    HTTP_READ,
    HTTP_SEND_SOCKET,
    HTTP_REPLYCODE,

    HTTPCONMAN_UPDATE_RATE,
    HTTPCONMAN_NEW_CONN,
    HTTPCONMAN_REUSE_CONN,

    HTTP_NEW_SOCKET,
    HTTP_NEW_TLSSOCKET,

    PLOGIC_NUMBER_STREAMS,
    PLOGIC_HIGHEST1,
    PLOGIC_HIGHEST2,
    PLOGIC_STATS,
    PLOGIC_BPS_AVAIL,
    PLOGIC_BPS_USED,
    PLOGIC_UPDATE_RATE_INPUT,
    PLOGIC_UPDATE_RATE,
    PLOGIC_SWITCH,
    PLOGIC_BUFSTATE,
    PLOGIC_BUFLEVEL,

    RBLOGIC_REPRESENTATION,
    RBLOGIC_UPDATE_RATE_INPUT,
    RBLOGIC_UPDATE_RATE,
    RBLOGIC_SWITCH,
    RBLOGIC_BUFSTATE,
    RBLOGIC_BUFLEVEL,

    MOVAVG,

    START,
    STOP,
    PLAYBACK_TIME,

    STREAM_INFO,

    # DASHMAN_SCHEDULE_MINBUF,
    # DASHMAN_SCHEDULE,
    # DASHMAN_UPDATE
})


class VLCNoStopInLog(Exception):
    pass
