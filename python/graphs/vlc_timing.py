#!/usr/bin/env python3.5
#
# Compute and save VLC timing metrics.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import helper
import plot_test
import vlc


def timing(data_dir, plot_dir, start, stop, override=False, verbose=False):
    with helper.cd(plot_dir):
        # Check if files exist
        file_time = '{name}{ext}'.format(name='vlc_playback-time', ext=helper.DOT_CSV)
        figure_time = '{name}{ext}'.format(name='vlc_playback-time', ext=helper.DOT_PDF)
        # file_stalls = '{name}{ext}'.format(name='vlc_playback-time-stalls', ext=common.DOT_CSV)
        # figure_stalls = '{name}{ext}'.format(name='vlc_playback-time-stalls', ext=common.DOT_PDF)
        if not override and any(os.path.isfile(f) for f in [file_time, figure_time]): #, file_stalls, figure_stalls]):
            print('timing file(s) exist(s) and will not be overridden...')
            return

        # Get data
        # df0 = vlc.load_data_df(directory=data_dir, vlc_info=vlc.PLAYBACK_TIME, verbose=verbose)
        df0 = vlc.PLAYBACK_TIME.load_data_df(directory=data_dir, verbose=verbose)

        # Compute
        time_index = plot_test.convert(vector=df0[vlc.MDATE],
                                       to_subtract=start,
                                       to_multiply=helper.MICRO)
        # delta_time = time_index.diff()[1:]
        # delta_playback_time = plot_test.convert(vector=df0['playback time (micros)'],
        #                                         to_subtract=0, to_multiply=plot_test.MICRO).diff()[1:]
        data = {'index': time_index,
                r'time vs time': time_index,
                r'playback time': plot_test.convert(vector=df0['playback time (micros)'],
                                                    to_subtract=0, to_multiply=helper.MICRO)
                }

        # data_stalls = {'index1': time_index[:-1].tolist(),
        #                'index2': time_index[1:].tolist(),
        #                r'delta time': delta_time.tolist(),
        #                r'delta playback time': delta_playback_time.tolist(),
        #                r'delta ratio': delta_time.div(delta_playback_time.tolist())}

        df = pd.DataFrame(data=data)
        # df_stalls = pd.DataFrame.from_dict(data=data_stalls)

        # Print csv to file
        df.to_csv(path_or_buf=file_time)
        # df_stalls.to_csv(path_or_buf=file_stalls)

        # Plot pdf to file
        plt.figure()
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        df.plot(x='index', style='--', ax=ax)

        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'time (s)')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
            ax.set_yticks(np.arange(0, 660, 60))
        ax.grid()

        plt.savefig(figure_time, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()
