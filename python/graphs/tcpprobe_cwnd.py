#!/usr/bin/env python3.5
#
# Compute and save congestion window metrics.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import helper
from tcpprobe import TCPProbe

LINK1_CLIENT_80 = '[::ffff:10.0.11.42]:80'
LINK2_CLIENT_80 = '[::ffff:10.0.22.42]:80'


def cwnd_ssthresh(data_dir, plot_dir, override=False, verbose=False):
    with helper.cd(os.path.join(data_dir, plot_dir)):
        # Check if files exist
        file_link1 = 'tcpprobe_link1_cwnd_ssthresh{ext}'.format(ext=helper.DOT_CSV)
        figure_link1 = 'tcpprobe_link1_cwnd_ssthresh{ext}'.format(ext=helper.DOT_PDF)
        file_link2 = 'tcpprobe_link2_cwnd_ssthresh{ext}'.format(ext=helper.DOT_CSV)
        figure_link2 = 'tcpprobe_link2_cwnd_ssthresh{ext}'.format(ext=helper.DOT_PDF)
        file_links_stats = 'tcpprobe_links_cwnd_ssthresh_stats{ext}'.format(ext=helper.DOT_CSV)
        # figure_links_stats = 'tcpprobe_links_cwnd_ssthresh_stats{ext}'.format(ext=common.DOT_PDF)
        file_link1_ports_stats = 'tcpprobe_link1_cwnd_ports_stats{ext}'.format(ext=helper.DOT_CSV)
        figure_link1_ports_stats_bytes = 'tcpprobe_link1_cwnd_ports_stats-bytes{ext}'.format(ext=helper.DOT_PDF)
        figure_link1_ports_stats_duration = 'tcpprobe_link1_cwnd_ports_stats-duration{ext}'.format(ext=helper.DOT_PDF)
        file_link2_ports_stats = 'tcpprobe_link2_cwnd_ports_stats{ext}'.format(ext=helper.DOT_CSV)
        figure_link2_port_stats_bytes = 'tcpprobe_link2_cwnd_ports_stats-bytes{ext}'.format(ext=helper.DOT_PDF)
        figure_link2_port_stats_duration = 'tcpprobe_link2_cwnd_ports_stats-duration{ext}'.format(ext=helper.DOT_PDF)
        if not override and any(os.path.isfile(f) for f in [file_link1, figure_link1, file_link2, figure_link2,
                                                            file_links_stats,
                                                            file_link1_ports_stats,
                                                            figure_link1_ports_stats_bytes,
                                                            figure_link1_ports_stats_duration,
                                                            file_link2_ports_stats,
                                                            figure_link2_port_stats_bytes,
                                                            figure_link1_ports_stats_duration]):
            print('tcpprobe cwnd/ssthresh result file(s) exist(s) and will not be overridden...')
            return

        # Get data
        df0_tcp_probe = TCPProbe.load_data_df(directory=data_dir, verbose=verbose)

        # Separate into the 2 different links
        link1 = df0_tcp_probe[df0_tcp_probe[TCPProbe.col_source] == LINK1_CLIENT_80]  # all data over link1
        link2 = df0_tcp_probe[df0_tcp_probe[TCPProbe.col_source] == LINK2_CLIENT_80]  # all data over link2

        # Compute
        data_link1 = {'time': link1[TCPProbe.col_timestamp],
                      r'daddr:port': link1[TCPProbe.col_destination],
                      r'cwnd on link 1': link1[TCPProbe.col_snd_cwnd],
                      r'ssthresh on link 1': link1[TCPProbe.col_ssthresh],
                      r'bytes on link 1': link1[TCPProbe.col_bytes]}
        data_link2 = {'time': link2[TCPProbe.col_timestamp],
                      r'daddr:port': link2[TCPProbe.col_destination],
                      r'cwnd on link 2': link2[TCPProbe.col_snd_cwnd],
                      r'ssthresh on link 2': link2[TCPProbe.col_ssthresh],
                      r'bytes on link 2': link2[TCPProbe.col_bytes]}
        data_links_stats = {'links_all cwnd': df0_tcp_probe[TCPProbe.col_snd_cwnd].describe(),
                            'link1 cwnd': link1[TCPProbe.col_snd_cwnd].describe(),
                            'link2 cwnd': link2[TCPProbe.col_snd_cwnd].describe(),
                            'links_all ssthresh': df0_tcp_probe[TCPProbe.col_ssthresh].describe(),
                            'link1 ssthresh': link1[TCPProbe.col_ssthresh].describe(),
                            'link2 ssthresh': link2[TCPProbe.col_ssthresh].describe(),
                            'links_all bytes': df0_tcp_probe[TCPProbe.col_bytes].describe(),
                            'link1 bytes': link1[TCPProbe.col_bytes].describe(),
                            'link2 bytes': link2[TCPProbe.col_bytes].describe()}

        data_link1_stats_per_port = list()
        for idx, port in enumerate(link1[TCPProbe.col_destination].unique()):
            link1_portx = link1[link1[TCPProbe.col_destination] == port]
            link1_portx_stats = link1_portx[TCPProbe.col_snd_cwnd].describe()
            link1_portx_stats.name = port
            link1_portx_stats['bytes'] = link1_portx[TCPProbe.col_bytes].sum()
            timestamp_start = link1_portx[TCPProbe.col_timestamp].iat[0]
            timestamp_stop = link1_portx[TCPProbe.col_timestamp].iat[-1]
            link1_portx_stats['duration (micros)'] = timestamp_stop - timestamp_start
            data_link1_stats_per_port.append(link1_portx_stats)
            del link1_portx
            del link1_portx_stats

        data_link2_stats_per_port = list()
        for idx, port in enumerate(link2[TCPProbe.col_destination].unique()):
            link2_portx = link2[link2[TCPProbe.col_destination] == port]
            link2_portx_stats = link2_portx[TCPProbe.col_snd_cwnd].describe()
            link2_portx_stats.name = port
            link2_portx_stats['bytes'] = link2_portx[TCPProbe.col_bytes].sum()
            timestamp_start = link2_portx[TCPProbe.col_timestamp].iat[0]
            timestamp_stop = link2_portx[TCPProbe.col_timestamp].iat[-1]
            link2_portx_stats['duration (micros)'] = timestamp_stop - timestamp_start
            data_link2_stats_per_port.append(link2_portx_stats)
            del link2_portx
            del link2_portx_stats

        # Create dfs
        df_link1 = pd.DataFrame(data=data_link1)
        df_link2 = pd.DataFrame(data=data_link2)
        df_links_stats = pd.DataFrame(data=data_links_stats)
        df_link1_ports_stats = pd.DataFrame()
        if len(data_link1_stats_per_port) > 0:
            df_link1_ports_stats = df_link1_ports_stats.append(data_link1_stats_per_port)
        df_link2_ports_stats = pd.DataFrame()
        if len(data_link2_stats_per_port) > 0:
            df_link2_ports_stats = df_link2_ports_stats.append(data_link2_stats_per_port)

        # Print csv to file
        df_link1.to_csv(path_or_buf=file_link1)
        df_link2.to_csv(path_or_buf=file_link2)
        df_links_stats.to_csv(path_or_buf=file_links_stats)
        df_link1_ports_stats.to_csv(path_or_buf=file_link1_ports_stats)
        df_link2_ports_stats.to_csv(path_or_buf=file_link2_ports_stats)

        # Plot pdf to file
        for n, df_link, df_ports, fig_link, fig_ports_bytes, fig_ports_duration in \
                zip([1, 2], [df_link1, df_link2], [df_link1_ports_stats, df_link2_ports_stats],
                    [figure_link1, figure_link2],
                    [figure_link1_ports_stats_bytes, figure_link2_port_stats_bytes],
                    [figure_link1_ports_stats_duration, figure_link2_port_stats_duration]):
            if df_ports is not None and not df_ports.empty:
                print('n is', n)

                conns = len(df_ports.index)
                if verbose:
                    print('For link {n}, found {unique} different destination ports...'.format(n=n, unique=conns))

                # if conns < 2:
                #     print('Something weird happened in ' + __file__)
                #     return -1

                # First case: only 1 or 2 connections
                if conns == 1 or conns == 2:
                    # Plot evolution of cwnd
                    # f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
                    plt.figure(n)
                    ax = plt.gca()
                    ax.set_clip_on(False)

                    if conns == 2:
                        daddr1 = df_link[r'daddr:port'].unique()[0]  # mpd connection
                        daddr2 = df_link[r'daddr:port'].unique()[1]  # streaming connection
                    else:
                        daddr2 = df_link[r'daddr:port'].unique()[0]  # streaming connection
                    # print('uniques', df_link[TCPProbe.col_destination].unique())  # delete

                    max_ssthresh = int(round(
                            max(df_link[df_link[TCPProbe.col_destination] == daddr2][
                                    r'ssthresh on link {}'.format(n)])))
                    max_cwnd = int(
                            round(max(df_link[df_link[TCPProbe.col_destination] == daddr2][
                                          r'cwnd on link {}'.format(n)])))

                    df_link[df_link[TCPProbe.col_destination] == daddr2].plot(ax=ax, x='time',
                                                                              y=r'ssthresh on link {}'.format(n),
                                                                              label=r'ssthresh on link {} (max={})'.format(
                                                                                      n, max_ssthresh),
                                                                              color='red')
                    df_link[df_link[TCPProbe.col_destination] == daddr2].plot(ax=ax, x='time',
                                                                              y=r'cwnd on link {}'.format(n),
                                                                              label='cwnd on link {} (max={})'.format(
                                                                                      n, max_cwnd), )
                    ax.legend(loc='best', frameon=True, title=r'1 connection')

                    ax.set_xlabel(r'time (s)')
                    ax.set_ylabel(r'segments')
                    ax.set_xlim(xmin=0)
                    ax.set_ylim(ymin=0)
                    if df_link[df_link[r'daddr:port'] == daddr2]['time'].max() < 660:
                        ax.set_xticks(np.arange(0, 660, 60))
                    for lim in [100, 150, 500, 800]:  # FIXME choose intervals
                        if df_link[df_link[r'daddr:port'] == daddr2][r'cwnd on link {}'.format(n)].max() < lim:
                            ax.set_ylim(ymax=lim)
                            break
                    ax.grid()

                    plt.savefig(fig_link, format='pdf', bbox_inches='tight', transparent=True)
                    plt.close()

                    # ZOOM 1
                    plt.figure(n)
                    ax = plt.gca()
                    ax.set_clip_on(False)

                    max_ssthresh = int(round(
                            max(df_link[df_link[TCPProbe.col_destination] == daddr2][
                                    r'ssthresh on link {}'.format(n)])))
                    max_cwnd = int(
                            round(max(df_link[df_link[TCPProbe.col_destination] == daddr2][
                                          r'cwnd on link {}'.format(n)])))

                    ax.scatter(x=df_link[df_link[TCPProbe.col_destination] == daddr2]['time'],
                               y=df_link[df_link[TCPProbe.col_destination] == daddr2][r'ssthresh on link {}'.format(n)],
                               label=r'ssthresh on link {} (max={})'.format(n, max_ssthresh),
                               marker='+', color='red')
                    ax.scatter(x=df_link[df_link[TCPProbe.col_destination] == daddr2]['time'],
                               y=df_link[df_link[TCPProbe.col_destination] == daddr2][r'cwnd on link {}'.format(n)],
                               label='cwnd on link {} (max={})'.format(n, max_cwnd),
                               marker='+')
                    ax.legend(loc='best', frameon=True, title=r'1 connection (zoom)')

                    ax.set_xlabel(r'time (s)')
                    ax.set_ylabel(r'segments')
                    ax.set_ylim(ymin=0)
                    ax.set_xticks(np.arange(0, 65, 5))
                    ax.set_xlim(0, 60)
                    for lim in [100, 150, 500, 800]:  # FIXME choose intervals
                        if df_link[df_link[r'daddr:port'] == daddr2][r'cwnd on link {}'.format(n)].max() < lim:
                            ax.set_ylim(ymax=lim)
                            break
                    ax.grid()

                    plt.savefig('tcpprobe_link{}_cwnd_ssthresh_zoom1.pdf'.format(n), format='pdf', bbox_inches='tight',
                                transparent=True)
                    plt.close()

                    # ZOOM 2
                    plt.figure(n)
                    ax = plt.gca()
                    ax.set_clip_on(False)

                    max_ssthresh = int(round(
                            max(df_link[df_link[TCPProbe.col_destination] == daddr2][
                                    r'ssthresh on link {}'.format(n)])))
                    max_cwnd = int(
                            round(max(df_link[df_link[TCPProbe.col_destination] == daddr2][
                                          r'cwnd on link {}'.format(n)])))

                    ax.scatter(x=df_link[df_link[TCPProbe.col_destination] == daddr2]['time'],
                               y=df_link[df_link[TCPProbe.col_destination] == daddr2][r'ssthresh on link {}'.format(n)],
                               label=r'ssthresh on link {} (max={})'.format(n, max_ssthresh),
                               marker='+', color='red')
                    ax.scatter(x=df_link[df_link[TCPProbe.col_destination] == daddr2]['time'],
                               y=df_link[df_link[TCPProbe.col_destination] == daddr2][r'cwnd on link {}'.format(n)],
                               label='cwnd on link {} (max={})'.format(n, max_cwnd),
                               marker='+')
                    ax.legend(loc='best', frameon=True, title=r'1 connection (zoom)')

                    ax.set_xlabel(r'time (s)')
                    ax.set_ylabel(r'segments')
                    ax.set_ylim(ymin=0)
                    ax.set_xticks(np.arange(0, 365, 5))
                    ax.set_xlim(300, 360)
                    for lim in [100, 150, 500, 800]:  # FIXME choose intervals
                        if df_link[df_link[r'daddr:port'] == daddr2][r'cwnd on link {}'.format(n)].max() < lim:
                            ax.set_ylim(ymax=lim)
                            break
                    ax.grid()

                    plt.savefig('tcpprobe_link{}_cwnd_ssthresh_zoom2.pdf'.format(n), format='pdf', bbox_inches='tight',
                                transparent=True)
                    plt.close()

                    # ZOOM 3
                    plt.figure(n)
                    ax = plt.gca()
                    ax.set_clip_on(False)

                    max_ssthresh = int(round(
                            max(df_link[df_link[TCPProbe.col_destination] == daddr2][
                                    r'ssthresh on link {}'.format(n)])))
                    max_cwnd = int(
                            round(max(df_link[df_link[TCPProbe.col_destination] == daddr2][
                                          r'cwnd on link {}'.format(n)])))

                    ax.scatter(x=df_link[df_link[TCPProbe.col_destination] == daddr2]['time'],
                               y=df_link[df_link[TCPProbe.col_destination] == daddr2][r'ssthresh on link {}'.format(n)],
                               label=r'ssthresh on link {} (max={})'.format(n, max_ssthresh),
                               marker='+', color='red')
                    ax.scatter(x=df_link[df_link[TCPProbe.col_destination] == daddr2]['time'],
                               y=df_link[df_link[TCPProbe.col_destination] == daddr2][r'cwnd on link {}'.format(n)],
                               label='cwnd on link {} (max={})'.format(n, max_cwnd),
                               marker='+')
                    ax.legend(loc='best', frameon=True, title=r'1 connection (zoom)')

                    ax.set_xlabel(r'time (s)')
                    ax.set_ylabel(r'segments')
                    ax.set_ylim(ymin=0)
                    ax.set_xticks(np.arange(0, 450, 5))
                    ax.set_xlim(360, 420)
                    for lim in [100, 150, 500, 800]:  # FIXME choose intervals
                        if df_link[df_link[r'daddr:port'] == daddr2][r'cwnd on link {}'.format(n)].max() < lim:
                            ax.set_ylim(ymax=lim)
                            break
                    ax.grid()

                    plt.savefig('tcpprobe_link{}_cwnd_ssthresh_zoom3.pdf'.format(n), format='pdf', bbox_inches='tight',
                                transparent=True)
                    plt.close()

                # Second case: > 2 connections
                else:
                    # Plot evolution of cwnd
                    plt.figure(n)
                    ax = plt.gca()
                    ax.set_clip_on(False)

                    max_ssthresh = int(round(max(df_link[r'ssthresh on link {}'.format(n)])))
                    max_cwnd = int(round(max(df_link[r'cwnd on link {}'.format(n)])))

                    df_link.plot(ax=ax, x='time', y=r'ssthresh on link {}'.format(n),
                                 label=r'ssthresh on link {} (max={})'.format(n, max_ssthresh),
                                 color='red')
                    df_link.plot(ax=ax, x='time', y=r'cwnd on link {}'.format(n),
                                 label='cwnd on link {} (max={})'.format(n, max_cwnd))
                    ax.legend(loc='best', frameon=True,
                              title=r'{} connections'.format(df_link[TCPProbe.col_destination].nunique()))
                    ax.set_xlabel(r'time (s)')
                    ax.set_ylabel(r'segments')
                    ax.set_xlim(xmin=0)
                    ax.set_ylim(ymin=0)
                    ax.set_xticks(np.arange(0, 660, 60))
                    for lim in [100, 150, 500, 800]:  # FIXME choose intervals
                        if df_link[r'cwnd on link {}'.format(n)].max() < lim:
                            ax.set_ylim(ymax=lim)
                        break
                    ax.grid()

                    plt.savefig(fig_link, format='pdf', bbox_inches='tight', transparent=True)
                    plt.close()

                    # ZOOM 1
                    plt.figure(n)
                    ax = plt.gca()
                    ax.set_clip_on(False)

                    ax.scatter(x=df_link['time'],
                               y=df_link[r'ssthresh on link {}'.format(n)],
                               label=r'ssthresh on link {} (max={})'.format(n, max_ssthresh),
                               marker='+', color='red')
                    ax.scatter(x=df_link['time'],
                               y=df_link[r'cwnd on link {}'.format(n)],
                               label='cwnd on link {} (max={})'.format(n, max_cwnd),
                               marker='+')
                    ax.legend(loc='best', frameon=True,
                              title=r'{} connections (zoom)'.format(df_link[TCPProbe.col_destination].nunique()))
                    # ax1.legend(loc='best', frameon=True)
                    # ax2.legend(loc='best', frameon=True)

                    ax.set_xlabel(r'time (s)')
                    ax.set_ylabel(r'segments')
                    ax.set_ylim(ymin=0)
                    ax.set_xticks(np.arange(0, 65, 5))
                    ax.set_xlim(0, 60)
                    for lim in [100, 150, 500, 800]:  # FIXME choose intervals
                        if df_link[r'cwnd on link {}'.format(n)].max() < lim:
                            ax.set_ylim(ymax=lim)
                            break
                    ax.grid()

                    plt.savefig('tcpprobe_link{}_cwnd_ssthresh_zoom1.pdf'.format(n), format='pdf', bbox_inches='tight',
                                transparent=True)
                    plt.close()

                    # ZOOM 2
                    plt.figure(n)
                    ax = plt.gca()
                    ax.set_clip_on(False)

                    ax.scatter(x=df_link['time'],
                               y=df_link[r'ssthresh on link {}'.format(n)],
                               label=r'ssthresh on link {} (max={})'.format(n, max_ssthresh),
                               marker='+', color='red')
                    ax.scatter(x=df_link['time'],
                               y=df_link[r'cwnd on link {}'.format(n)],
                               label='cwnd on link {} (max={})'.format(n, max_cwnd),
                               marker='+')
                    ax.legend(loc='best', frameon=True,
                              title=r'{} connections (zoom)'.format(df_link[TCPProbe.col_destination].nunique()))

                    ax.set_xlabel(r'time (s)')
                    ax.set_ylabel(r'segments')
                    ax.set_ylim(ymin=0)
                    ax.set_xticks(np.arange(0, 365, 5))
                    ax.set_xlim(300, 360)
                    for lim in [100, 150, 500, 800]:  # FIXME choose intervals
                        if df_link[r'cwnd on link {}'.format(n)].max() < lim:
                            ax.set_ylim(ymax=lim)
                            break
                    ax.grid()

                    # ax.set_title(r'Link {}'.format(n))
                    plt.savefig('tcpprobe_link{}_cwnd_ssthresh_zoom2.pdf'.format(n), format='pdf', bbox_inches='tight',
                                transparent=True)
                    plt.close()

    print('made it to the end of cwnd/ssthresh')
