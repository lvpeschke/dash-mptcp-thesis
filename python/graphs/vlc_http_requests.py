#!/usr/bin/env python3.5
#
# Compute and save VLC request metrics.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import statsmodels.api as sm
from pylab import MaxNLocator

import helper
import plot_test
import vlc


def requests(data_dir, plot_dir, start, stop, seg_size, seg_amount, max_bitrate, override=False, verbose=False):
    with helper.cd(plot_dir):
        # Check if files exist
        file_goodput = '{name}{ext}'.format(name='vlc_http-goodput', ext=helper.DOT_CSV)
        figure_goodput = '{name}{ext}'.format(name='vlc_http-goodput', ext=helper.DOT_PDF)
        file_payload = '{name}{ext}'.format(name='vlc_http-payload', ext=helper.DOT_CSV)
        figure_payload = '{name}{ext}'.format(name='vlc_http-payload', ext=helper.DOT_PDF)
        figure_payload_cdf = '{name}{ext}'.format(name='vlc_http-payload-cdf', ext=helper.DOT_PDF)  # TODO keep?
        figure_payload_dist = '{name}{ext}'.format(name='vlc_http-payload-dist', ext=helper.DOT_PDF)  # TODO keep?
        file_payload_mycdf = '{name}{ext}'.format(name='vlc_http-payload-mycdf', ext=helper.DOT_CSV)
        figure_payload_mycdf = '{name}{ext}'.format(name='vlc_http-payload-mycdf', ext=helper.DOT_PDF)
        file_seg_payload = '{name}{ext}'.format(name='vlc_http-seg-payload', ext=helper.DOT_CSV)
        figure_seg_payload = '{name}{ext}'.format(name='vlc_http-seg-payload', ext=helper.DOT_PDF)
        figure_seg_payload_cdf = '{name}{ext}'.format(name='vlc_http-seg-payload-cdf', ext=helper.DOT_PDF)
        file_seg_payload_mycdf = '{name}{ext}'.format(name='vlc_http-seg-payload-mycdf', ext=helper.DOT_CSV)
        figure_seg_payload_mycdf = '{name}{ext}'.format(name='vlc_http-seg-payload-mycdf', ext=helper.DOT_PDF)
        file_download = '{name}{ext}'.format(name='vlc_http-download', ext=helper.DOT_CSV)
        figure_download = '{name}{ext}'.format(name='vlc_http-download', ext=helper.DOT_PDF)
        file_seg_perf = '{name}{ext}'.format(name='vlc_http-seg-perf', ext=helper.DOT_CSV)
        figure_seg_perf = '{name}{ext}'.format(name='vlc_http-seg-perf', ext=helper.DOT_PDF)
        figure_seg_perf_cdf = '{name}{ext}'.format(name='vlc_http-seg-perf-cdf', ext=helper.DOT_PDF)
        file_get = '{name}{ext}'.format(name='vlc_http-get', ext=helper.DOT_CSV)
        figure_get = '{name}{ext}'.format(name='vlc_http-get', ext=helper.DOT_PDF)

        if not override and any(os.path.isfile(f) for f in [file_goodput, figure_goodput,
                                                            file_payload, figure_payload,
                                                            figure_payload_cdf, figure_payload_dist, # TODO keep?
                                                            file_payload_mycdf, figure_payload_mycdf,
                                                            file_seg_payload, figure_seg_payload,
                                                            figure_seg_payload_cdf,
                                                            file_seg_payload_mycdf, figure_seg_payload_mycdf,
                                                            file_download, figure_download,
                                                            file_seg_perf, figure_seg_perf, figure_seg_perf_cdf,
                                                            file_get, figure_get]):
            print('HTTP requests file(s) exist(s) and will not be overridden...')
            return

        # Get data
        df0_send = vlc.HTTP_SEND_SOCKET.load_data_df(directory=data_dir, verbose=verbose)
        df0_replycode = vlc.HTTP_REPLYCODE.load_data_df(directory=data_dir, verbose=verbose)
        df0_read = vlc.HTTP_READ.load_data_df(directory=data_dir, verbose=verbose)

        if len(df0_send.index) != len(df0_replycode.index):
            raise helper.ArgumentLengthMismatchException(
                    'HTTP send and reply-code size mismatch (skip): {} != {}'.format(len(df0_send.index),
                                                                                     len(df0_replycode.index)))
        # eliminate failed requests
        failed_requests = df0_replycode[np.logical_and(df0_replycode['replycode'] != 200,
                                                       df0_replycode['replycode'] != 206)].index.values.tolist()
        # TODO do not ignore responses != 200, 206, mark them
        df0_send = df0_send.drop(failed_requests)  # df0_send = df0_send.drop(df0_send.index[failed_requests])
        # print('index after:', len(df0_send.index))
        if len(df0_send.index) != len(df0_read.index):
            raise helper.ArgumentLengthMismatchException(
                    'HTTP send and read size mismatch (skip): {} != {}'.format(len(df0_send.index),
                                                                               len(df0_read.index)))

        # Find actual video segments in payload
        init_segments = df0_read[
            df0_read['content-length (bytes)'] < 1000].index.values.tolist() # /!\ init segment size

        df0_segment_read = df0_read.drop(init_segments)  # careful: non continuous index
        df0_segment_send = df0_send.drop(init_segments)  # careful: non continuous index
        if len(df0_segment_read) != len(df0_segment_send) != seg_amount:
            raise helper.ArgumentLengthMismatchException('segment send and read')

        # Compute
        time_index_read = plot_test.convert(vector=df0_read[vlc.MDATE],
                                            to_subtract=start,
                                            to_multiply=helper.MICRO)
        time_index_segment_read = plot_test.convert(vector=df0_segment_read[vlc.MDATE],
                                                    to_subtract=start,
                                                    to_multiply=helper.MICRO)
        delta_time_sec = df0_read[vlc.MDATE].sub(df0_send[vlc.MDATE]).mul(helper.MICRO)
        delta_time_segment_sec = df0_segment_read[vlc.MDATE].sub(df0_segment_send[vlc.MDATE]).mul(helper.MICRO)
        payload_bits = df0_read['content-length (bytes)'].mul(helper.BYTES_TO_BITS)
        payload_segment_bits = df0_segment_read['content-length (bytes)'].mul(helper.BYTES_TO_BITS)
        goodput_bps = payload_bits / delta_time_sec
        goodput_segment_bps = payload_segment_bits / delta_time_segment_sec

        seg_performance = seg_size / delta_time_segment_sec

        # Goodput = payload / download time interval
        data_goodput = {'index': time_index_read,
                        r'goodput': goodput_bps,
                        r'cumulative goodput': goodput_bps.cumsum() / goodput_bps.index
                        }
        df_goodput = pd.DataFrame(data=data_goodput)

        # Content-length (plain)
        data_content_length = {'index': time_index_read,
                               r'HTTP content-length': df0_read['content-length (bytes)']}
        df_content_length = pd.DataFrame(data=data_content_length)

        # Content-length (segments)
        data_content_length_segment = {'index': time_index_segment_read,
                                       r'HTTP content-length': df0_segment_read['content-length (bytes)']}
        df_content_length_segment = pd.DataFrame(data=data_content_length_segment)

        # Download time = response time - request time
        data_download = {'index': time_index_read,
                         r'segment download time': delta_time_sec}
        df_download = pd.DataFrame(data=data_download)

        # Segment performance = segment playback time interval / download time interval
        data_seg_performance = {'index': time_index_segment_read,
                                r'segment performance': seg_performance}
        df_seg_performance = pd.DataFrame(data=data_seg_performance)

        # Weighted CDF by hand
        df_payload_mycdf = plot_test.normalise_for_cdf(df0_read['content-length (bytes)'], 'bytes')
        df_payload_segment_mycdf = plot_test.normalise_for_cdf(df0_segment_read['content-length (bytes)'], 'bytes')

        # Request schedule
        data_get = {'index': plot_test.convert(vector=df0_send[vlc.MDATE],
                                               to_subtract=start,
                                               to_multiply=helper.MICRO)}
        df_get = pd.DataFrame(data=data_get)

        # Print csv to file
        df_goodput.to_csv(path_or_buf=file_goodput)
        df_content_length.to_csv(path_or_buf=file_payload)
        df_content_length_segment.to_csv(path_or_buf=file_seg_payload)
        df_download.to_csv(path_or_buf=file_download)
        df_seg_performance.to_csv(path_or_buf=file_seg_perf)
        df_payload_mycdf.to_csv(path_or_buf=file_payload_mycdf)
        df_payload_segment_mycdf.to_csv(path_or_buf=file_seg_payload_mycdf)
        df_get.to_csv(path_or_buf=file_get)

        print('here')

        # Plot pdf to file
        # BANDWIDTH OVER TIME
        plt.figure(1)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        df_goodput.plot(x='index', ax=ax)
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'bandwidth (bps)')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.grid()

        plt.savefig(figure_goodput, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # HTTP CONTENT-LENGTH OVER TIME
        plt.figure(2)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        ax.scatter(x=df_content_length['index'],
                   y=df_content_length[r'HTTP content-length'],
                   label=r'HTTP content-length', )
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'payload (bytes)')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.set_ylim(ymin=0)
        ax.grid()

        plt.savefig(figure_payload, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # HTTP CONTENT-LENGTH DISTRIBUTION (CDF)
        plt.figure()
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        ax.hist(df_content_length[r'HTTP content-length'],
                bins=len(df_content_length[r'HTTP content-length'].index), # /!\ bin size?
                cumulative=True, normed=True, histtype='step')
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'payload (bytes)')
        ax.set_ylabel(r'CDF')
        ax.set_xlim(xmin=0)  # TODO limit by smallest and biggest available segment size!
        ax.set_ylim(ymin=0)
        ax.grid()

        plt.savefig(figure_payload_cdf, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # HTTP CONTENT-LENGTH DISTRIBUTION (BARS)
        plt.figure()
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        ax.hist(df_content_length[r'HTTP content-length'],
                bins=len(df_content_length[r'HTTP content-length'].index), #  /!\ bin size?
                cumulative=False, normed=True, histtype='bar')
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'payload (bytes)')
        ax.set_ylabel(r'distribution')
        ax.set_xlim(xmin=0)  # TODO limit by smallest and biggest available segment size!
        ax.set_ylim(ymin=0)
        ax.grid()

        plt.savefig(figure_payload_dist, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # HTTP CONTENT-LENGTH DISTRIBUTION (WEIGHTED CDF)  # KEEP
        plt.figure(3)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        # df_payload_mycdf.scatter(x='size', y='share', legend=r'HTTP content-length', ax=ax)
        ax.plot(df_payload_mycdf['bytes'],
                df_payload_mycdf['share'].cumsum(),
                label=r'HTTP content-length')
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'payload (bytes)')
        ax.set_ylabel(r'normalised CDF')
        ax.set_xlim(xmin=0)  # TODO limit by smallest and biggest available segment size!
        ax.set_ylim(ymin=0, ymax=1)
        ax.set_yticks(np.arange(0, 1.1, 0.1))
        ax.grid()

        plt.savefig(figure_payload_mycdf, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # SEGMENT HTTP CONTENT-LENGTH OVER TIME
        plt.figure()
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        ax.scatter(x=df_content_length_segment['index'],
                   y=df_content_length_segment[r'HTTP content-length'],
                   label=r'HTTP content-length', )
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'segment payload (bytes)')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.set_ylim(ymin=0)
        ax.grid()

        plt.savefig(figure_seg_payload, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # SEGMENT HTTP CONTENT-LENGTH DISTRIBUTION (CDF)
        plt.figure()
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        ax.hist(df_content_length_segment[r'HTTP content-length'],
                bins=len(df_content_length_segment[r'HTTP content-length'].index), #  /!\ bin size?
                cumulative=True, normed=True, histtype='step')
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'segment payload (bytes)')
        ax.set_ylabel(r'CDF')
        ax.set_xlim(xmin=0)  # TODO limit by smallest and biggest available segment size!
        ax.set_ylim(ymin=0)
        ax.grid()

        plt.savefig(figure_seg_payload_cdf, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # SEGMENT HTTP CONTENT-LENGTH DISTRIBUTION (WEIGHTED CDF)  # KEEP
        plt.figure()
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        ax.plot(df_payload_segment_mycdf['bytes'],
                df_payload_segment_mycdf['share'].cumsum(),
                label=r'HTTP content-length')
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'segment payload (bytes)')
        ax.set_ylabel(r'normalised CDF')
        ax.set_xlim(xmin=0)  # TODO limit by smallest and biggest available segment size!
        ax.set_ylim(ymin=0, ymax=1)
        ax.set_yticks(np.arange(0, 1.1, 0.1))
        ax.grid()

        plt.savefig(figure_seg_payload_mycdf, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # SEGMENT DOWNLOAD TIME OVER TIME
        plt.figure(4)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        ax.scatter(x=df_download['index'],
                   y=df_download[r'segment download time'])

        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'time (s)')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.grid()

        plt.savefig(figure_download, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # SEGMENT PERFORMANCE OVER TIME
        plt.figure(5)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        ax.scatter(x=df_seg_performance['index'],
                   y=df_seg_performance[r'segment performance'])
        ax.axhline(y=1, color='red')
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'playback time / download time')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.set_ylim(ymin=0)
        ax.set_yticks(np.arange(0, ax.get_ylim()[1] + 1, 1))
        ax.grid()

        plt.savefig(figure_seg_perf, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # SEGMENT PERFORMANCE DISTRIBUTION (CDF)
        plt.figure()
        ax = plt.gca()
        ax.set_clip_on(False)

        # ax.hist(df_seg_performance[r'segment performance'],
        #         bins=len(df_seg_performance[r'segment performance'].index),
        #         cumulative=True, normed=True, histtype='step')
        ecdf_seg_perf = sm.distributions.ECDF(df_seg_performance[r'segment performance'])

        ax.plot(ecdf_seg_perf.x, ecdf_seg_perf.y, label=r'segment performance')
        # does the same: plt.step(cwnd_ecdf.x, cwnd_ecdf.y, label=r'plt step')

        ax.axvline(x=1, color='red')
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'playback time / download time (s/s)')
        ax.set_ylabel(r'CDF')
        ax.set_xlim(0, 12)  # FIXME
        ax.set_xticks(np.arange(0, ax.get_xlim()[1] + 1, 1))
        ax.set_ylim(ymin=0, ymax=1)
        ax.set_yticks(np.arange(0, 1.1, 0.1))
        ax.grid()

        plt.savefig(figure_seg_perf_cdf, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # GET REQUEST SCHEDULE OVER TIME
        plt.figure(6)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        ax.yaxis.set_major_locator(MaxNLocator(integer=True))
        ax.scatter(x=df_get['index'], y=[1] * df_get['index'].size, label=r'GET requests', c='blue')
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'--')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.grid()

        plt.savefig(figure_get, format='pdf')  #, bbox_inches='tight', transparent=True)
        plt.close()
