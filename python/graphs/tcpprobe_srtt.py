#!/usr/bin/env python3.5
#
# Compute and save srtt metrics.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import helper
from tcpprobe import TCPProbe

LINK1_CLIENT_80 = '[::ffff:10.0.11.42]:80'
LINK2_CLIENT_80 = '[::ffff:10.0.22.42]:80'


def srtt(data_dir, plot_dir, override=False, verbose=False):
    with helper.cd(os.path.join(data_dir, plot_dir)):
        # Check if files exist
        file_link1 = 'tcpprobe_link1_srtt{ext}'.format(ext=helper.DOT_CSV)
        figure_link1 = 'tcpprobe_link1_srtt{ext}'.format(ext=helper.DOT_PDF)
        file_link2 = 'tcpprobe_link2_srtt{ext}'.format(ext=helper.DOT_CSV)
        figure_link2 = 'tcpprobe_link2_srtt{ext}'.format(ext=helper.DOT_PDF)
        file_links_stats = 'tcpprobe_links_srtt_stats{ext}'.format(ext=helper.DOT_CSV)
        # figure_links_stats = 'tcpprobe_links_srtt_stats{ext}'.format(ext=common.DOT_PDF)
        if not override and any(os.path.isfile(f) for f in [file_link1, figure_link1, file_link2, figure_link2,
                                                            file_links_stats]):
            print('tcpprobe srtt result file(s) exist(s) and will not be overridden...')
            return

        # Get data
        df0_tcp_probe = TCPProbe.load_data_df(directory=data_dir, verbose=verbose) # TODO new

        # Separate into the 2 different links
        link1 = df0_tcp_probe[df0_tcp_probe[TCPProbe.col_source] == LINK1_CLIENT_80]  # all data over link1
        link2 = df0_tcp_probe[df0_tcp_probe[TCPProbe.col_source] == LINK2_CLIENT_80]  # all data over link2

        # Compute
        data_link1 = {'time': link1[TCPProbe.col_timestamp],
                      r'daddr:port': link1[TCPProbe.col_destination],
                      r'srtt (micros) on link 1': link1[TCPProbe.col_srtt],
                      r'bytes on link 1': link1[TCPProbe.col_bytes]}
        data_link2 = {'time': link2[TCPProbe.col_timestamp],
                      r'daddr:port': link2[TCPProbe.col_destination],
                      r'srtt (micros) on link 2': link2[TCPProbe.col_srtt],
                      r'bytes on link 2': link2[TCPProbe.col_bytes]}

        data_links_stats = {'links_all srtt': df0_tcp_probe[TCPProbe.col_snd_cwnd].describe(),
                            'link1 srtt': link1[TCPProbe.col_snd_cwnd].describe(),
                            'link2 srtt': link2[TCPProbe.col_snd_cwnd].describe(),
                            'links_all bytes': df0_tcp_probe[TCPProbe.col_bytes].describe(),
                            'link1 bytes': link1[TCPProbe.col_bytes].describe(),
                            'link2 bytes': link2[TCPProbe.col_bytes].describe()}

        # Create dfs
        df_link1 = pd.DataFrame(data=data_link1)
        df_link2 = pd.DataFrame(data=data_link2)
        df_links_stats = pd.DataFrame(data=data_links_stats)

        # Print csv to file
        df_link1.to_csv(path_or_buf=file_link1)
        df_link2.to_csv(path_or_buf=file_link2)
        df_links_stats.to_csv(path_or_buf=file_links_stats)

        print('end of save in srtt, now plot')  # delete

        # Plot pdf to file
        for n, df_link, fig_link in zip([1, 2], [df_link1, df_link2], [figure_link1, figure_link2]):
            if df_link is not None and not df_link.empty:
                print('n is', n)

                conns = len(df_link[TCPProbe.col_destination].unique())
                if verbose:
                    print('For link {n}, found {unique} different destination ports...'.format(n=n, unique=conns))

                # if conns < 2:
                #     print('Something weird happened in ' + __file__)
                #     return -1

                # First case: only 2 connections
                if conns == 2:
                    # Plot evolution of srtt
                    plt.figure(n)
                    ax = plt.gca()
                    ax.set_clip_on(False)

                    daddr1 = df_link[r'daddr:port'].unique()[0]  # mpd connection
                    daddr2 = df_link[r'daddr:port'].unique()[1]  # streaming connection
                    # print('uniques', df_link[r'daddr:port'].unique())  # delete

                    df_link_daddr2 = df_link[df_link[r'daddr:port'] == daddr2]
                    ax.plot(df_link_daddr2['time'],
                            df_link_daddr2[r'srtt (micros) on link {}'.format(n)] / 1000,
                            label=r'srtt for {}'.format(daddr2.rsplit(':')[-1]),
                            marker='.', linestyle=None)
                    ax.legend(loc='best', frameon=True)

                    ax.set_xlabel(r'time (s)')
                    ax.set_ylabel(r'srtt (ms)')
                    ax.set_xlim(xmin=0)
                    ax.set_ylim(ymin=0)
                    if df_link[df_link[r'daddr:port'] == daddr2]['time'].max() < 660:
                        ax.set_xticks(np.arange(0, 660, 60))
                    ax.grid()

                    ax.set_title(r'Link {}'.format(n))
                    plt.savefig(fig_link, format='pdf', bbox_inches='tight', transparent=True)
                    plt.close()

                # Second case: > 2 connections
                else:
                    # Plot max cwnd over size of transmitted data
                    plt.figure(n)
                    ax = plt.gca()
                    ax.set_clip_on(False)

                    ax.scatter(x=df_link['time'],
                               y=df_link[r'srtt (micros) on link {}'.format(n)].div(1000),
                               label=r'srtt on over all connections on link {}'.format(n),
                               marker='.')
                    ax.legend(loc='best', frameon=True)  # frameon=False

                    ax.set_xlabel(r'time (s)')
                    ax.set_ylabel(r'srtt (ms)')
                    ax.set_xlim(xmin=0)
                    ax.set_ylim(ymin=0)
                    if df_link['time'].max() < 660:
                        ax.set_xticks(np.arange(0, 660, 60))
                    ax.grid()

                    plt.title(r'Link {}'.format(n))
                    plt.savefig(fig_link, format='pdf', bbox_inches='tight', transparent=True)
                    plt.close()

    print('made it to the end of srtt')
