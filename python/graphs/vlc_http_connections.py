#!/usr/bin/env python3.5
#
# Compute and save VLC connections metrics.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pylab import MaxNLocator

import helper
import plot_test
import vlc


def connections(data_dir, plot_dir, start, stop, override=False, verbose=False):
    with helper.cd(plot_dir):
        # Check if files exist
        file_conn = '{name}{ext}'.format(name='vlc_http-connection', ext=helper.DOT_CSV)
        figure_conn = '{name}{ext}'.format(name='vlc_http-connection', ext=helper.DOT_PDF)
        if not override and any(os.path.isfile(f) for f in [file_conn, figure_conn]):
            print('HTTP connection file(s) exist(s) and will not be overridden...')
            return

        # Get data
        # df0_new_conn = vlc.load_data_df(directory=data_dir, vlc_info=vlc.HTTPCONMAN_NEW_CONN, verbose=verbose)
        # df0_reuse_conn = vlc.load_data_df(directory=data_dir, vlc_info=vlc.HTTPCONMAN_REUSE_CONN, verbose=verbose)
        df0_new_conn = vlc.HTTPCONMAN_NEW_CONN.load_data_df(directory=data_dir, verbose=verbose)
        df0_reuse_conn = vlc.HTTPCONMAN_REUSE_CONN.load_data_df(directory=data_dir, verbose=verbose)

        # Compute
        all_conn = df0_new_conn.append(df0_reuse_conn)
        time_index_all = plot_test.convert(vector=all_conn[vlc.MDATE],
                                           to_subtract=start,
                                           to_multiply=helper.MICRO)
        time_index_new = plot_test.convert(vector=df0_new_conn[vlc.MDATE],
                                           to_subtract=start,
                                           to_multiply=helper.MICRO)
        data_conn = {'index-pool': time_index_all,
                     r'HTTP connection pool': all_conn['connection pool size'],
                     'index-new': time_index_new,
                     r'new HTTP connection': df0_new_conn['connection pool size']
                     }
        df_conn = pd.DataFrame(data=data_conn)

        # Print csv to file
        df_conn.to_csv(path_or_buf=file_conn)

        # Plot pdf to file
        plt.figure()
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        ax.yaxis.set_major_locator(MaxNLocator(integer=True))

        df_conn.plot(x='index-pool', y=r'HTTP connection pool', ax=ax)
        ax.scatter(x=df_conn['index-new'],
                   y=df_conn[r'new HTTP connection'],
                   label=r'new HTTP connection', c='blue')

        ax.legend(loc='best', frameon=True)  # frameon=False
        # frame = legend.get_frame()
        # frame.set_linewidth(0)

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'active HTTP connections')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.grid()

        plt.savefig(figure_conn, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()
