#!/usr/bin/env python3.5
#
# Compute and save VLC VHF metrics.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import helper
import plot_test
import vlc


def vhf(data_dir, plot_dir, start, stop, override=False, verbose=False):
    with helper.cd(plot_dir):
        # Check if files exist
        file_values = '{name}{ext}'.format(name='vlc_vhf-values', ext=helper.DOT_CSV)
        figure_values = '{name}{ext}'.format(name='vlc_vhf-values', ext=helper.DOT_PDF)
        file_alpha = '{name}{ext}'.format(name='vlc_vhf-alpha', ext=helper.DOT_CSV)
        figure_alpha = '{name}{ext}'.format(name='vlc_vhf-alpha', ext=helper.DOT_PDF)
        if not override and any(os.path.isfile(f) for f in [file_values, figure_values, file_alpha, figure_alpha]):
            print('vhf file(s) exist(s) and will not be overridden...')
            return

        # Get data
        # df0 = vlc.load_data_df(directory=data_dir, vlc_info=vlc.MOVAVG, verbose=verbose)
        df0 = vlc.MOVAVG.load_data_df(directory=data_dir, verbose=verbose)

        # Compute
        time_index = plot_test.convert(vector=df0[vlc.MDATE],
                                       to_subtract=start,
                                       to_multiply=helper.MICRO)
        data_values = {'index': time_index,
                       r'diffsums': df0['diffsums'],
                       r'deltamax': df0['deltamax'],
                       r'average': df0['avg']
                       }
        data_alpha = {'index': time_index,
                      r'alpha': df0['alpha']}
        df_values = pd.DataFrame(data=data_values)
        df_alpha = pd.DataFrame(data=data_alpha)

        # Print csv to file
        df_values.to_csv(path_or_buf=file_values)
        df_alpha.to_csv(path_or_buf=file_alpha)

        # Plot pdf to file
        plt.figure(1)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        df_values.plot(ax=ax, x='index')

        ax.legend(loc='best', frameon=True)  # frameon=False
        # frame = legend.get_frame()
        # frame.set_linewidth(0)

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'bandwidth (bps)')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.grid()

        plt.savefig(figure_values, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        plt.figure(2)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        df_alpha.plot(ax=ax, x='index')

        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'fraction')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.set_ylim(0, 1)
        ax.set_yticks(np.arange(0, 1.1, 0.1))
        ax.grid()

        plt.savefig(figure_alpha, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()
