#!/usr/bin/env python3.5
#
# Compute and save congestion window and srtt CDF metrics.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import statsmodels.api as sm

import helper
from tcpprobe import TCPProbe

LINK1_CLIENT_80 = '[::ffff:10.0.11.42]:80'
LINK2_CLIENT_80 = '[::ffff:10.0.22.42]:80'


def tcpprobe_cdf(data_dir, plot_dir, override=False, verbose=False):
    with helper.cd(os.path.join(data_dir, plot_dir)):
        file_link1_cwnd_cdf = 'tcpprobe_link1_cwnd_ssthresh_cdf{ext}'.format(ext=helper.DOT_CSV)
        figure_link1_cwnd_cdf = 'tcpprobe_link1_cwnd_ssthresh_cdf{ext}'.format(ext=helper.DOT_PDF)
        file_link2_cwnd_cdf = 'tcpprobe_link2_cwnd_ssthresh_cdf{ext}'.format(ext=helper.DOT_CSV)
        figure_link2_cwnd_cdf = 'tcpprobe_link2_cwnd_ssthresh_cdf{ext}'.format(ext=helper.DOT_PDF)
        file_link1_cwnd_cdf2 = 'tcpprobe_link1_cwnd_ssthresh_cdf2{ext}'.format(ext=helper.DOT_CSV)
        figure_link1_cwnd_cdf2 = 'tcpprobe_link1_cwnd_ssthresh_cdf2{ext}'.format(ext=helper.DOT_PDF)
        file_link2_cwnd_cdf2 = 'tcpprobe_link2_cwnd_ssthresh_cdf2{ext}'.format(ext=helper.DOT_CSV)
        figure_link2_cwnd_cdf2 = 'tcpprobe_link2_cwnd_ssthresh_cdf2{ext}'.format(ext=helper.DOT_PDF)

        file_link1_srtt_cdf = 'tcpprobe_link1_srtt_cdf{ext}'.format(ext=helper.DOT_CSV)
        figure_link1_srtt_cdf = 'tcpprobe_link1_srtt_cdf{ext}'.format(ext=helper.DOT_PDF)
        file_link2_srtt_cdf = 'tcpprobe_link2_srtt_cdf{ext}'.format(ext=helper.DOT_CSV)
        figure_link2_srtt_cdf = 'tcpprobe_link2_srtt_cdf{ext}'.format(ext=helper.DOT_PDF)

        if not override and any(os.path.isfile(f) for f in [file_link1_cwnd_cdf, figure_link1_cwnd_cdf,
                                                            file_link2_cwnd_cdf, figure_link2_cwnd_cdf,
                                                            file_link1_cwnd_cdf2, figure_link1_cwnd_cdf2,
                                                            file_link2_cwnd_cdf2, figure_link2_cwnd_cdf2,
                                                            file_link1_srtt_cdf, figure_link1_srtt_cdf,
                                                            file_link2_srtt_cdf, figure_link2_srtt_cdf]):
            print('tcpprobe cdf result file(s) exist(s) and will not be overridden...')
            return

        # Get data
        df0_tcp_probe = TCPProbe.load_data_df(directory=data_dir, verbose=verbose) # TODO new

        # Separate into the 2 different links
        link1 = df0_tcp_probe[df0_tcp_probe[TCPProbe.col_source] == LINK1_CLIENT_80]  # all data over link1
        link2 = df0_tcp_probe[df0_tcp_probe[TCPProbe.col_source] == LINK2_CLIENT_80]  # all data over link2

        # Compute
        data_link1 = {'time': link1[TCPProbe.col_timestamp],
                      r'daddr:port': link1[TCPProbe.col_destination],
                      r'cwnd on link 1': link1[TCPProbe.col_snd_cwnd],
                      r'ssthresh on link 1': link1[TCPProbe.col_ssthresh],
                      r'srtt (micros) on link 1': link1[TCPProbe.col_srtt],
                      r'bytes on link 1': link1[TCPProbe.col_bytes]}
        data_link2 = {'time': link2[TCPProbe.col_timestamp],
                      r'daddr:port': link2[TCPProbe.col_destination],
                      r'cwnd on link 2': link2[TCPProbe.col_snd_cwnd],
                      r'ssthresh on link 2': link2[TCPProbe.col_ssthresh],
                      r'srtt (micros) on link 2': link2[TCPProbe.col_srtt],
                      r'bytes on link 2': link2[TCPProbe.col_bytes]}

        # Create dfs
        df_link1 = pd.DataFrame(data=data_link1)
        df_link2 = pd.DataFrame(data=data_link2)

        # Count connections
        conns = {1: df_link1[r'daddr:port'].nunique(), 2: df_link2[r'daddr:port'].nunique()}
        print(conns)

        # Plot CDF of all cwnd
        for n, df_link, fig_cwnd, fig_srtt, fig_cwnd2 in zip([1, 2], [df_link1, df_link2],
                                                             [figure_link1_cwnd_cdf, figure_link2_cwnd_cdf],
                                                             [figure_link1_srtt_cdf, figure_link2_srtt_cdf],
                                                             [figure_link1_cwnd_cdf2, figure_link2_cwnd_cdf2]):
            if df_link is not None and not df_link.empty:
                print('n is', n)
                if verbose:
                    print('For link {n}, found {unique} different destination ports...'.format(n=n, unique=df_link[
                        TCPProbe.col_destination].nunique()))
                # if df_link[TCPProbe.col_destination].nunique() < 2:
                #     print('Something weird happened in ' + __file__)
                #     return -1

                # CWND
                # In all cases, CDF of the cwnd (and ssthresh)
                plt.figure()
                ax = plt.gca()
                ax.set_clip_on(False)

                cwnd_ecdf = sm.distributions.ECDF(df_link[r'cwnd on link {}'.format(n)])
                ssthresh_ecdf = sm.distributions.ECDF(df_link[r'ssthresh on link {}'.format(n)])

                ax.plot(ssthresh_ecdf.x, ssthresh_ecdf.y,
                        label=r'ssthresh on link {} (max={})'.format(n, int(round(max(ssthresh_ecdf.x)))), color='red')
                ax.plot(cwnd_ecdf.x, cwnd_ecdf.y,
                        label=r'cwnd on link {} (max={})'.format(n, int(round(max(cwnd_ecdf.x)))))
                # does the same: plt.step(cwnd_ecdf.x, cwnd_ecdf.y, label=r'plt step')

                ax.legend(loc='best', frameon=True, title=r'{} connections'.format(conns[n]))
                ax.set_xlabel(r'(segments)')
                ax.set_ylabel(r'CDF')

                # if max(cwnd_ecdf.x) <= 100:
                #     ax.set_xticks(np.arange(0, 110, 10))
                # elif max(cwnd_ecdf.x) <= 150:
                #     ax.set_xticks(np.arange(0, 3000, 25))
                # else:
                #     ax.set_xticks(np.arange(0, 3000, 200))
                ax.set_xticks(np.arange(0, 550, 50))
                ax.set_yticks(np.arange(0, 1.1, 0.1))
                ax.set_xlim(xmin=0)
                for lim in [500, 2600]:  # FIXME choose intervals 100, 150, 200,
                    if max(cwnd_ecdf.x) <= lim:
                        ax.set_xlim(xmax=lim)
                        break
                ax.set_ylim(ymin=0, ymax=1)
                ax.grid()

                plt.savefig(fig_cwnd, format='pdf', bbox_inches='tight', transparent=True)
                plt.close()

                # SRTT
                # In all cases, CDF of the cwnd (and ssthresh)
                plt.figure()
                ax = plt.gca()
                ax.set_clip_on(False)

                df_link[r'srtt (millis) on link {}'.format(n)] = df_link[r'srtt (micros) on link {}'.format(n)].div(
                        10 ** 3)
                srtt_ecdf = sm.distributions.ECDF(df_link[r'srtt (millis) on link {}'.format(n)])
                ax.plot(srtt_ecdf.x, srtt_ecdf.y, label=r'srtt on link {} (max={})'.format(n, int(
                        round(max(srtt_ecdf.x)))))  # not yet # milliseconds
                # does the same: plt.step(srtt_ecdf.x, srtt_ecdf.y, label=r'plt step')

                ax.legend(loc='best', frameon=True, title='{} connections'.format(conns[n]))

                ax.set_xlabel(r'(ms)')
                ax.set_ylabel(r'CDF')
                # if max(srtt_ecdf.x) <= 100:
                #     ax.set_xticks(np.arange(0, 200, 10))
                # else:
                #     ax.set_xticks(np.arange(0, 2000, 25))
                ax.set_xticks(np.arange(0, 550, 50))
                ax.set_yticks(np.arange(0, 1.1, 0.1))
                ax.set_xlim(xmin=0)
                for lim in [500, 1000]:  # FIXME choose intervals 100, 150, 250, 325,
                    if max(srtt_ecdf.x) <= lim:
                        ax.set_xlim(xmax=lim)
                        break
                ax.set_ylim(ymin=0, ymax=1)
                ax.grid()
                plt.savefig(fig_srtt, format='pdf', bbox_inches='tight', transparent=True)
                plt.close()

                # CWND WITHOUT MPD AND OUTLIER (for BBB2s)
                # For too many connections, delete first 2
                print('conns[{}] is {}'.format(n, conns[n]))
                if conns[n] > 2:
                    plt.figure()
                    ax = plt.gca()
                    ax.set_clip_on(False)

                    ports = df_link[r'daddr:port'].unique()
                    mpd_conns = df_link[df_link[r'daddr:port'] == ports[0]].index.values.tolist()
                    long_conns = df_link[df_link[r'daddr:port'] == ports[1]].index.values.tolist()
                    df_link.drop(mpd_conns, inplace=True)
                    df_link.drop(long_conns, inplace=True)

                    cwnd_ecdf = sm.distributions.ECDF(df_link[r'cwnd on link {}'.format(n)])
                    ssthresh_ecdf = sm.distributions.ECDF(df_link[r'ssthresh on link {}'.format(n)])

                    ax.plot(ssthresh_ecdf.x, ssthresh_ecdf.y,
                            label=r'ssthresh on link {} (max={})'.format(n, int(round(max(ssthresh_ecdf.x)))),
                            color='red')
                    ax.plot(cwnd_ecdf.x, cwnd_ecdf.y,
                            label=r'cwnd on link {} (max={})'.format(n, int(round(max(cwnd_ecdf.x)))))
                    # does the same: plt.step(cwnd_ecdf.x, cwnd_ecdf.y, label=r'plt step')

                    ax.legend(loc='best', frameon=True, title=r'{} connections'.format(conns[n] - 2))
                    ax.set_xlabel(r'(segments)')
                    ax.set_ylabel(r'CDF')

                    # if max(cwnd_ecdf.x) <= 150:
                    #     ax.set_xticks(np.arange(0, 3000, 25))
                    # else:
                    if max(cwnd_ecdf.x) <= 40:
                        ax.set_xticks(np.arange(0, 50, 5))
                    elif max(cwnd_ecdf.x) <= 150:
                        ax.set_xticks(np.arange(0, 3000, 25))
                    else:
                        ax.set_xticks(np.arange(0, 3000, 200))
                    ax.set_yticks(np.arange(0, 1.1, 0.1))
                    ax.set_xlim(xmin=0)
                    for lim in [40, 50, 150, 200, 500, 2600]:  # FIXME choose intervals
                        if max(cwnd_ecdf.x) <= lim:
                            ax.set_xlim(xmax=lim)
                            break
                    ax.set_ylim(ymin=0, ymax=1)
                    ax.grid()

                    plt.savefig(fig_cwnd2, format='pdf', bbox_inches='tight', transparent=True)
                    plt.close()
