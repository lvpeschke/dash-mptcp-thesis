#!/usr/bin/env python3.5
#
# Compute and save VLC logic metrics.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.interpolate import interp1d

import config
import helper
import plot_test
import vlc


def rblogic(data_dir, plot_dir, start, stop, override=False, verbose=False):
    with helper.cd(plot_dir):
        # Check if files exist
        file_rep = '{name}{ext}'.format(name='vlc_rblogic-representation', ext=helper.DOT_CSV)
        figure_rep = '{name}{ext}'.format(name='vlc_rblogic-representation', ext=helper.DOT_PDF)
        file_bw = '{name}{ext}'.format(name='vlc_rblogic-bandwidth', ext=helper.DOT_CSV)
        figure_bw = '{name}{ext}'.format(name='vlc_rblogic-bandwidth', ext=helper.DOT_PDF)
        file_buffer = '{name}{ext}'.format(name='vlc_rblogic-buffer', ext=helper.DOT_CSV)
        figure_buffer = '{name}{ext}'.format(name='vlc_rblogic-buffer', ext=helper.DOT_PDF)
        file_input = '{name}{ext}'.format(name='vlc_rblogic-input', ext=helper.DOT_CSV)
        figure_input = '{name}{ext}'.format(name='vlc_rblogic-input', ext=helper.DOT_PDF)
        if not override and any(os.path.isfile(f) for f in [file_rep, figure_rep,
                                                            file_bw, figure_bw,
                                                            file_buffer, figure_buffer,
                                                            file_input, figure_input]):
            print('rblogic file(s) exist(s) and will not be overridden...')
            return

        # Get data
        df0_rep = vlc.RBLOGIC_REPRESENTATION.load_data_df(directory=data_dir, verbose=verbose)
        df0_update = vlc.RBLOGIC_UPDATE_RATE.load_data_df(directory=data_dir, verbose=verbose)  # no stream
        df0_update_input = vlc.RBLOGIC_UPDATE_RATE_INPUT.load_data_df(directory=data_dir, verbose=verbose)
        df0_switch = vlc.RBLOGIC_SWITCH.load_data_df(directory=data_dir, verbose=verbose)
        df0_bufstate = vlc.RBLOGIC_BUFSTATE.load_data_df(directory=data_dir, verbose=verbose)
        df0_buflevel = vlc.RBLOGIC_BUFLEVEL.load_data_df(directory=data_dir, verbose=verbose)

        # Compute
        time_index_rep = plot_test.convert(vector=df0_rep[vlc.MDATE],
                                           to_subtract=start,
                                           to_multiply=helper.MICRO)
        data_rep = {'index': time_index_rep,
                    r'segment bitrate': df0_rep['representation (bps)'],
                    r'cumulative bitrate average':
                        df0_rep['representation (bps)'].cumsum() / df0_rep['representation (bps)'].index
                    }
        df_rep = pd.DataFrame(data=data_rep)

        time_index_update = plot_test.convert(vector=df0_update[vlc.MDATE],
                                              to_subtract=start,
                                              to_multiply=helper.MICRO)
        time_index_switch = plot_test.convert(vector=df0_switch[vlc.MDATE],
                                              to_subtract=start,
                                              to_multiply=helper.MICRO)
        data_bw = {'index-bw': time_index_update,
                   r'measured bandwidth': df0_update['observed bandwidth (bps)'] / 10 ** 6,
                   r'VHF bandwidth': df0_update['VHF bandwidth (bps)'] / 10 ** 6,
                   r'bitrate threshold': df0_update['current bandwidth threshold (bps)'] / 10 ** 6,
                   'index-bitrate': time_index_rep,
                   r'used bitrate': df0_rep['representation (bps)'] / 10 ** 6,
                   'index-switch': time_index_switch,
                   r'used bitrate switch': df0_switch['representation (bps)'] / 10 ** 6
                   }
        df_bw = pd.DataFrame(data=data_bw)

        time_index_buflevel = plot_test.convert(vector=df0_buflevel[vlc.MDATE],
                                                to_subtract=start,
                                                to_multiply=helper.MICRO)
        time_index_bufstate = plot_test.convert(vector=df0_bufstate[vlc.MDATE],
                                                to_subtract=start,
                                                to_multiply=helper.MICRO)
        data_buffer = {'index-buflevel': time_index_buflevel,
                       r'target buffer level': plot_test.convert(df0_buflevel['target buffer level (micros)'], 0,
                                                                 helper.MICRO),
                       r'current buffer level': plot_test.convert(df0_buflevel['current buffer level (micros)'], 0,
                                                                  helper.MICRO),
                       'index-bufstate': time_index_bufstate,
                       r'buffer enabled': df0_bufstate['buffer enabled (bool)']
                       }
        df_buffer = pd.DataFrame(data=data_buffer)

        time_index_input = plot_test.convert(vector=df0_update_input[vlc.MDATE],
                                             to_subtract=start,
                                             to_multiply=helper.MICRO)
        data_input = {'index': time_index_input,
                      'input time': plot_test.convert(df0_update_input['input time (micros)'], 0, helper.MICRO),
                      'input size': df0_update_input['input size (Bytes)'],
                      'observation window time': plot_test.convert(df0_update_input['observation window time (micros)'],
                                                                   0, helper.MICRO),
                      'observation window size': df0_update_input['observation window size (Bytes)']}
        df_input = pd.DataFrame(data=data_input)

        # Print csv to file
        df_rep.to_csv(path_or_buf=file_rep)
        df_bw.to_csv(path_or_buf=file_bw)
        df_buffer.to_csv(path_or_buf=file_buffer)
        df_input.to_csv(path_or_buf=file_input)

        # Plot pdf to file
        # FIG 1
        plt.figure(1)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        df_rep.plot(ax=ax, x='index')  # marker='.',

        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'bitrate (bps)')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        # TODO get min and max available bitrates
        ax.grid()

        plt.savefig(figure_rep, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # FIG 2
        plt.figure(2)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        df_bw.plot(ax=ax, x='index-bw', y=r'measured bandwidth', color='green', linewidth=3)
        df_bw.plot(ax=ax, x='index-bw', y=r'VHF bandwidth', color='greenyellow', linewidth=1)
        df_bw.plot(ax=ax, x='index-bw', y=r'bitrate threshold', color='dodgerblue')
        df_bw.plot(ax=ax, x='index-bitrate', y=r'used bitrate', color='blue')
        ax.vlines(x=df_bw['index-switch'], ymin=[0], ymax=df_bw[r'used bitrate switch'],
                  color='grey')  # linestyles='solid'

        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'bandwidth/bitrate (Mbps)')

        if df_bw[r'measured bandwidth'].max() < 15:
            ax.set_ylim(0, 15)
            ax.set_yticks(np.arange(0, 15 + 1, 1))
        elif df_bw[r'measured bandwidth'].max() < 25:
            ax.set_ylim(0, 25)
            ax.set_yticks(np.arange(0, 25 + 1, 5))
        elif df_bw[r'measured bandwidth'].max() < 50:
            ax.set_ylim(0, 50)
            ax.set_yticks(np.arange(0, 50 + 1, 5))
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.grid()

        plt.savefig(figure_bw, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # FIG VHF special
        plt.figure()
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        df_bw.plot(ax=ax, x='index-bw', y=r'measured bandwidth', color='#2561B2', linewidth=3)
        df_bw.plot(ax=ax, x='index-bw', y=r'VHF bandwidth', color='#FF6426', linewidth=1)
        df_bw.plot(ax=ax, x='index-bitrate', y=r'used bitrate', color='#21ABCD', label=r'video bitrate')
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'bandwidth (Mbps)')

        if df_bw[r'measured bandwidth'].max() < 15:
            ax.set_ylim(0, 15)
            ax.set_yticks(np.arange(0, 15 + 1, 1))
        elif df_bw[r'measured bandwidth'].max() < 25:
            ax.set_ylim(0, 25)
            ax.set_yticks(np.arange(0, 25 + 1, 5))
        elif df_bw[r'measured bandwidth'].max() < 50:
            ax.set_ylim(0, 50)
            ax.set_yticks(np.arange(0, 50 + 1, 5))
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.grid()

        plt.savefig('vlc_rblogic_estimation.pdf', format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # FIG PURE MEASURED
        plt.figure()
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        df_bw.plot(ax=ax, x='index-bw', y=r'measured bandwidth', color='#2561B2')
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'bandwidth (Mbps)')

        ax.set_yticks(np.arange(0, 100, 10))
        ax.set_ylim(0, 70)  # FIXME adapt
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.grid()

        plt.savefig('vlc_rblogic_estimation_pure.pdf', format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # FIG 3
        plt.figure(3)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        df_buffer.plot(ax=ax, x='index-buflevel', y=r'current buffer level', color='#228B22')
        df_buffer.plot(ax=ax, x='index-buflevel', y=r'target buffer level', color='#014421')

        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'time (s)')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.set_ylim(ymin=0)  # TODO get max value
        ax.grid()

        plt.savefig(figure_buffer, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # FIG INPUT (PERCEIVED DELAY)
        plt.figure()
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        df_input.plot(ax=ax, x='index', y='input time', label=r'perceived delay')
        df_input.plot(ax=ax, x='index', y='observation window time', label=r'observation window length')
        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'time (s)')
        ax.set_xticks(np.arange(0, 660, 60))
        ax.set_xlim(0, stop * helper.MICRO)
        ax.grid()

        plt.savefig(figure_input, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()


def multiple_rblogic(data_dirs, plot_dir, starts, stops, video: config.DASHVideo, override=False, verbose=False):
    # TODO also save data to csv
    with helper.cd(plot_dir):
        # Check if files exist
        # file = '{name}{ext}'.format(name='vlc_multiple_rblogic-representation', ext=common.DOT_CSV)
        figure_rep = '{name}{ext}'.format(name='vlc_multiple_rblogic-representation', ext=helper.DOT_PDF)
        figure_bw = '{name}{ext}'.format(name='vlc_multiple_rblogic-bw', ext=helper.DOT_PDF)
        figure_bw_sum = '{name}{ext}'.format(name='vlc_multiple_rblogic-bw_sum', ext=helper.DOT_PDF)
        if not override and any(os.path.isfile(f) for f in [figure_rep, figure_bw, figure_bw_sum]):
            print('multiple-rblogic file(s) exist(s) and will not be overridden...')
            return

        print('ARGS ARE:')
        print(data_dirs)
        print(plot_dir)
        print(starts)
        print(stops)

        assert len(data_dirs) == len(starts) == len(stops)
        reps = []
        updates = []
        switches = []  # TODO
        for data_dir, start, stop in zip(data_dirs, starts, stops):
            reps.append(vlc.RBLOGIC_REPRESENTATION.load_data_df(directory=data_dir, verbose=verbose))
            updates.append(vlc.RBLOGIC_UPDATE_RATE.load_data_df(directory=data_dir, verbose=verbose))  # no stream
            switches.append(vlc.RBLOGIC_SWITCH.load_data_df(directory=data_dir, verbose=verbose))

            # print(vlc.RBLOGIC_REPRESENTATION.load_data_df(directory=data_dir, verbose=verbose))
            # print(vlc.RBLOGIC_UPDATE_RATE.load_data_df(directory=data_dir, verbose=verbose))

        switch_count = [switches[i][vlc.MDATE].count() - 2 for i in range(len(switches))]

        # Figure
        plt1 = plt.figure(1)
        ax1 = plt.gca()  # gca stands for 'get current axis'
        ax1.set_clip_on(False)
        plt2 = plt.figure(2)
        ax2 = plt.gca()
        ax2.set_clip_on(False)
        plt3 = plt.figure(3)
        ax3 = plt.gca()
        ax3.set_clip_on(True)

        # colors
        client_bitrate = '#21ABCD'
        client_bw = '#2561B2'
        rival_bitrate = '#CC0570'
        rival_bw = '#7F2656'
        total_2vm_bw = 'green'

        if video.id == config.VIDEO_BBB2_SIMPLE.id:  # FIXME setup
            max_y = 20  # FIXME setup
        elif video.id == config.VIDEO_BBB10_SIMPLE.id:
            max_y = 16
        else:
            raise ValueError('wrong video!')

        bw_x = np.linspace(0, int(video.get_duration_s()), num=video.segment_amount, endpoint=True, retstep=False)
        bw_interpols = []
        for idx, (df_rep, df_update, start, stop, bitrate_color, bw_color, name) in enumerate(
                zip(reps, updates, starts, stops, [client_bitrate, rival_bitrate], [client_bw, rival_bw],
                    ['client', 'rival'])):

            time_index_update = plot_test.convert(vector=df_update[vlc.MDATE],
                                                  to_subtract=start,
                                                  to_multiply=helper.MICRO)
            time_index_rep = plot_test.convert(vector=df_rep[vlc.MDATE],
                                               to_subtract=start,
                                               to_multiply=helper.MICRO)

            update_desc = (df_update['observed bandwidth (bps)'] * helper.MICRO).describe().to_dict()
            rep_desc = (df_rep['representation (bps)'] * helper.MICRO).describe().to_dict()

            bw_interp1d = interp1d(time_index_update, (df_update['observed bandwidth (bps)'] * helper.MICRO),
                                   bounds_error=False)
            bw_interpols.append(bw_interp1d)

            ax1.plot(time_index_update, (df_update['observed bandwidth (bps)'] * helper.MICRO),
                     label=r'measured bw ({}), avg={:.2f}, std={:.2f}'.format(name, update_desc['mean'],
                                                                              update_desc['std']),
                     color=bw_color)
            ax2.plot(time_index_rep, (df_rep['representation (bps)'] * helper.MICRO),
                     label=r'used bitrate ({}), avg={:.2f}, std={:.2f}, switches={}'.format(name, rep_desc['mean'],
                                                                                            rep_desc['std'],
                                                                                            switch_count[idx]),
                     color=bitrate_color)

        bw_sums = np.zeros(bw_x.shape)
        for bw_interp in bw_interpols:
            np.add(bw_sums, bw_interp(bw_x), out=bw_sums)

        bw_sums_avg = np.nanmean(bw_sums)
        bw_sums_std = np.nanstd(bw_sums)
        ax3.plot(bw_x, bw_sums,
                 label=r'sum of measured bandwidths (avg={:.2f}, std={:.2f})'.format(bw_sums_avg, bw_sums_std),
                 color=total_2vm_bw)

        ax1.legend(loc='best', frameon=True)
        ax2.legend(loc='best', frameon=True)
        ax3.legend(loc='best', frameon=True)

        ax1.set_xlabel(r'time (s)')
        ax1.set_ylabel(r'bandwidth (Mbps)')

        ax2.set_xlabel(r'time (s)')
        ax2.set_ylabel(r'bitrate (Mbps)')

        ax3.set_xlabel(r'time (s)')
        ax3.set_ylabel(r'bandwidth (Mbps)')

        ax1.set_xticks(np.arange(0, 900, 60))
        ax2.set_xticks(np.arange(0, 900, 60))
        ax3.set_xticks(np.arange(0, 900, 60))

        ax1.set_xlim(0, max(stops) * helper.MICRO)
        ax2.set_xlim(0, max(stops) * helper.MICRO)
        ax3.set_xlim(0, max(stops) * helper.MICRO)

        ax1.set_yticks(np.arange(0, max_y + 1, 1))
        ax2.set_yticks(np.arange(0, max_y, 1))
        ax3.set_yticks(np.arange(0, max_y + 1, 1))

        ax1.set_ylim(0, max_y / 2) # FIXME depends on test case
        ax2.set_ylim(0, 5) # FIXME bitrates
        ax3.set_ylim(0, max_y) # FIXME depends on test case
        ax1.grid()
        ax2.grid()
        ax3.grid()

        plt1.savefig(figure_bw, format='pdf', bbox_inches='tight', transparent=True)
        plt2.savefig(figure_rep, format='pdf', bbox_inches='tight', transparent=True)
        plt3.savefig(figure_bw_sum, format='pdf', bbox_inches='tight', transparent=True)
        plt.close('all')


def plogic(data_dir, plot_dir, start, stop, override=False, verbose=False):
    # Get data
    df0_streams = vlc.PLOGIC_NUMBER_STREAMS.load_data_df(directory=data_dir, verbose=verbose)  # no stream
    df0_highest1 = vlc.PLOGIC_HIGHEST1.load_data_df(directory=data_dir, verbose=verbose)
    df0_highest2 = vlc.PLOGIC_HIGHEST2.load_data_df(directory=data_dir, verbose=verbose)
    df0_stats = vlc.PLOGIC_STATS.load_data_df(directory=data_dir, verbose=verbose)
    df0_avail = vlc.PLOGIC_BPS_AVAIL.load_data_df(directory=data_dir, verbose=verbose)  # no stream
    df0_used = vlc.PLOGIC_BPS_USED.load_data_df(directory=data_dir, verbose=verbose)
    df0_update = vlc.PLOGIC_UPDATE_RATE.load_data_df(directory=data_dir, verbose=verbose)
    df0_switch = vlc.PLOGIC_SWITCH.load_data_df(directory=data_dir, verbose=verbose)
    df0_bufstate = vlc.PLOGIC_BUFSTATE.load_data_df(directory=data_dir, verbose=verbose)
    df0_buflevel = vlc.PLOGIC_BUFLEVEL.load_data_df(directory=data_dir, verbose=verbose)

    # Compute
    time_index_used = plot_test.convert(vector=df0_used[vlc.MDATE],
                                        to_subtract=start,
                                        to_multiply=helper.MICRO)
    data_rep = {'index': time_index_used,
                r'segment bitrate': df0_used['representation (bps)'],
                r'cumulative bitrate average':
                    df0_used['representation (bps)'].cumsum() / df0_used['representation (bps)'].index}
    df_rep = pd.DataFrame(data=data_rep)

    # TODO ratio buffer level --> show the zones

    time_index_stats = plot_test.convert(vector=df0_stats[vlc.MDATE],
                                         to_subtract=start,
                                         to_multiply=helper.MICRO)
    data_buffer1 = {'index': time_index_stats,
                    r'target buffer level': plot_test.convert(df0_stats['target buffer level (micros)'], 0,
                                                              helper.MICRO),
                    r'current buffer level': plot_test.convert(df0_stats['current buffer level (micros)'], 0,
                                                               helper.MICRO),
                    r'minimum buffer level': plot_test.convert(df0_stats['mininmum buffer level (micros)'], 0,
                                                               helper.MICRO)
                    }
    df_buffer1 = pd.DataFrame(data=data_buffer1)

    time_index_buflevel = plot_test.convert(vector=df0_buflevel[vlc.MDATE],
                                            to_subtract=start,
                                            to_multiply=helper.MICRO)
    time_index_bufstate = plot_test.convert(vector=df0_bufstate[vlc.MDATE],
                                            to_subtract=start,
                                            to_multiply=helper.MICRO)
    data_buffer2 = {'index-buflevel': time_index_buflevel,
                    r'target buffer level': plot_test.convert(df0_buflevel['target buffer level (micros)'], 0,
                                                              helper.MICRO),
                    r'current buffer level': plot_test.convert(df0_buflevel['current buffer level (micros)'], 0,
                                                               helper.MICRO),
                    'index-bufstate': time_index_bufstate,
                    r'buffer enabled': df0_bufstate['buffer enabled (bool)']
                    }
    df_buffer2 = pd.DataFrame(data=data_buffer2)

    time_index_update = plot_test.convert(vector=df0_update[vlc.MDATE],
                                          to_subtract=start,
                                          to_multiply=helper.MICRO)
    time_index_avail = plot_test.convert(vector=df0_avail[vlc.MDATE],
                                         to_subtract=start,
                                         to_multiply=helper.MICRO)
    time_index_switch = plot_test.convert(vector=df0_switch[vlc.MDATE],
                                          to_subtract=start,
                                          to_multiply=helper.MICRO)
    data_bw = {'index-update': time_index_update,
               r'measured bandwidth': df0_update['download rate (bps)'],
               'index-max': time_index_stats,
               r'maximum used bitrate': df0_stats['maximum bitrate (bps)'], # TODO keep or not ?
               'index-avail': time_index_avail,
               r'available bandwidth': df0_avail['available bandwidth (bps)'],
               'index-switch': time_index_switch,
               r'used bitrate': df0_switch['bandwidth usage (bps)']
               }
    df_bw = pd.DataFrame(data=data_bw)

    with helper.cd(plot_dir):
        # Check if files exist
        file_rep = '{name}{ext}'.format(name='vlc_plogic-representation', ext=helper.DOT_CSV)
        figure_rep = '{name}{ext}'.format(name='vlc_plogic-representation', ext=helper.DOT_PDF)
        file_buffer1 = '{name}{ext}'.format(name='vlc_plogic-buffer1', ext=helper.DOT_CSV)
        figure_buffer1 = '{name}{ext}'.format(name='vlc_plogic-buffer1', ext=helper.DOT_PDF)
        file_buffer2 = '{name}{ext}'.format(name='vlc_plogic-buffer2', ext=helper.DOT_CSV)
        figure_buffer2 = '{name}{ext}'.format(name='vlc_plogic-buffer2', ext=helper.DOT_PDF)
        file_bw = '{name}{ext}'.format(name='vlc_plogic-bandwidth', ext=helper.DOT_CSV)
        figure_bw = '{name}{ext}'.format(name='vlc_plogic-bandwidth', ext=helper.DOT_PDF)
        if not override and any(os.path.isfile(f) for f in [file_rep, figure_rep,
                                                            file_buffer1, figure_buffer1,
                                                            file_buffer2, figure_buffer2,
                                                            file_bw, figure_bw]):
            print('plogic file(s) exist(s) and will not be overridden...')
            return

        # Print csv to file
        df_rep.to_csv(path_or_buf=file_rep)
        df_buffer1.to_csv(path_or_buf=file_buffer1)
        df_buffer2.to_csv(path_or_buf=file_buffer2)
        df_bw.to_csv(path_or_buf=file_bw)

        # Plot pdf to file
        # FIG 1
        plt.figure(1)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        df_rep.plot(ax=ax, x='index')  # marker='.',

        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'bitrate (bps)')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.grid()

        # fig_rep.set_tight_layout(True)
        plt.savefig(figure_rep, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # FIG 2
        plt.figure(2)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        df_buffer1.plot(x='index', ax=ax)

        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'time (s)')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.set_ylim(ymin=0)
        ax.grid()

        plt.savefig(figure_buffer1, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # FIG 3
        plt.figure(3)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        df_buffer2.plot(x='index-buflevel', y=r'target buffer level', ax=ax)
        df_buffer2.plot(x='index-buflevel', y=r'current buffer level', ax=ax)
        buffer_enabled = df_buffer2[df_buffer2[r'buffer enabled'] == 1]['index-bufstate']
        buffer_disabled = df_buffer2[df_buffer2[r'buffer enabled'] == 0]['index-bufstate']
        ax.scatter(x=buffer_enabled,
                   y=[0] * buffer_enabled.size,
                   label=r'buffer enabled', c='green', s=50)
        ax.scatter(x=buffer_disabled,
                   y=[0] * buffer_disabled.size,
                   label=r'buffer disabled', c='red', s=50)

        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'time (s)')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.set_ylim(ymin=0)
        ax.grid()

        plt.savefig(figure_buffer2, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()

        # FIG 4
        plt.figure(4)
        ax = plt.gca()  # gca stands for 'get current axis'
        ax.set_clip_on(False)

        df_bw.plot(x='index-update', y=r'measured bandwidth', ax=ax)
        df_bw.plot(x='index-max', y=r'maximum used bitrate', ax=ax)
        df_bw.plot(x='index-avail', y=r'available bandwidth', ax=ax)
        ax.vlines(x=df_bw['index-switch'], ymin=[0], ymax=df_bw[r'used bitrate'],
                  color='grey', label=r'used bitrate switch')  # linestyles='solid'

        ax.legend(loc='best', frameon=True)  # frameon=False

        ax.set_xlabel(r'time (s)')
        ax.set_ylabel(r'bandwidth/bitrate (bps)')
        ax.set_xlim(0, stop * helper.MICRO)
        if stop * helper.MICRO < 660:
            ax.set_xticks(np.arange(0, 660, 60))
        ax.set_ylim(ymin=0)
        ax.grid()

        plt.savefig(figure_bw, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()
