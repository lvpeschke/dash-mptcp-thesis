#!/usr/bin/env python3.5
#
# Scripts to extract QoE parameter values.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import os
import re
import sys
import time

import config
import helper
import qoe
import vlc

description = """
Create QoE file(s).
Requires files from extract.py to have been created.
"""
epilog = """
See also: config.py, do_extract_vlc.py, qoe.py, vlc.py
"""


def find_folders(directory, find_file_func=vlc.find_vlclog, only_subs=False, verbose=False):
    """Get all folders in the directory where 'find_file_func' succeeds.

    :param directory: where to explore subfolders
    :param find_file_func: function returning something if it succeeds
    :param only_subs: only look at the subfolders, not the directory
    :param verbose: toggle verbose output
    :return: a list of folders
    """
    result = list()
    with helper.cd(directory):
        if not only_subs:
            # Try the main directory as data folder
            if verbose:
                print('Trying in main directory: {} (provided)...'.format(directory))

            if find_file_func():
                result = [os.getcwd()]

        # Else, try the sub-directories as data folders
        else:
            if verbose:
                if not only_subs:
                    print('No files found in {}, trying subfolders...'.format(directory))
                else:
                    print('Trying subfolders of {}...'.format(directory))
                    # try subfolders
            for subdir in [subdir for subdir in os.listdir(os.getcwd())
                           if os.path.isdir(os.path.abspath(os.path.expanduser(subdir)))]:
                full_subpath = os.path.abspath(os.path.expanduser(subdir))
                with helper.cd(full_subpath):
                    if verbose:
                        print('Trying the subdirectory: {}...'.format(subdir), end=' ', flush=True)
                    if find_file_func():
                        result.append(os.getcwd())
                    elif verbose:
                        print('nothing found, pass...')
    return result


def get_setup_from_dir_name(folder_name, verbose=False):
    """Deduce a test setup from the result folder name."""

    infos = folder_name.rstrip('/').split('/')[-1]
    if verbose:
        print('Trying to extract session infos from directory name: {}...'.format(infos))
    # print(infos)
    # '{dt}_{video.id}_{logic}_{mptcp}_{cong_ctrl}_{rates[0]:.2f}Mbps{delays[0]}ms_{rates[1]:.2f}Mbps{delays[1]}ms'
    # '(?P<datetime>\d{6}-\d{4})_(?P<video_id>.+)_(?P<logic>.+)_(?P<mptcp>[mt])_(?P<cong_ctrl>\w+)'
    # '_(?P<rate0>\d+\.\d{2})Mbps(?P<delay0>\d+(\.\d+)?)ms_(?P<rate1>\d+\.\d{2})Mbps(?P<delay1>\d+(\.\d+)?)ms'
    m = re.match(config.SINGLE_TEST_FOLDER_REVERSE, infos)
    if m:
        assert m.group('video_id') in config.ALL_VIDEOS.keys()
        assert m.group('logic') in vlc.ADAPTIVE_LOGICS
        assert m.group('mptcp') in {'m', 't'}
        assert m.group('cong_ctrl') in config.CONG_CTRL_ALGOS
        assert helper.check_positive_num([m.group('rate0'), m.group('rate1')])
        assert helper.check_positive_num([m.group('delay0'), m.group('delay1')])

        return {'datetime': m.group('datetime'),
                'video': config.ALL_VIDEOS.get(m.group('video_id')),
                'logic': m.group('logic'),
                'mptcp': 0 if m.group('mptcp') == 't' else 1,
                'cong_ctrl': m.group('cong_ctrl'),
                'rates': helper.convert_to_num([m.group('rate0'), m.group('rate1')]),
                'delays': helper.convert_to_num([m.group('delay0'), m.group('delay1')]),
                'link': 1  # static
                }
    else:
        raise RuntimeError('could not match the directory regex to the input directory name')


def do_comparison(directory, video, logic, rate, delay, override=False, verbose=False):
    """ Extract QoE for two clients in parallel."""

    assert isinstance(video, config.DASHVideo)
    assert video in config.ALL_VIDEOS.values()
    assert logic in vlc.ADAPTIVE_LOGICS

    paths = find_folders(directory=directory, find_file_func=vlc.find_vlclog, only_subs=True, verbose=verbose)
    print('Found {} subfolders'.format(len(paths)))
    with helper.cd(directory):
        if verbose:
            print('Trying to create QoEComparison for: {}...'.format(paths))

        try:
            datetime = str(time.strftime('%y%m%d-%H%M'))
            qoe_comparison = qoe.QoEComparison(video=video, logic=logic)
            qoe_comparison.create_qoe_comparison_file(datetime=datetime, rate=rate, delay=delay,
                                                      override=override, verbose=verbose)
        except FileExistsError as err:
            if not override:  # if not override, just do nothing
                if verbose:
                    print(err)
                pass
            else:
                raise
        else:
            for path in paths:
                setup = get_setup_from_dir_name(folder_name=path, verbose=verbose)
                # setup contains: 'datetime', 'video', 'logic', 'mptcp', 'cong_ctrl', 'rates', 'delays'

                # qoe_comparison.write_qoe_comparison_result(directory=path, setup=setup, verbose=verbose)
                qoe_comparison.copy_single_to_comparison(directory=path, setup=setup,
                                                         override=override, verbose=verbose)

            qoe_comparison.write_qoe_averages(directory=directory)


def do_single(directory, override=False, verbose=False):
    """Extract QoE for a single client."""

    paths = find_folders(directory=directory, find_file_func=vlc.find_vlclog, verbose=verbose)
    for path in paths:
        with helper.cd(path):
            if verbose:
                print('Trying to create QoE in: {} (provided)...'.format(path))

                setup = get_setup_from_dir_name(folder_name=path, verbose=verbose)
                qoe_single = qoe.QoE(video=setup['video'], logic=setup['logic'])
                qoe_single.check_and_set_setup(datetime=setup['datetime'],
                                               mptcp=setup['mptcp'], cong_ctrl=setup['cong_ctrl'],
                                               rates=setup['rates'], delays=setup['delays'],
                                               link=1, # static
                                               verbose=verbose)
                try:
                    qoe_single.create_qoe_file(override=override, verbose=verbose)
                    qoe_single.write_qoe_result(directory=os.getcwd(), verbose=verbose)
                except FileExistsError as err:
                    if not override:  # if not override, just do nothing
                        if verbose:
                            print(err)
                        pass
                    else:
                        raise
    if verbose:
        print('Extract single QoE success!')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-v', '--version',
                        action='version',
                        version=__file__ + ' version 6th December 2016')
    subparsers = parser.add_subparsers(title='cmd', dest='cmd')
    subparsers.required = True

    parser_single = subparsers.add_parser('single', help='for a single test')
    parser_single.add_argument('directory',
                               type=str,
                               help='directory where to find the files')
    parser_single.add_argument('--override',
                               action='store_true',
                               default=False,
                               help='override already existing extracted QoE files')
    parser_comparison = subparsers.add_parser('comparison', help='compare several tests')
    parser_comparison.add_argument('directory',
                                   type=str,
                                   help='super-directory where to find the files')
    parser_comparison.add_argument('rate',
                                   type=float,
                                   help='baseline bandwidth')
    parser_comparison.add_argument('delay',
                                   type=float,
                                   help='baseline delay')
    parser_comparison.add_argument('--video',
                                   type=str,
                                   default=config.VIDEO_DEFAULT.id,
                                   choices=config.ALL_VIDEOS,
                                   help='chosen video (see config.py)')
    parser_comparison.add_argument('--logic',
                                   type=str,
                                   default=vlc.RATE_LOGIC,
                                   choices=vlc.ADAPTIVE_LOGICS,
                                   help='choose adaptation logic for VLC')
    parser_comparison.add_argument('--override',
                                   action='store_true',
                                   default=False,
                                   help='override already existing QoE comparison file')
    args = parser.parse_args()

    if args and args.cmd == 'single':
        sys.exit(do_single(directory=args.directory, override=args.override, verbose=True))
    elif args and args.cmd == 'comparison':
        args.video = config.ALL_VIDEOS.get(args.video)
        sys.exit(do_comparison(directory=args.directory,
                               video=args.video, logic=args.logic, rate=args.rate, delay=args.delay,
                               override=args.override, verbose=True))
