# QoE parameters.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import csv
import os
import pprint
import sys

import numpy as np
import pandas as pd

import config
import helper
import vlc


# /!\ Only for rate adaptation logic.

class QoE:
    """Class representing a QoE result over a single video session."""

    QOE_RECORD = '{dt}_qoe_{video}_{logic}_{mt}_{cc}_{rate1}Mpbs_{rate2}Mbps_{delay1}ms_{delay2}ms_{link}{ext}'
    ext = helper.DOT_CSV
    results_max = 1

    col_headers = [
        # setup
        'timestamp',
        'video id', 'video duration (s)', 'vlc logic',
        'mptcp enabled', 'congestion control',
        'primary link', 'link 1 rate (Mbps)', 'link 2 rate (Mbps)', 'link 1 delay (ms)', 'link 2 delay (ms)',
        # QoE: bitrate
        'max. bitrate success rate',
        'count bitrate', 'mean bitrate (bps)', 'std bitrate (bps)',
        'min bitrate (bps)', 'max bitrate (bps)',
        '25 percentile bitrate (bps)', '50 percentile bitrate (bps)', '75 percentile bitrate (bps)',
        # QoE: bandwidth estimation
        'count bw estimation', 'mean bw estimation (bps)', 'std bw estimation (bps)',
        'min bw estimation (bps)', 'max bw estimation (bps)',
        '25 percentile bw estimation (bps)', '50 percentile bw estimation (bps)', '75 percentile bw estimation (bps)',
        # QoE: switches & stabled
        'down', 'drop', 'up', 'raise',
        'count switch', 'mean switch amplitude (bps)', 'std switch amplitude (bps)',
        'min switch amplitude (bps)', 'max switch amplitude (bps)',
        '25 percentile switch amplitude (bps)', '50 percentile switch amplitude (bps)',
        '75 percentile switch amplitude (bps)',
        'count stable', 'mean stable (micros)', 'std stable (micros)',
        'min stable (micros)', 'max stable (micros)',
        '25 percentile stable (micros)', '50 percentile stable (micros)', '75 percentile stable (micros)',
        # QoE: buffer
        'mean target buffer (micros)', 'std target buffer (micros)',
        'count buffer', 'mean buffer (micros)', 'std buffer (micros)',
        'min buffer (micros)', 'max buffer (micros)',
        '25 percentile buffer (micros)', '50 percentile buffer (micros)', '75 percentile buffer (micros)',
        # QoE: time
        'playback duration (s)',
        'estimated startup delay (s)',
        # HTTP from VLC
        'HTTP GET request count',
        'count HTTP responses', 'mean HTTP responses content-length (B)', 'std HTTP responses content-length (B)',
        'min HTTP responses content-length (B)', 'max HTTP responses content-length (B)',
        '25 percentile HTTP responses content-length (B)', '50 percentile HTTP responses content-length (B)',
        '75 percentile HTTP responses content-length (B)',
        'count HTTP responses no init', 'mean HTTP responses no init content-length (B)',
        'std HTTP responses no init content-length (B)',
        'min HTTP responses no init content-length (B)', 'max HTTP responses no init content-length (B)',
        '25 percentile HTTP responses no init content-length (B)',
        '50 percentile HTTP responses no init content-length (B)',
        '75 percentile HTTP responses no init content-length (B)',
        # HTTP from VLC: segment performance
        'mean HTTP segment performance (ratio)', 'std HTTP segment performance (ratio)',
        'min HTTP segment performance (ratio)', 'max HTTP segment performance (ratio)',
        '25 percentile HTTP segment performance (ratio)',
        '50 percentile HTTP segment performance (ratio)',
        '75 percentile HTTP segment performance (ratio)',
    ]

    def __init__(self, video, logic):
        assert isinstance(video, config.DASHVideo)
        assert video in config.ALL_VIDEOS.values()
        assert logic in vlc.ADAPTIVE_LOGICS

        # In the file (setup)
        self.video = video  # permanent
        self.logic = logic  # permanent
        self.datetime = None
        self.mptcp = None
        self.cong_ctrl = None
        self.rates = None
        self.delays = None
        self.link = None

        # For processing
        self.filename = None
        self.results_count = 0

    def check_and_set_setup(self, datetime, mptcp, cong_ctrl, rates, delays, link, video=None, logic=None,
                            verbose=False):
        if not isinstance(self.video, config.DASHVideo):
            raise TypeError('self.video is not a DASHVideo (see config.py)')
        if self.video not in config.ALL_VIDEOS.values():
            raise ValueError('self.video is not in config.ALL_VIDEOS (see config.py)')
        if self.logic not in vlc.ADAPTIVE_LOGICS:
            raise ValueError('self.logic is not in config.ADAPTIVE_LOGICS (see config.py)')
        if video and not video == self.video:
            raise ValueError('video and self.video cannot be different for a same QoE')
        if logic and not logic == self.logic:
            raise ValueError('logic and self.logic cannot be different for a same QoE')

        if datetime is None:
            raise ValueError('datetime cannot be None')
        if mptcp is None or (mptcp != 0 and mptcp != 1):
            raise ValueError('mptcp cannot be {}'.format(mptcp))
        if cong_ctrl is None or cong_ctrl not in config.CONG_CTRL_ALGOS:
            raise ValueError('cong_ctrl cannot be {}'.format(cong_ctrl))
        if rates is None or len(rates) == 0 or not helper.check_positive_num(rates):
            raise ValueError('rates cannot be {}'.format(rates))
        if delays is None or len(delays) == 0 or not helper.check_positive_num(delays):
            raise ValueError('delays cannot {}'.format(delays))
        if len(rates) != len(delays):
            raise ValueError('rates and delays must have the same length, but have {} and {}'
                             .format(len(rates), len(delays)))
        if link is None or link not in config.ALL_TRIPLE_LINKS:
            raise ValueError('link cannot be {}'.format(link))

        self.datetime = datetime
        self.mptcp = mptcp
        self.cong_ctrl = cong_ctrl
        self.rates = rates
        self.delays = delays
        self.link = link

        if verbose:
            print('QoE setup set (file={}): mptcp={}, cong_ctrl={}, rates={}, delays={}, link={}...'
                  .format(self.filename, mptcp, cong_ctrl, rates, delays, link))

    def _clear_setup(self, verbose=False):
        self.datetime = None
        self.mptcp = None
        self.cong_ctrl = None
        self.rates = None
        self.delays = None
        self.link = None
        # video & logic stay

        if verbose:
            print('QoE setup cleared (file={})...'.format(self.filename))

    def _check_and_get_setup(self, verbose=False):
        if not isinstance(self.video, config.DASHVideo):
            raise TypeError('self.video is not a DASHVideo (see config.py)')
        if self.video not in config.ALL_VIDEOS.values():
            raise ValueError('self.video is not in config.ALL_VIDEOS (see config.py)')
        if self.logic not in vlc.ADAPTIVE_LOGICS:
            raise ValueError('self.logic is not in config.ADAPTIVE_LOGICS (see config.py)')

        if self.datetime is None:
            raise ValueError('self.datetime cannot be None')
        if self.mptcp is None or (self.mptcp != 0 and self.mptcp != 1):
            raise ValueError('self.mptcp cannot be {}'.format(self.mptcp))
        if self.cong_ctrl is None or self.cong_ctrl not in config.CONG_CTRL_ALGOS:
            raise ValueError('self.cong_ctrl cannot be {}'.format(self.cong_ctrl))
        if self.rates is None or len(self.rates) == 0 or not helper.check_positive_num(self.rates):
            raise ValueError('self.rates cannot be {}'.format(self.rates))
        if self.delays is None or len(self.delays) == 0 or not helper.check_positive_num(self.delays):
            raise ValueError('self.delays cannot {}'.format(self.delays))
        if self.link is None or self.link not in config.ALL_TRIPLE_LINKS:
            raise ValueError('self.link cannot be {}'.format(self.link))

        if verbose:
            print('QoE setup checked and valid...')

        return {'datetime': self.datetime,
                'video': self.video,
                'logic': self.logic,
                'mptcp': self.mptcp,
                'cong_ctrl': self.cong_ctrl,
                'rates': self.rates,
                'delays': self.delays,
                'link': self.link
                }

    def _setup_to_writable_dict(self):
        return {'timestamp': self.datetime,
                'video id': self.video.id,
                'video duration (s)': self.video.get_duration_s(),
                'vlc logic': self.logic,
                'mptcp enabled': self.mptcp,
                'congestion control': self.cong_ctrl,
                'primary link': self.link,
                'link 1 rate (Mbps)': self.rates[0],
                'link 2 rate (Mbps)': self.rates[1],
                'link 1 delay (ms)': self.delays[0],
                'link 2 delay (ms)': self.delays[1],
                }

    def create_qoe_file(self, override=False, verbose=False):
        self._check_and_get_setup(verbose=verbose)

        # QOE_COMPARISON_RECORD =
        # '{dt}_qoe_{video}_{logic}_{mt}_{cc}_{rate1}Mpbs_{rate2}Mbps_{delay1}ms_{delay2}ms_{link}{ext}'
        rate1 = '{:.2f}'.format(self.rates[0])
        rate2 = '{:.2f}'.format(self.rates[1])
        filename = QoE.QOE_RECORD.format(dt=self.datetime, video=self.video.id, logic=self.logic,
                                         mt=('m' if self.mptcp else 't'), cc=self.cong_ctrl,
                                         rate1=rate1, rate2=rate2, delay1=self.delays[0], delay2=self.delays[1],
                                         link=self.link,
                                         ext=QoE.ext)
        self.filename = filename  # set filename before possible FileExistsError
        helper.create_file_with_override(filename, newline='', override=override)
        with open(self.filename, 'a') as fd:
            # fd.write(', '.join(QoE.col_headers) + '\n')
            # writer = csv.writer(fd, delimiter=',',)
            writer = csv.DictWriter(fd, fieldnames=self.col_headers)
            writer.writeheader()

        if verbose:
            print('New QoE file...', filename)

        return filename

    def _write_qoe_entry(self, stats: dict, setup: dict = None, verbose: bool = False):
        if self.filename is None:
            raise RuntimeError('no file has been created for this qoe')
        if not os.path.isfile(self.filename):
            raise FileNotFoundError('could not find qoe {} at {}'.format(self.filename, os.getcwd()))
        if not os.stat(self.filename).st_size > 0:
            raise helper.EmptyFileException('qoe {} at {} is empty'.format(self.filename, os.getcwd()))
        if not self.results_count < self.results_max:
            raise RuntimeError('qoe {} accepts max. {} result, current count is {}'.format(self.filename,
                                                                                           self.results_max,
                                                                                           self.results_count))
        if setup:
            self.check_and_set_setup(**setup)
        else:
            self._check_and_get_setup(verbose=verbose)

        setup_and_stats = stats
        setup_and_stats.update(self._setup_to_writable_dict())  # merge stat results and formated setup
        if not set(setup_and_stats.keys()) == set(self.col_headers):
            print(len(setup_and_stats), 'and', len(self.col_headers))
            pprint.pprint(setup_and_stats.keys())
            print()
            pprint.pprint(self.col_headers)
            raise csv.Error('stat does not contain the same keys as the column headers for this QoE record')

        with open(self.filename, 'a') as fd:
            if verbose:
                print('Writing QoE entry to file {}...'.format(self.filename))
            writer = csv.DictWriter(fd, fieldnames=self.col_headers)
            pprint.pprint(setup_and_stats)
            writer.writerow(setup_and_stats)

        if setup:
            self._clear_setup(verbose=verbose)

        self.results_count += 1
        if verbose:
            print('Done writing QoE entry to file {}, entry #{} of a max; #{}'
                  .format(self.filename, self.results_count, self.results_max))

    def _fetch_qoe_result(self, directory, verbose=False) -> dict:
        # compute all the results
        success_rate, bitrate = bitrate_stats(directory=directory, video=self.video, logic=self.logic, verbose=verbose)
        bw_estimation = bw_estimation_stats(directory=directory, logic=self.logic, verbose=verbose)
        switches, amplitude = switch_stats(directory=directory, logic=self.logic, verbose=verbose)
        stables = no_switch_stats(directory=directory, logic=self.logic, verbose=verbose)
        buffer_target, buffer_level = buffer_stats(directory=directory, logic=self.logic, verbose=verbose)
        playback, startup_delay = playback_time(directory=directory, video=self.video, verbose=verbose)
        requests, responses, responses_no_init = http_stats(directory=directory, video=self.video, verbose=verbose)
        segment_performance = video_segment_performance(directory=directory, video=self.video, verbose=verbose)

        # return everything as a dict
        return {
            'max. bitrate success rate': success_rate,
            'count bitrate': bitrate['count'],
            'mean bitrate (bps)': bitrate['mean'],
            'std bitrate (bps)': bitrate['std'],
            'min bitrate (bps)': bitrate['min'],
            'max bitrate (bps)': bitrate['max'],
            '25 percentile bitrate (bps)': bitrate['25%'],
            '50 percentile bitrate (bps)': bitrate['50%'],
            '75 percentile bitrate (bps)': bitrate['75%'],

            'count bw estimation': bw_estimation['count'],
            'mean bw estimation (bps)': bw_estimation['mean'],
            'std bw estimation (bps)': bw_estimation['std'],
            'min bw estimation (bps)': bw_estimation['min'],
            'max bw estimation (bps)': bw_estimation['max'],
            '25 percentile bw estimation (bps)': bw_estimation['25%'],
            '50 percentile bw estimation (bps)': bw_estimation['50%'],
            '75 percentile bw estimation (bps)': bw_estimation['75%'],

            'down': switches['downs'],
            'drop': switches['drops'],
            'up': switches['ups'],
            'raise': switches['raises'],
            'count switch': amplitude['count'],
            'mean switch amplitude (bps)': amplitude['mean'],
            'std switch amplitude (bps)': amplitude['std'],
            'min switch amplitude (bps)': amplitude['min'],
            'max switch amplitude (bps)': amplitude['max'],
            '25 percentile switch amplitude (bps)': amplitude['25%'],
            '50 percentile switch amplitude (bps)': amplitude['50%'],
            '75 percentile switch amplitude (bps)': amplitude['75%'],
            'count stable': stables['count'],
            'mean stable (micros)': stables['mean'],
            'std stable (micros)': stables['std'],
            'min stable (micros)': stables['min'],
            'max stable (micros)': stables['max'],
            '25 percentile stable (micros)': stables['25%'],
            '50 percentile stable (micros)': stables['50%'],
            '75 percentile stable (micros)': stables['75%'],

            'mean target buffer (micros)': buffer_target['mean'],
            'std target buffer (micros)': buffer_target['std'],
            'count buffer': buffer_level['count'],
            'mean buffer (micros)': buffer_level['mean'],
            'std buffer (micros)': buffer_level['std'],
            'min buffer (micros)': buffer_level['min'],
            'max buffer (micros)': buffer_level['max'],
            '25 percentile buffer (micros)': buffer_level['25%'],
            '50 percentile buffer (micros)': buffer_level['50%'],
            '75 percentile buffer (micros)': buffer_level['75%'],

            'playback duration (s)': playback,
            'estimated startup delay (s)': startup_delay,

            'HTTP GET request count': requests,
            'count HTTP responses': responses['count'],
            'mean HTTP responses content-length (B)': responses['mean'],
            'std HTTP responses content-length (B)': responses['std'],
            'min HTTP responses content-length (B)': responses['min'],
            'max HTTP responses content-length (B)': responses['max'],
            '25 percentile HTTP responses content-length (B)': responses['25%'],
            '50 percentile HTTP responses content-length (B)': responses['50%'],
            '75 percentile HTTP responses content-length (B)': responses['75%'],
            'count HTTP responses no init': responses_no_init['count'],
            'mean HTTP responses no init content-length (B)': responses_no_init['mean'],
            'std HTTP responses no init content-length (B)': responses_no_init['std'],
            'min HTTP responses no init content-length (B)': responses_no_init['min'],
            'max HTTP responses no init content-length (B)': responses_no_init['max'],
            '25 percentile HTTP responses no init content-length (B)': responses_no_init['25%'],
            '50 percentile HTTP responses no init content-length (B)': responses_no_init['50%'],
            '75 percentile HTTP responses no init content-length (B)': responses_no_init['75%'],
            'mean HTTP segment performance (ratio)': segment_performance['mean'],
            'std HTTP segment performance (ratio)': segment_performance['std'],
            'min HTTP segment performance (ratio)': segment_performance['min'],
            'max HTTP segment performance (ratio)': segment_performance['max'],
            '25 percentile HTTP segment performance (ratio)': segment_performance['25%'],
            '50 percentile HTTP segment performance (ratio)': segment_performance['50%'],
            '75 percentile HTTP segment performance (ratio)': segment_performance['75%'],
        }

    def write_qoe_result(self, directory, setup=None, verbose=False):
        if verbose:
            print('Write QoE result (fetch & write entry)...')
        results = self._fetch_qoe_result(directory=directory, verbose=verbose)
        # self._write_qoe_entry(**results, verbose=verbose)
        self._write_qoe_entry(stats=results, setup=setup, verbose=verbose)
        if verbose:
            print('Done writing QoE result (fetch & write entry)...')


class QoEComparison(QoE):
    """Class representing a QoE comparison result."""

    QOE_COMPARISON_RECORD = '{dt}_qoe_{video}_{logic}_{rate}Mpbs_{delay}ms{ext}'
    ext = helper.DOT_CSV
    results_max = sys.maxsize

    def __init__(self, video, logic):
        super().__init__(video, logic)

    def create_qoe_comparison_file(self, datetime, rate, delay, override=False, verbose=False):
        # QOE_COMPARISON_RECORD = '{dt}_qoe_{video}_{logic}_{rate}Mpbs_{delay}ms{ext}'
        rate_tcp = '{:.2f}'.format(rate)
        filename = QoEComparison.QOE_COMPARISON_RECORD.format(dt=datetime, video=self.video.id, logic=self.logic,
                                                              rate=rate_tcp, delay=delay, ext=QoE.ext)
        helper.create_file_with_override(filename, newline='', override=override)
        with open(filename, 'a') as fd:
            # fd.write(', '.join(QoEComparison.col_headers) + '\n')
            writer = csv.DictWriter(fd, fieldnames=self.col_headers)
            writer.writeheader()

        self.filename = filename
        if verbose:
            print('New QoEComparison file...', filename)
        return filename

    def load_qoe_qomparison(self, directory):
        with helper.cd(directory):
            return pd.read_csv(filepath_or_buffer=self.filename, header=0, skipinitialspace=True, verbose=False)

    def copy_single_to_comparison(self, directory, setup, override=False, verbose=False):
        # Check if single QoE result exists by trying to create it.
        qoe_single = QoE(video=setup['video'], logic=setup['logic'])
        qoe_single.check_and_set_setup(datetime=setup['datetime'],
                                       mptcp=setup['mptcp'], cong_ctrl=setup['cong_ctrl'],
                                       rates=setup['rates'], delays=setup['delays'],
                                       link=setup['link'],
                                       verbose=verbose)
        try:
            with helper.cd(directory):
                qoe_single.create_qoe_file(override=override, verbose=False)
                qoe_single.write_qoe_result(directory=os.getcwd(), verbose=verbose)
        except FileExistsError:
            pass

        if self.col_headers != qoe_single.col_headers:
            raise RuntimeError('QoEComparison has different col_headers than QoE, no result can be written to file!')

        # Copy the single result to the comparison file.
        with open(self.filename, 'a') as comp_fd, open(os.path.join(directory, qoe_single.filename), 'r') as qoe_fd:
            writer = csv.DictWriter(comp_fd, fieldnames=self.col_headers)
            reader = csv.DictReader(qoe_fd, fieldnames=qoe_single.col_headers)
            next(reader)  # skip the header
            writer.writerow(next(reader))  # copy the only line

    def write_qoe_averages(self, directory):
        file_path = os.path.join(directory, self.filename)
        df_all = pd.read_csv(filepath_or_buffer=file_path, header=0, skipinitialspace=True)
        # Group the entries by setup (mptcp on/off, same bandwidths, same delays)
        grouped = df_all.groupby(['mptcp enabled',
                                  'video id', 'vlc logic',
                                  'congestion control',
                                  'primary link',
                                  'link 1 rate (Mbps)', 'link 2 rate (Mbps)',
                                  'link 1 delay (ms)', 'link 2 delay (ms)'],
                                 sort=False)

        grouped_avg = grouped.agg(np.mean)

        grouped_avg['timestamp'] = 'average'  # mark as not relative to a test
        grouped_avg_reset_index = grouped_avg.reset_index()  # flatten out the index to not loose info

        with open(self.filename, 'a') as fd:
            writer = csv.DictWriter(fd, fieldnames=self.col_headers)
            writer.writerows(grouped_avg_reset_index.to_dict('records'))


def bitrate_stats(directory, video: config.DASHVideo, logic, verbose=False):
    # OK with predictive
    # 'max. bitrate success rate', 'bitrate'.description
    if logic == vlc.RATE_LOGIC:
        df = vlc.RBLOGIC_REPRESENTATION.load_data_df(directory=directory, verbose=verbose)
    elif logic == vlc.PREDICTIVE_LOGIC:
        df = vlc.PLOGIC_BPS_USED.load_data_df(directory=directory, verbose=verbose)
    else:
        raise ValueError('logic is {} but must be in {}'.format(logic, vlc.ADAPTIVE_LOGICS))

    # /!\ with VLC, the first and the last recorded representations are useless
    # /!\ 1st is repeated before actually fetching the 1st (lowest) init segment
    # /!\ last is used for the last HTTP request that always fails (no more segments)
    df_cropped = df[1:-1]  # crop first and last representations to align with segments
    count_max_bitrate = sum(df_cropped['representation (bps)'] == video.max_bitrate)
    count_all = df_cropped['representation (bps)'].count()
    # assert count_all == video.segment_amount
    if count_all != video.segment_amount:
        print('Careful! QoE detected {} representations, {} segments'.format(count_all, video.segment_amount))

    success_rate = count_max_bitrate / count_all
    bitrate = df_cropped['representation (bps)'].describe().to_dict()
    return success_rate, bitrate


def bw_estimation_stats(directory, logic, verbose=False):
    # 'count bw estimation', 'mean bw estimation (bps)', 'std bw estimation (bps)',
    # 'min bw estimation (bps)', 'max bw estimation (bps)',
    # '25 percentile bw estimation (bps)', '50 percentile bw estimation (bps)', '75 percentile bw estimation (bps)'
    if logic == vlc.RATE_LOGIC:
        df = vlc.RBLOGIC_UPDATE_RATE.load_data_df(directory=directory, verbose=verbose)
        bw_estimation = df['observed bandwidth (bps)']
    elif logic == vlc.PREDICTIVE_LOGIC:
        df = vlc.PLOGIC_UPDATE_RATE.load_data_df(directory=directory, verbose=verbose)
        bw_estimation = df['download rate (bps)']
    else:
        raise ValueError('logic is {} but must be in {}'.format(logic, vlc.ADAPTIVE_LOGICS))

    return bw_estimation.describe().to_dict()


def no_switch_stats(directory, logic, verbose=False):
    if logic == vlc.RATE_LOGIC:
        df = vlc.RBLOGIC_SWITCH.load_data_df(directory=directory, verbose=verbose)
        # col = 'representation (bps)'
    elif logic == vlc.PREDICTIVE_LOGIC:
        df = vlc.PLOGIC_SWITCH.load_data_df(directory=directory, verbose=verbose)
        # col = 'bandwidth usage (bps)'
    else:
        raise ValueError('logic is {} but must be in {}'.format(logic, vlc.ADAPTIVE_LOGICS))

    df_stable = (df[vlc.MDATE].diff()).dropna()
    print('diff:')
    pprint.pprint(df[vlc.MDATE].diff())
    print()
    print('diff dropna:')
    pprint.pprint((df[vlc.MDATE].diff()).dropna())

    return df_stable.describe().to_dict()


def switch_stats(directory, logic, verbose=False):
    # OK with predictive
    # 'down', 'drop', 'up', 'raise', 'switches'
    if logic == vlc.RATE_LOGIC:
        df = vlc.RBLOGIC_SWITCH.load_data_df(directory=directory, verbose=verbose)
        col = 'representation (bps)'
    elif logic == vlc.PREDICTIVE_LOGIC:
        df = vlc.PLOGIC_SWITCH.load_data_df(directory=directory, verbose=verbose)
        col = 'bandwidth usage (bps)'
    else:
        raise ValueError('logic is {} but must be in {}'.format(logic, vlc.ADAPTIVE_LOGICS))

    # The last recorded "switch" is to representation 0, which does not exist. Drop it.
    zero_indexes = df[df[col] == 0].index
    if len(zero_indexes) > 1:
        print('Careful! {} > 1 zero-representation in switch df!'.format(len(zero_indexes)))
    if len(zero_indexes) == 0 and zero_indexes[0] != df[col].count():
        print('Careful! zero-representation in switch df not at the end but at {}!'.format(
                zero_indexes[0]))
    df_no_zero = df.drop(zero_indexes)

    downs = df_no_zero[col].diff().lt(0).sum()
    ups = df_no_zero[col].diff().gt(0).sum()

    direction = 0
    drops = 0
    raises = 0
    for cur, prev in zip(df_no_zero[col][1:], df_no_zero[col][:-1]):
        new_direction = cur - prev
        if direction < 0:
            if new_direction < 0:
                pass
            elif new_direction == 0:
                print('Careful! 0-direction (not up, not down) in switch df!')
            elif new_direction > 0:
                raises += 1
                # change direction
        elif direction == 0:
            if new_direction < 0:
                drops += 1
                # change direction
            elif new_direction == 0:
                print('Careful! 0-direction (not up, not down) in switch df!')
            elif new_direction > 0:
                raises += 1
                # change direction
        elif direction > 0:
            if new_direction < 0:
                drops += 1
                # change direction
            elif new_direction == 0:
                print('Careful! 0-direction (not up, not down) in switch df!')
            elif new_direction > 0:
                pass
        direction = new_direction

    switches = {'downs': downs,
                'drops': drops,
                'ups': ups,
                'raises': raises}

    # Compute the quality switch amplitudes, only valid results (NaN at beginning), differences in absolute value.
    amplitudes = ((df_no_zero[col].diff()).dropna()).abs()

    return switches, amplitudes.describe().to_dict()


def buffer_stats(directory, logic, verbose=False):
    # 'mean target buffer (micros)', 'std target buffer (micros)'
    # 'count buffer', 'mean buffer (micros)', 'std buffer (micros)', 'min buffer (micros)', 'max buffer (micros)'
    # '25 percentile buffer (micros)', '50 percentile buffer (micros)', '75 percentile buffer (micros)'
    if logic == vlc.RATE_LOGIC:
        df = vlc.RBLOGIC_BUFLEVEL.load_data_df(directory=directory, verbose=verbose)
    elif logic == vlc.PREDICTIVE_LOGIC:
        df = vlc.PLOGIC_BUFLEVEL.load_data_df(directory=directory, verbose=verbose)
    else:
        raise ValueError('logic is {} but must be in {}'.format(logic, vlc.ADAPTIVE_LOGICS))

    # Find all empty buffer moments and group them by consecutive occurrences. The first is the startup phase.
    curbuf_zero_idxs = df[df['current buffer level (micros)'] == 0].index
    curbuf_consecutive_zero_idxs = np.split(curbuf_zero_idxs, np.where(np.diff(curbuf_zero_idxs) != 1)[0] + 1)

    # Drop the startup from the current buffer level.
    df_curbuf_no_startup = df['current buffer level (micros)'].drop(curbuf_consecutive_zero_idxs[0])

    buffer_target = {'mean': df['target buffer level (micros)'].mean(),
                     'std': df['target buffer level (micros)'].std()}
    buffer_level = df_curbuf_no_startup.describe().to_dict()
    return buffer_target, buffer_level


def playback_time(directory, video: config.DASHVideo, verbose=False):
    # 'playback duration (s)'
    df_start = vlc.START.load_data_df(directory=directory, verbose=verbose)
    df_stop = vlc.STOP.load_data_df(directory=directory, verbose=verbose)
    total_time = (df_stop.iloc[0, 0] - df_start.iloc[0, 0]) * (10 ** -6)
    startup_delay = total_time - video.get_duration_s()
    if startup_delay < 0:
        raise ValueError('the estimated startup delay is negative... {}'.format(startup_delay))
    return total_time, startup_delay


def http_stats(directory, video: config.DASHVideo, verbose=False):
    # 'HTTP GET request count',
    # 'count HTTP responses', 'mean HTTP responses content-length (B)', 'std HTTP responses content-length (B)',
    # 'min HTTP responses content-length (B)', 'max HTTP responses content-length (B)',
    # '25 percentile HTTP responses content-length (B)',
    # '50 percentile HTTP responses content-length (B)',
    # '75 percentile HTTP responses content-length (B)',
    # 'count HTTP responses no init',
    # 'mean HTTP responses no init content-length (B)', 'std HTTP responses no init content-length (B)',
    # 'min HTTP responses no init content-length (B)', 'max HTTP responses no init content-length (B)',
    # '25 percentile HTTP responses no init content-length (B)',
    # '50 percentile HTTP responses no init content-length (B)',
    # '75 percentile HTTP responses no init content-length (B)'
    df_requests = vlc.HTTP_SEND_SOCKET.load_data_df(directory=directory, verbose=verbose)
    df_read = vlc.HTTP_READ.load_data_df(directory=directory, verbose=verbose)
    df_read_no_init = df_read[df_read['content-length (bytes)'] > video.max_init_bytes]

    return df_requests[vlc.MDATE].count(), df_read['content-length (bytes)'].describe().to_dict(), \
           df_read_no_init['content-length (bytes)'].describe().to_dict()


def video_segment_performance(directory, video: config.DASHVideo, verbose=False):
    df_send = vlc.HTTP_SEND_SOCKET.load_data_df(directory=directory, verbose=verbose)
    df_replycode = vlc.HTTP_REPLYCODE.load_data_df(directory=directory, verbose=verbose)
    df_read = vlc.HTTP_READ.load_data_df(directory=directory, verbose=verbose)

    # Eliminate failed requests (last one)
    failed_requests_idxs = df_replycode[np.logical_and(df_replycode['replycode'] != 200,
                                                       df_replycode['replycode'] != 206)].index.values.tolist()
    df_send.drop(failed_requests_idxs, inplace=True)

    # Find and discard init segments in DataFrames
    init_segments_idxs = df_read[df_read['content-length (bytes)'] <= video.max_init_bytes].index.values.tolist()
    df_segment_read = df_read.drop(init_segments_idxs)  # careful: non continuous index
    df_segment_send = df_send.drop(init_segments_idxs)  # careful: non continuous index
    if len(df_segment_read) != len(df_segment_send) != video.segment_amount:
        raise helper.ArgumentLengthMismatchException(
                'segment send/read/amount mismatch: {}/{}/{}'.format(len(df_segment_send), len(df_segment_read),
                                                                     video.segment_amount))

    # Download time from request to read
    df_delta_time_segment_sec = df_segment_read[vlc.MDATE].sub(df_segment_send[vlc.MDATE]).mul(helper.MICRO)

    # Segment performance (segment duration / download time)
    df_segment_performance = video.segment_size / df_delta_time_segment_sec

    return df_segment_performance.describe().to_dict()
