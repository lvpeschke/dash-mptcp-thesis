# Network related settings on the VMs: MPTCP, congestion control, capture tcpdump/tcpprobe.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import contextlib
import os
import subprocess

import sh

import config
import remote

# path to capture_tcpprobe.sh on server VM
CAPTURE_TCPPROBE_SCRIPT = os.path.join(config.VM_PATH_SCRIPTS, 'bash/capture_tcpprobe.sh')
# path to capture_tcpdump.sh on server VM
CAPTURE_TCPDUMP_SCRIPT = os.path.join(config.VM_PATH_SCRIPTS, 'bash/capture_tcpdump.sh')


def set_mptcp(machine, mptcp):
    """Set net.mptcp.mptcp_enabled to 0 or 1 on the specified VM.

    :param machine: name of the Virtualbox VM where the setting is changed
    :param mptcp: 0 or 1 to enable or disable
    :return:
    """
    if mptcp not in [0, 1, '0', '1']:
        raise ValueError('mptcp has to be 0 or 1, not {}'.format(mptcp))

    ssh_set_mptcp_command = 'sysctl -w net.mptcp.mptcp_enabled={}'.format(mptcp)
    return remote.ssh_execute(machine=machine, command=ssh_set_mptcp_command,
                              err_msg='could not set mptcp_enabled={} on {}.'.format(mptcp, machine),
                              sudo=True,
                              python=None,
                              must_succeed=True)


def set_cong_ctrl(machine, cong_ctrl):
    """Set net.ipv4.tcp_congestion_control on the specified VM.

    :param machine: name of the Virtualbox VM where the setting is changed
    :param cong_ctrl: name of the TCP congestion control algorithm
    :return:
    :raise ArgumentException: if cong_algo is not in the list CONG_CTRL_ALGOS
    """
    if cong_ctrl not in config.CONG_CTRL_ALGOS:
        raise ValueError('cong_ctrl {} is not in the list of accepted entries'.format(cong_ctrl))

    ssh_set_cong_ctrl_command = 'sysctl -w net.ipv4.tcp_congestion_control={}'.format(cong_ctrl)
    return remote.ssh_execute(machine=machine, command=ssh_set_cong_ctrl_command,
                              err_msg='could not set tcp_congestion_control={} on {}.'.format(cong_ctrl, machine),
                              sudo=True,
                              python=None,
                              must_succeed=True)


@contextlib.contextmanager
def disable_mptcp(machine, cong_ctrl):
    """Context where net.mptcp.mptcp_enabled is set to 0.
    At the end of the context, mptcp is enabled
    and its associated default congestion control set.

    :param machine: name of the Virtualbox VM where the setting is changed
    :param cong_ctrl: congestion control algorithm to set
    :return: --
    """
    if cong_ctrl not in config.CONG_CTRL_ALGOS:
        raise ValueError('cong_ctrl {} is not in the list of accepted entries'.format(cong_ctrl))

    try:
        # sh.ssh(machine, 'sudo', 'sysctl', '-w', 'net.mptcp.mptcp_enabled=0')
        set_mptcp(machine=machine, mptcp=0)
        set_cong_ctrl(machine=machine, cong_ctrl=cong_ctrl)

        yield  # do stuff

    finally:
        # sh.ssh(machine, 'sudo', 'sysctl', '-w', 'net.mptcp.mptcp_enabled=1')
        set_mptcp(machine=machine, mptcp=1)
        set_cong_ctrl(machine=machine, cong_ctrl=config.CONG_CTRL_MPTCP_DEFAULT)


@contextlib.contextmanager
def capture_tcpprobe(machine, port, folder, logfile):
    """Capture TCPProbe on the machine.

    :param machine: name of the Virtualbox VM where the setting is changed
    :param port: number
    :param folder: where to store the log file
    :param logfile: name of the log file
    :return: --
    """
    pid = None
    try:
        modprobe = remote.ssh_execute(machine=machine,
                                      command='{p} {port} {f}'.format(p=CAPTURE_TCPPROBE_SCRIPT, port=port, f=logfile),
                                      err_msg='could not load tcp_probe on {}'.format(machine),
                                      sudo=False,
                                      must_succeed=True)  # load tcp_probe & redirect output to logfile in bg
        pid = modprobe.stdout.rstrip()  # pid of the process `cat /proc/net/tcpprobe > logfile &`

    except (subprocess.SubprocessError, ValueError, OSError):
        # raise helper.ContextException(__file__, 'modprobe error:', e)
        raise

    else:
        yield  # do stuff

    finally:
        try:
            sh.ssh(machine, 'sudo', 'kill', pid)  # stop the process redirecting tcp_probe's output to logfile
            # /!\ kill modprobe?
        except sh.ErrorReturnCode as error:
            print(error)
            pass
        finally:
            sh.ssh(machine, 'sudo', 'mv', logfile, folder)  # move the logfile to the desired folder on the VM


@contextlib.contextmanager
def capture_tcpdump(machine, folder, logfile, nets, size=config.TCPDUMP_SNAPLEN):
    """Run tcpdump as a background process on the machine until the end of the context.

    :param machine: name of the Virtualbox VM where the setting is changed
    :param folder: where to store the logfile
    :param logfile: name of the logfile
    :param nets: list of subnets to filter
    :param size: bytes of data from each packet rather than the default of 65535 bytes
    :return: --
    """
    kwargs_list = ['net', ' or '.join(nets),
                   '-i', 'any', # all interfaces
                   '-n', # no DNS resolution, faster
                   '-s', str(size), # limit capture size
                   '-w', logfile]
    kwargs = ' '.join(kwargs_list)
    pid = None
    try:
        tcpdump = remote.ssh_execute(machine=machine,
                                     command='{p} {kwargs}'.format(p=CAPTURE_TCPDUMP_SCRIPT, kwargs=kwargs),
                                     err_msg='could not launch tcpdump on {}'.format(machine),
                                     sudo=False,
                                     must_succeed=True)
        pid = tcpdump.stdout.rstrip()

    except (subprocess.SubprocessError, ValueError, OSError):
        raise

    else:
        yield  # do stuff

    finally:
        try:
            sh.ssh(machine, 'sudo', 'kill', pid)
        except sh.ErrorReturnCode as error:
            print(error)
            pass
        finally:
            sh.ssh(machine, 'sudo', 'mv', logfile, folder)  # move the logfile to the desired folder on the VM


def flush_tcp_metrics(machine):
    ssh_ip_tcp_metrics_flush_command = 'ip tcp_metrics flush'
    return remote.ssh_execute(machine=machine, command=ssh_ip_tcp_metrics_flush_command,
                              err_msg='could not flush ip tcp_metrics on {}'.format(machine),
                              sudo=True,
                              python=None,
                              must_succeed=True)


def set_tcp_no_save_metrics(machine, value):
    """Enable or disable tcp_no_save_metrics on the machine.

    :param machine: name of the Virtualbox VM where the setting is changed
    :param value: 1 is to disable, 0 to enable; default 0
    :return:
    """
    if value not in [0, 1, '0', '1']:
        raise ValueError('tcp_save_metrics must be 0 or 1, not {}'.format(value))

    ssh_set_tcp_no_save_metrics_command = 'sysctl -w net.ipv4.tcp_no_metrics_save={}'.format(value)
    return remote.ssh_execute(machine=machine, command=ssh_set_tcp_no_save_metrics_command,
                              err_msg='could not set tcp_no_metrics_save={} on {}'.format(value, machine),
                              sudo=True,
                              python=None,
                              must_succeed=False)


def set_tcp_slow_start_after_idle(machine, value):
    """Enable or disable tcp_slow_start_after_idle on the machine.

    :param machine: name of the Virtualbox VM where the setting is changed
    :param value: 1 is to enable, 0 to disable; default 1
    :return:
    """
    if value not in [0, 1, '0', '1']:
        raise ValueError('tcp_slow_start_after_idle must be 0 or 1, not {}'.format(value))

    ssh_set_tcp_slow_start_after_idle_command = 'sysctl -w net.ipv4.tcp_slow_start_after_idle={}'.format(value)
    return remote.ssh_execute(machine=machine, command=ssh_set_tcp_slow_start_after_idle_command,
                              err_msg='could not set tcp_slow_start_after_idle={} on {}'.format(value, machine),
                              sudo=True,
                              python=None,
                              must_succeed=False)
