# TCPProbe contents.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import glob
import os

import pandas as pd

import helper


class TCPProbe:
    """Class representing a TCP probe file."""
    SSTHRESH_NONE = 2147483647  # ssthresh initialisation value (// -1)
    TCPPROBE_LOGFILE = '{dt}_server_tcpprobe{ext}' # ext is none or .txt

    # based on http://lxr.free-electrons.com/source/net/ipv4/tcp_probe.c?v=3.18
    col_timestamp = 'timestamp'
    col_source = 'saddr:port'
    col_destination = 'daddr:port'
    col_bytes = 'packet bytes'
    col_snd_next = 'snd next'
    col_snd_una = 'snd una'
    col_snd_cwnd = 'snd cwnd'
    col_ssthresh = 'ssthresh'
    col_snd_wnd = 'snd wnd'
    col_srtt = 'srtt'
    col_rcv_wnd = 'rcv wnd'
    col_headers = [col_timestamp,
                   col_source, col_destination,
                   col_bytes,
                   col_snd_next, col_snd_una,
                   col_snd_cwnd, col_ssthresh, col_snd_wnd,
                   col_srtt,
                   col_rcv_wnd]

    @staticmethod
    def load_data_df(directory, verbose=False):
        """
        Load one tcpprobe file into one pandas.DataFrame
        :param directory:
        :param verbose:
        :return:
        """
        with helper.cd(directory):
            string = TCPProbe.TCPPROBE_LOGFILE.format(dt='*', ext='*')
            fs = glob.glob(string)
            if len(fs) <= 0:
                raise FileNotFoundError('could not find {} file at {}'.format(string, directory))
            if len(fs) > 1:
                raise helper.TooManyFilesException('found {} {} files at {}'.format(len(fs), string, directory))

            if not os.stat(fs[0]).st_size > 0:
                raise helper.EmptyFileException('{} at {} is empty'.format(fs[0], directory))

            df = pd.read_csv(filepath_or_buffer=fs[0], sep=' ',
                             header=None, names=TCPProbe.col_headers, dtype={'saddr:port': str, 'daddr:port': str},
                             verbose=verbose)
            if verbose:
                print('* tcpprobe file loaded into df:\n', df.describe(), '\n', df.dtypes)

            # replace uninitialised ssthresh by 0
            if verbose:
                print('Uninitialised ssthresh entries before conversion...',
                      sum(df[TCPProbe.col_ssthresh] == TCPProbe.SSTHRESH_NONE))

            df[TCPProbe.col_ssthresh] = df[TCPProbe.col_ssthresh].apply(lambda x:
                                                                        0 if x == TCPProbe.SSTHRESH_NONE else x)
            if verbose:
                print('Uninitialised ssthresh entries after conversion...',
                      sum(df[TCPProbe.col_ssthresh] == TCPProbe.SSTHRESH_NONE))
            if verbose:
                print('* tcpprobe df after sshthresh_none tracking:\n', df.describe())

            return df
