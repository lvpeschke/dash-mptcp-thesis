#!/usr/bin/env python3.5
#
# Script to assess the simulated network performance with iperf and ping.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import itertools
import sys
import time

import sh

import config
import helper
import remote
import set_network
import vm_settings

# Make sure it's only called on the host.
config.check_machine_is_host()

description = """
Traffic control and performance test for set_network.py.
Test all RTT delays against all bandwidths with
 - ping
 - iperf UDP (bidirectional)
 - iperf TCP (bidirectional)
"""
epilog = """
See also: set_network.py
"""

NETWORK_TEST_LOGFILE = '{datetime}_testnetwork_{cc}'
NETWORK_TEST_TCPPROBE_LOGFILE = '{datetime}_testnetwork_{cc}_tcpprobe'
NETWORK_TEST_SERVER_FOLDER = 'testnetwork'

BANDWIDTHS = [1, 10, 20, 50, 100] # [1, 5, 10, 15, 20, 50, 75]


def test_ping(fd, server, machine=config.CLIENT_SSH, count=10):
    """Do a ping test from the machine to the server.

    :param fd:
    :param server:
    :param machine:
    :param count:
    :return:
    """
    print('ping from {} to {}...'.format(machine, server), end=' ', flush=True)

    def write_down(arg):
        fd.write(arg + '\n')

    sh.ssh(str(machine), 'ping', server, '-q', '-c', str(count), _out=write_down, _err=write_down)

    print('done.', flush=True)


def iperf_server_udp(fd, machine=config.CLIENT_SSH):
    """Start iperf server in UDP mode on the machine.

    :param fd:
    :param machine:
    :return:
    """
    print('UDP iperf server on {}...'.format(machine), end=' ', flush=True)

    def write_down(arg):
        fd.write(arg)

    # iperf server: UDP
    sh.ssh(str(machine), 'iperf', '-s', '-u', _bg=True, _out=write_down, _err=write_down)
    print('done.', flush=True)


def test_iperf_client_udp(server, bandwidth, trans_time=60, machine=config.SERVER_SSH):
    """Start iperf client in UDP mode on the machine

    :param server:
    :param bandwidth: target throughput
    :param trans_time:
    :param machine:
    :return:
    """
    assert helper.check_positive_num(bandwidth)
    assert helper.check_positive_num(trans_time)
    print('UDP iperf client on {client} with {bw}Mbps towards {server}...'.format(client=machine, server=server,
                                                                                  bw=bandwidth), end=' ', flush=True)
    sh.ssh(str(machine), 'iperf', '-u', '-c', str(server),
           '-b', str(bandwidth) + 'm', # target bandwidth (m=Mbits)
           '-t', str(trans_time), # time in seconds to transmit for
           '-r', # do a bidirectional test individually
           '-f', 'm'  # format to report (m=Mbits)
           )
    # -c for csv format
    print('done.', flush=True)


def iperf_server_tcp(fd, machine=config.CLIENT_SSH):
    """Start iperf server in TCP mode on the machine

    :param fd:
    :param machine:
    :return:
    """

    def write_down(arg):
        fd.write(arg)

    print('TCP iperf server on {}...'.format(machine), end=' ', flush=True)
    sh.ssh(str(machine), 'iperf', '-s', _bg=True, _out=write_down, _err=write_down)
    print('done.', flush=True)


def test_iperf_client_tcp(server, trans_time=120, machine=config.SERVER_SSH):
    """Start iperf client in TCP mode on the machine.

    :param server:
    :param trans_time:
    :param machine:
    :return:
    """
    print('TCP iperf client on {machine} towards {server}...'.format(machine=machine, server=server),
          end=' ', flush=True)
    sh.ssh(str(machine), 'iperf', '-c', str(server),
           '-t', str(trans_time),  # time in seconds to transmit for
           '-r',  # do a bidirectional test individually
           '-f', 'm'  # format to report (m=Mbits)
           )
    # -c for csv format
    print('done.', flush=True)


def killall_iperf(machine, must_succeed=True):
    """Kill running instances of iperf on the machine.

    :param machine:
    :param must_succeed: raise error if True and killall returns non-zero exit code (no processes to kill or fail)
    :return: --
    :raise sh.ErrorReturnCode: if must_succedd is True and no processes to kill or fail
    """
    try:
        print('Trying to killall SIGTERM iperf on {}...'.format(machine), end=' ', flush=True)
        sh.ssh(str(machine), 'sudo', 'killall', '-15', 'iperf')  # SIGTERM
        # sh.ssh(machine, 'sudo', 'killall', '-9', 'iperf')  # KILL
    except sh.ErrorReturnCode_1 as error:
        print('done, but failed:')
        print('\tkillall -15 (SIGTERM) failed on {}, returned 1'.format(machine))
        if must_succeed:
            raise error
    else:
        print('done.', flush=True)


def main(delays: list, bandwidths: list, cong_ctrl, directory=config.HOST_PATH_TESTS_NETWORK, verbose=False):
    # assert which_link in config.ALL_LINKS
    which_link = 1
    assert helper.check_positive_num(delays)
    assert helper.check_positive_num(bandwidths)
    assert cong_ctrl in config.CONG_CTRL_ALGOS

    link = config.ALL_TRIPLE_LINKS[which_link]
    datetime = str(time.strftime('%y%m%d-%H%M'))

    # network settings: disable mptcp, set cong_ctrl
    with vm_settings.disable_mptcp(config.CLIENT_SSH, cong_ctrl) and \
            vm_settings.disable_mptcp(config.SERVER_SSH, cong_ctrl):
        with helper.cd(directory):
            # create log file
            with open(NETWORK_TEST_LOGFILE.format(datetime=datetime, cc=cong_ctrl), 'w+') as fd:
                fd.write('NETWORK CONFIGURATION TEST\n')
                fd.write(' * link:\t' + str(which_link) + '\n')
                fd.write(' * delays:\t' + str(delays) + '\n')
                fd.write(' * bandwidths:\t' + str(bandwidths) + '\n')
                fd.write(' * {} congestion control:\t'.format(config.CLIENT_SSH) + cong_ctrl + '\n')
                fd.write(' * {} congestion control:\t'.format(config.SERVER_SSH) + cong_ctrl + '\n')
                fd.write('================================================\n\n')

                # record tcp_probe on the server, any port
                with vm_settings.capture_tcpprobe(machine=config.SERVER_SSH, port=0, folder=NETWORK_TEST_SERVER_FOLDER,
                                                  logfile=NETWORK_TEST_TCPPROBE_LOGFILE.format(datetime=datetime,
                                                                                               cc=cong_ctrl)):
                    # kill any running iperf processes, can fail if none active
                    killall_iperf(config.CLIENT_SSH, must_succeed=False)
                    killall_iperf(config.SERVER_SSH, must_succeed=False)

                    for d, bw in itertools.product(delays, bandwidths):
                        if verbose:
                            print('\n=== Test RTT delay={d}ms and bandwidth={bw}Mbps... ==='.format(d=d, bw=bw))

                        # flush tcp metrics on both VMs
                        vm_settings.flush_tcp_metrics(machine=config.CLIENT_SSH)
                        vm_settings.flush_tcp_metrics(machine=config.SERVER_SSH)

                        # set network
                        set_network.set_link(link=link, delay_rtt=d, rate=bw)
                        if verbose:
                            set_network.configs_all_links()
                        fd.write('\n================================================\n')
                        fd.write('=== RTT delay = {0:3d}ms,   bandwidth = {1:3d}Mbps ===\n'.format(d, bw))
                        fd.write('================================================\n')

                        # perform tests
                        fd.write('=== PING ===\n')
                        test_ping(fd=fd, server=link.server_addr)

                        fd.write('=== IPERF UDP ===\n')
                        iperf_server_udp(fd=fd)
                        time.sleep(5)  # give iperf server time to setup
                        test_iperf_client_udp(server=link.client_addr, bandwidth=bw)  # check with target bw
                        test_iperf_client_udp(server=link.client_addr, bandwidth=2 * bw)  # check with 2ble target bw
                        killall_iperf(config.CLIENT_SSH, must_succeed=True)  # iperf server
                        killall_iperf(config.SERVER_SSH, must_succeed=False)  # iperf client

                        fd.write('=== IPERF TCP ===\n')
                        iperf_server_tcp(fd=fd)
                        time.sleep(5)  # give iperf server time to setup
                        test_iperf_client_tcp(server=link.client_addr)
                        killall_iperf(config.CLIENT_SSH, must_succeed=True)  # iperf server
                        killall_iperf(config.SERVER_SSH, must_succeed=False)  # iperf client

                    # reset network: no qdiscs
                    set_network.stop_all_links()

            # just to make sure
            killall_iperf(config.CLIENT_SSH, must_succeed=False)
            killall_iperf(config.SERVER_SSH, must_succeed=False)

            # get the tcp_probe log file from server and delete it there
            remote.rsync_from_remote(remote_machine=config.SERVER_SSH, remote_path=NETWORK_TEST_SERVER_FOLDER,
                                     local_folder='.', local_directory=directory,
                                     err_msg='could not rsync files from myserver')
            sh.ssh(config.SERVER_SSH, 'sudo', 'rm', '-r', NETWORK_TEST_SERVER_FOLDER + '/*',
                   _in='/dev/null', _out='/dev/null', _err='/dev/null')

    if verbose:
        print('End of testnework, success!')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--delays',
                        type=float,
                        nargs='+',
                        default=config.RTT_DELAYS,
                        help='RTT delays (ms) to test (default={})'.format(config.RTT_DELAYS))
    parser.add_argument('--bandwidths',
                        type=float,
                        nargs='+',
                        default=BANDWIDTHS,
                        help='bandwidths (Mbps) to test (default={})'.format(BANDWIDTHS))
    parser.add_argument('--cong-ctrl',
                        type=str,
                        choices=config.CONG_CTRL_ALGOS,
                        default=config.CONG_CTRL_TCP_DEFAULT,
                        help='choose congestion control algorithm on the VMs (default={})'.format(
                                config.CONG_CTRL_TCP_DEFAULT))
    parser.add_argument('-v', '--version',
                        action='version',
                        version=__file__ + ' version 2nd December 2016')

    args = parser.parse_args()
    print(__file__, 'arguments are...', str(args))

    sys.exit(main(delays=args.delays, bandwidths=args.bandwidths, cong_ctrl=args.cong_ctrl, verbose=True))
