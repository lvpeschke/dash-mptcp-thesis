#!/usr/bin/env python3.5

# Script to launch a TCP calibration.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse  # command line arguments and exception handling
import os
import pprint
import sys
import time

import numpy as np

import calibration
import config
import do_test
import helper
import qoe
import vlc

description = """
Calibrate VLC with TCP: find the required bandwidth to achieve a good quality
trade-off for streaming a video with VLC.
"""
epilog = """
See also: calibration.py, config.py, do_extract_vlc.py, do_test.py, qoe.py, vlc.py
"""

# Log files per test.
QUALITY_ACHIEVED = 'quality-achieved_{bw:.4f}Mbps_{delay}ms_{sr:.4f}percent_{downs}down_{drops}drops'


def main(video, logic, delay, min_Mbps, max_Mbps, samples, repetition, cong_ctrl=config.CONG_CTRL_TCP_DEFAULT,
         directory=config.HOST_PATH_TESTS_CALIBRATION, verbose=False):
    datetime = str(time.strftime('%y%m%d-%H%M'))  # output changes each time script is run

    assert isinstance(video, config.DASHVideo)
    assert video in config.ALL_VIDEOS.values()
    assert logic in vlc.ADAPTIVE_LOGICS
    assert helper.check_positive_num(delay)
    assert min_Mbps > 0
    assert max_Mbps > 0
    assert samples > 0
    assert repetition > 0
    assert cong_ctrl in config.CONG_CTRL_ALGOS

    # list of equally spaced values
    bw_values_Mbps = np.linspace(start=min_Mbps, stop=max_Mbps, num=samples, endpoint=True)
    if verbose:
        print('Search space has', str(len(bw_values_Mbps)), 'items (Mbps)...')
        pprint.pprint(bw_values_Mbps)

    with helper.cd(directory):
        # Create the calibration quality record file to hold all results.
        cqr = calibration.CQR(video=video, logic=logic, delay=delay, cong_ctrl=cong_ctrl)
        cqr_file = cqr.create_file(datetime=datetime)
        if verbose:
            print('Created file...', cqr_file)

        first = 0
        last = len(bw_values_Mbps) - 1
        test_index = (first + last) // 2  # initialise to the middle value

        found_bw_Mbps = None
        found_drops = video.segment_amount + 3

        # Binary search.
        while first <= last:
            test_bw_Mbps = bw_values_Mbps[test_index]

            quality_list = list()
            drops_list = list()

            # Execute test, with repetitions for stability.
            if verbose:
                print('Start new test for {}ms and {}Mbps, {} repetitions...'.format(delay, test_bw_Mbps, repetition))
                print('Remaining values after this test...', str(last - first))
            for _ in range(repetition):
                folder = do_test.main(video=video, logic=logic,
                                      rates=[test_bw_Mbps] * 2,
                                      delays=[delay] * 2,
                                      link=1, mptcp=0,
                                      cong_ctrl=cong_ctrl,
                                      directory=directory, do_extract=True, verbose=verbose)

                with helper.cd(folder):
                    success_rate, bitrate_desc = qoe.bitrate_stats(directory=os.getcwd(), video=video, logic=logic,
                                                                   verbose=verbose)
                    switches, _ = qoe.switch_stats(directory=os.getcwd(), logic=logic, verbose=verbose)
                    # Write a file to mark which quality was achieved.
                    print('==> Found this: {} success rate over {} rep., {} drops'
                          .format(success_rate, bitrate_desc['count'], switches['drops']))
                    open(QUALITY_ACHIEVED.format(bw=test_bw_Mbps, delay=delay, sr=success_rate * 100,
                                                 downs=switches['downs'], drops=switches['drops']), 'x').close()

                # Add the quality result of the test to the record file.
                cqr.write_entry(bw=test_bw_Mbps, delay=delay, success_rate=success_rate * 100,
                                bitrate_count=bitrate_desc['count'],
                                bitrate_mean=bitrate_desc['mean'], bitrate_std=bitrate_desc['std'],
                                bitrate_min=bitrate_desc['min'], bitrate_max=bitrate_desc['max'],
                                bitrate_lower=bitrate_desc['25%'],
                                bitrate_fifty=bitrate_desc['50%'],
                                bitrate_upper=bitrate_desc['75%'],
                                down_switches=switches['downs'], drops=switches['drops'])

                # Remember quality quality for this test setup.
                quality_list.append(success_rate)
                drops_list.append(switches['drops'])

                if video is config.VIDEO_ENVIVIO:  # test video is only 30 sec, give time to have unique timestamps
                    time.sleep(30)

            quality = np.average(quality_list)
            drops = np.average(drops_list)

            if verbose:
                print('Quality {} & drops {}...'.format(quality, drops))

            # Apply calibration rule.
            if quality >= 0.95:
                print('New candidate: quality {} for {}ms and {}Mbps'.format(quality, delay, test_bw_Mbps))
                # Remember that this bandwidth works and is better than the previous one found.
                if (found_bw_Mbps is None) or (test_bw_Mbps < found_bw_Mbps and drops <= found_drops):
                    found_bw_Mbps = test_bw_Mbps
                    found_drops = drops

                # Split search space, keeping [min;test_bw[.
                first = first
                last = test_index - 1
                test_index = (first + last) // 2

            else:  # 0 =< quality < 0.95
                if verbose:
                    print('Not a candidate: quality {} for {}ms and {}Mbps...'.format(quality, delay, test_bw_Mbps))
                # split search space, keeping ]test_bw;max]
                first = test_index + 1
                last = last
                test_index = (first + last) // 2

    return found_bw_Mbps


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('delay',
                        type=float,
                        help='RTT delay on the link (ms)')
    parser.add_argument('samples',
                        type=int,
                        help='number of values to sample between min and max')
    parser.add_argument('--video',
                        type=str,
                        default=config.VIDEO_DEFAULT.id,
                        choices=config.ALL_VIDEOS,
                        help='which video to play (see config.py)')
    parser.add_argument('--logic',
                        type=str,
                        default=vlc.RATE_LOGIC,
                        choices=vlc.ADAPTIVE_LOGICS,
                        help='adaptation logic for VLC (default=rate)')
    parser.add_argument('--cong-ctrl',
                        type=str,
                        choices=config.CONG_CTRL_ALGOS,
                        help='choose congestion control algorithm on the VMs')
    parser.add_argument('--min',
                        type=int,
                        help='minimum bandwidth (Mbps)')
    parser.add_argument('--max',
                        type=int,
                        help='maximum bandwidth (Mbps)')
    parser.add_argument('--repetitions',
                        type=int,
                        default=2,
                        help='number of times a value is tested')
    parser.add_argument('-v', '--version',
                        action='version',
                        version=__file__ + 'version 5th December 2016')
    args = parser.parse_args()

    if args.delay < 0:
        raise ValueError('args.delay < 0')
    if args.min and args.max and args.min >= args.max:
        raise ValueError('args.min >= args.max')
    if args.samples < 1:
        raise ValueError('args.samples < 1')
    if args.repetitions < 1:
        raise ValueError('args.repetitions < 1')

    args.video = config.ALL_VIDEOS.get(args.video)
    args.min = (args.min if args.min else args.video.get_max_bitrate_Mbps())
    args.max = (args.max if args.max else args.video.get_max_bitrate_Mbps() * 5)

    do_test.play_and_kill(link=1, video=args.video, seconds=30, verbose=True)  # avoid swapping
    found_bw = main(video=args.video, logic=args.logic, delay=args.delay,
                    min_Mbps=args.min, max_Mbps=args.max,
                    samples=args.samples, repetition=args.repetitions,
                    cong_ctrl=args.cong_ctrl, verbose=True)
    if helper.check_positive_num(found_bw):
        sys.exit(0)
    else:
        sys.exit(1)
