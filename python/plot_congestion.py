#!/usr/bin/env python3.5
#
# Script to compute, save, and plot congestion results.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import multiprocessing as mp
import os
import sys
import traceback

import matplotlib.pyplot as plt

import config
import helper
import plot_test
import vlc
from graphs import vlc_logic

description = """
Plot results for congestion tests.
"""
epilog = """
See also: all files in graphs/
"""

# -- LaTeX settings -- #
plt.rc('font', **{'family': 'serif', 'serif': ['Palatino']}) # 'size': 13
plt.rc('text', usetex=True)
plt.rc('legend', fontsize='large')
plt.rc('xtick', labelsize='large')
plt.rc('ytick', labelsize='large')
plt.rc('axes', labelsize='x-large')


def find_server_parameters():
    filename = 'parameters_server.log'
    isfile = os.path.isfile(filename)
    if not isfile:
        return None
    else:
        return filename


if __name__ == '__main__':
    print('plot_congestion is __main__')
    parser = argparse.ArgumentParser(description=description, epilog=epilog)
    parser.add_argument('directory',
                        type=str,
                        help='directory/superdirectory')
    parser.add_argument('--video',
                        type=str,
                        default=config.VIDEO_DEFAULT.id,
                        choices=config.ALL_VIDEOS,
                        help='which video was used')
    parser.add_argument('--override',
                        action='store_true',
                        default=False,
                        help='override already existing plot folder(s) or not')
    parser.add_argument('-v', '--version',
                        action='version',
                        version=__file__ + ' version 20 December 2016')
    args = parser.parse_args()

    args.video = config.ALL_VIDEOS.get(args.video)
    folder = config.HOST_FOLDER_PLOTS  # SETUP
    verb = True  # SETUP

    # Find where there are vlclogs (here or subdirs)
    data_dirs = plot_test.find_folders(directory=args.directory, find_file_func=find_server_parameters, verbose=verb)

    # MULTITHREADING
    mp.set_start_method('forkserver')
    with mp.Pool(processes=10) as pool:
        for data_dir in data_dirs:
            if verb:
                print('Trying to plot in: {} (provided)...'.format(data_dir))

            # check if plot folder needs to be created (do not override)
            folder_path = os.path.join(data_dir, folder)
            if os.path.isdir(folder_path):
                plot_dir = folder_path
            else:
                plot_dir = helper.create_folder_in_path(path=data_dir, folder=folder, override=args.override)

            # find all subdirs where there are vlclogs
            client_dirs = plot_test.find_folders(directory=data_dir, find_file_func=vlc.find_vlclog, verbose=verb)

            starts = []
            stops = []
            for client_dir in client_dirs:
                start, stop = plot_test.find_time_limit(directory=client_dir, verbose=verb)  # milliseconds
                starts.append(start)
                stops.append(stop)

            plt.close('all')

            results = [

                pool.apply_async(func=vlc_logic.multiple_rblogic,
                                 args=(client_dirs, folder_path, starts, stops, args.video, args.override, verb)),
            ]

            print('Ordered results using pool.apply_async():')
            for r in results:
                try:
                    print('\t', r.get())
                except Exception as e:
                    traceback.print_tb(e.__traceback__)
                    print(e)
            print()
            plt.close('all')
    # End of mp

    print('End of plot!')
    sys.exit()
