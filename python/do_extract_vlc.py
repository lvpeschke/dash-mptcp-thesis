#!/usr/bin/env python3.5
#
# Scripts to extract VLC metrics.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse  # command line arguments and exception handling
import contextlib  # utilities for with-statement contexts
import os
import re  # regular expression
import sys

import helper
import parameters
import vlc
import logging

description = """
Extract VLC log data to CSV format. Each type of data is put into a separate file.
The input file format contains lines like <TFE, <TEXT>, TIMESTAMP(, VALUE)>,
where TEXT is a string description and VALUE is an optional integer.
The output file format is TIMESTAMP(, VALUE).
Files are created in the same directory as the input log file.

Errors handled (VLC):
- data on the next line due to colliding print commands,
- text appended without a new line (whether TFE entry or not).
"""
epilog = """
See also: play.py, playandlog.py, parameters.py, vmlog.py
"""


def signal_error(l, regex, err_msg):
    """Find regex error in line and print an error message to stdout.
    :param l: line where to search
    :param regex: error to find
    :param err_msg: error message to print to stdout
    :return: --
    """
    if re.search(regex, l):
        logging.warning('Error during vlc extraction: %s', err_msg)


def write_if_full_match(l, fd, regex):
    """Write line to file if it fully matches the regex.
    :param l: line where we look for data
    :param fd: file descriptor of the file to write to
    :param regex: regex to fully match
    :return: True if the line matched regex and was written to file, False if not
    """
    if re.fullmatch(regex, l):
        fd.write(l)
        return True
    else:
        logging.info('No full match between %s and %s', regex, l)
        return False


def write_if_matches_inline(l, fd, partial_regex, full_regex):
    """Write extracted matching entries to file (search for one or two on the provided line).
    :param l: line where to look for entries
    :param fd: file descriptor of the file to write to
    :param partial_regex: regex to match (no EOL, first search)
    :param full_regex: regex to fully match (EOL, second search)
    :return: how many entries have been found and written to file
    """
    counter = 0  # count how many lines are written to file
    match = partial_regex.match(l)
    if match:  # something at the end of the correct entry, already copy the entry and keep looking
        fd.write(match.group(0) + '\n')  # without EOL, have to add one
        counter += 1
        new_match = re.fullmatch(full_regex, re.sub(match.group(0), '', l))
        if new_match:  # check if another TFE entry after the previous one
            fd.write(new_match.group(0))  # with EOL
            counter += 1
        else:  # garbage at the EOL, ignore

            logging.info('FormatError: unexpected content at EOL, has been ignored, >> %s <<, >> %l <<',
                         partial_regex.sub('', l), l)
        logging.info('Wrote %d lines to file (1 or 2 is good)', counter)
    else:
        logging.info('Wrote 0 lines to file (bad)')
    return counter


def find_tfe_description(l, fd, regex, full_regex, err_msg):
    """ Find and remember tfe description (should be called when split detected).

    Print an error message if nothing matched.
    :param l: line where we look for the description
    :param fd: file descriptor of the file to write to
    :param regex: regex to match
    :param full_regex: full regex to match
    :param err_msg: error message to display if no match
    :return: the partial match if any, the lines found afterwards but not written; None otherwise
    """
    # description and data split across lines, correct it
    tfe_description = regex.match(l)
    if tfe_description:
        line_err = tfe_description.group(0)
        logging.info('FormatError: the description could be retrieved (good) >> %s', line_err)
        new_match = re.fullmatch(full_regex, re.sub(line_err, '', l))
        if new_match:  # check if another TFE entry after the previous one
            fd.write(new_match.group(0))  # write it to file immediately
            logging.info('FormatError: retrieved in-between line (good) >> %s', new_match.group(0))
        return line_err
    else:  # unexpected error
        logging.warning('Error during VLC extraction: %s, line: %s', err_msg, l)
        return None


def find_tfe_data(l, line_err, fd, regex):
    """Find tfe data on a different line than the description and write the reconstructed line to file.
    :param l: line where we look for the data
    :param line_err: description with no data that triggered the error
    :param fd: file descriptor of the file to write to
    :param regex: regex to match
    :return: None if the error was corrected, the initial linerror otherwise
    """
    tfe_data = regex.match(l)
    if tfe_data:
        line_err += tfe_data.group(0)  # add the data to the TFE description
        fd.write(line_err + '\n')  # write to entire reconstructed line to file
        # if verbose:
        #     print('FormatError: the data could be retrieved (good)')
        #     print('          >>', line_err)
        logging.info('FormatError: the data could be retrieved (good) >> %s', line_err)
        return None  # data was found, no more current error
    return line_err  # data was not found


def fill_tfefile(vlclog, tfelog, identifier, errs, err_msgs,
                 regex, regex_noeol, regex_part1, regex_part2):
    """Parse the .vlclog and look for entries starting with the identifier.

    If found entries are found, try to match the regex format.
    Correct potential errors if possible. Write extracted lines to .tfe.
    :param vlclog: file to parse
    :param tfelog: file to write to
    :param identifier: identifier to match at the beginning of the line
    :param errs: lines matching errors in the first file
    :param err_msgs: errors corresponding to the errors
    :param regex: full regex to match in the first file
    :param regex_noeol: regex with no EOL
    :param regex_part1: first part of the regex (description)
    :param regex_part2: second part of the regex (data)
    :return: --
    """
    if len(errs) != len(err_msgs):
        logging.warning('Error during VLC extraction: errors and errormessages mismatch in length')

    with open(vlclog, 'r') as vlclog_fd, open(tfelog, 'w') as tfelog_fd:
        line_err = None  # keep track of errors on the previous lines

        for line in vlclog_fd:
            for err, err_msg in zip(errs, err_msgs):  # detect errors and print appropriate error messages
                signal_error(line, err, err_msg)

            if line_err:  # line error (split line) on one of the previous lines, try to treat it
                line_err = find_tfe_data(l=line, line_err=line_err, fd=tfelog_fd, regex=regex_part2)

            if re.search(identifier, line):
                # discard the unnecessary blabla before it
                line = re.sub(vlc.VLC_DEBUG_BEFORE_TFE, '', line)
            if line.startswith(identifier):  # search for entries marked with the identifier
                written = write_if_full_match(l=line, fd=tfelog_fd, regex=regex)  # match the entire line

                if not written:  # an error has occurred
                    # first try to find entire entries with no EOL and treat what is appended
                    i = write_if_matches_inline(l=line, fd=tfelog_fd, partial_regex=regex_noeol, full_regex=regex)
                    # else try to find partial entries
                    if i == 0:
                        line_err = find_tfe_description(l=line, fd=tfelog_fd,
                                                        regex=regex_part1, full_regex=regex,
                                                        err_msg='line not taken!')
            else:
                logging.warning('=== THIS VLCLOG LINE IS WRONG: ===\n\t%s', line)


def extract_data(line, identifier, num_entries, sep, fd):
    """
    If line starts with the identifier and has num_entries separated by sep,
    write the entries to fd (without the identifier).
    :param line: line
    :param identifier: identifier to match
    :param num_entries:
    :param sep:
    :param fd: file descriptor
    :return: --
    """
    if line.startswith(identifier):
        if line.count(sep) == num_entries:  # one more to sep identifier from content
            fd.write(re.sub(identifier, '', line))
        else:
            logging.warning('=== THIS TFE LINE COULD NOT BE USED: ===\n\t%s', line)


def fill_all_csvfiles(tfelog, vlc_info_set, csvlogs):
    """Parse the .tfe and look for entries matching with the identifier.

    If found, copy them to the appropriate .csv file.
    :param tfelog: .tfe file to parse
    :param vlc_info_set:
    :param csvlogs: list of files to hold data found thanks to the keywords
    :return: --
    """
    if len(vlc_info_set) != len(csvlogs):
        raise helper.ArgumentLengthMismatchException('vlc_info_set and csvlogs mismatch in length')

    with open(tfelog, 'r') as tfelog_fd:
        with contextlib.ExitStack() as stack:
            csvlogs_fds = [stack.enter_context(open(csvlog, 'a')) for csvlog in csvlogs]  # open all the csv files

            close_files = stack.pop_all().close

            # test all lines against all identifiers
            for line in tfelog_fd:
                for vlc_info, csvlog_fd in zip([e for e in vlc_info_set], csvlogs_fds):
                    extract_data(line=line, fd=csvlog_fd,
                                 identifier=vlc_info.description, num_entries=vlc_info.col_no, sep=vlc.TFE_SEP)
            close_files()


def do_extract(vlc_info_set: vlc.VLCInfoSet, vlclog, override=False):
    """Extract all VLC entries from the single log files."""

    if not parameters.ClientParameters.check_play_and_log_exit():  # check whether play exited normally
        raise vlc.VLCNoStopInLog('no stop found in vlclog')

    logging.info('Extracting from %s...', vlclog)
    
    # Create .tfe and fill it
    tfelog = helper.create_file_from_file(vlclog, helper.DOT_TFE, override=override)
    fill_tfefile(vlclog=vlclog, tfelog=tfelog, identifier=vlc.TFE_ID,
                 errs=[vlc.ERROR_FATALIO], err_msgs=["a fatal XIO error occurred!"],
                 regex=vlc.TFEREGEX, regex_noeol=vlc.TFEREGEX_NONEWLINE,
                 regex_part1=vlc.TFEREGEX_DESCRIPTION, regex_part2=vlc.TFEREGEX_DATA)
    # Create all .csv
    csvlogs = [e.create_csv_file(tfelog=tfelog, override=override) for e in vlc_info_set]
    fill_all_csvfiles(tfelog=tfelog, csvlogs=csvlogs, vlc_info_set=vlc_info_set)


def main(path, vlc_info_set=vlc.CURRENT_VLCINFOSET, override=False):
    if not isinstance(vlc_info_set, vlc.VLCInfoSet):
        raise TypeError('vlc_info_set is not a VLCInfoSet')

    with helper.cd(path):
        # if verbose:
        #     print('Trying to extract in: {} (provided)...'.format(path))
        logging.info('Trying to extract in: %s (provided)...', path)

        vlclog = vlc.find_vlclog()
        if vlclog:
            try:
                do_extract(vlc_info_set=vlc_info_set, vlclog=vlclog, override=override)
            except (helper.TooManyFilesException, FileExistsError) as err:
                print(err)
                return

        else:
            logging.info('No files found in %s, trying subdirectories...', path)

            for subdir in [subdir for subdir in os.listdir(path)
                           if os.path.isdir(os.path.join(path, subdir))]:
                try:
                    full_subpath = os.path.join(path, subdir)
                    with helper.cd(full_subpath):
                        logging.info('Trying to extract in: %s...', subdir)
                        vlclog = vlc.find_vlclog()
                        if vlclog:
                            do_extract(vlc_info_set=vlc_info_set, vlclog=vlclog, override=override)
                except (helper.TooManyFilesException, FileExistsError) as err:
                    logging.warning(err)
                    pass
    logging.info('Extract_vlc success!')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('path',
                        type=str,
                        help='full path to the directory containing the input file(s)')
    parser.add_argument('--override',
                        action='store_true',
                        default=False,
                        help='override already existing extracted files or not')
    parser.add_argument('-v', '--version',
                        action='version',
                        version=__file__ + ' version 22nd November 2016')
    args = parser.parse_args()

    # if main() returns None, exit code = 0
    sys.exit(main(path=args.path, override=args.override))  # use default VLC findings
