#!/usr/bin/env python3.5
#
# Scripts to log the state of the VMs before and after a test.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse  # command line arguments and exception handling
import os  # os functionality
import sys

import helper
import parameters


description = """
Client and server: log parameters before or after an event.
"""
epilog = """
See also: parameters
"""


def store_before(paramobject: parameters.AbstractParameters, folder, verbose=False):
    """Store state before a tes.

    :param paramobject:
    :param folder:
    :param verbose:
    :return:
    :raise: FileExistsError if folder exists
    """
    os.mkdir(folder)  # create folder, will raise a FileExistsError if exists
    with helper.cd(folder):  # enter newly created folder

        paramobject.create_file()
        paramobject.store_start()

        if verbose:
            print('All parameters stored before...')


def store_after(paramobject: parameters.AbstractParameters, folder, verbose=False):
    """Store state after a test.

    :param paramobject:
    :param folder:
    :param verbose:
    :return:
    :raise: TODO
    """
    with helper.cd(folder):  # enter test folder
        paramobject.store_end()

        if verbose:
            print('All parameters stored after...')


def main(when, folder, verbose=False):
    if folder is None:
        raise ValueError('folder is None')

    paramobject = parameters.get_machine_param_object()

    if when == parameters.LOG_BEFORE:
        store_before(paramobject=paramobject, folder=folder, verbose=verbose)
    elif when == parameters.LOG_AFTER:
        store_after(paramobject=paramobject, folder=folder, verbose=verbose)
    else:
        raise ValueError('when ("{}") is undefined, must be "{before}" or "{after}"'.
                         format(when, before=parameters.LOG_BEFORE, after=parameters.LOG_AFTER))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=description, epilog=epilog)
    parser.add_argument('when',
                        type=str,
                        choices=[parameters.LOG_BEFORE, parameters.LOG_AFTER],
                        help='before or after the video')
    parser.add_argument('folder',
                        type=str,
                        help='name of the log file folder')
    parser.add_argument('-v', '--version',
                        action='version',
                        version='vmlog.py version 22nd November 2016')
    args = parser.parse_args()

    # if main() returns None, exit code = 0
    sys.exit(main(when=args.when, folder=args.folder, verbose=True))
