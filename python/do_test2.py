#!/usr/bin/env python3.5
#
# Script to perform a test with two clients (congestion).
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse  # command line arguments and exception handling
import contextlib  # utilities for with-statement contexts
import os
import re  # regular expression
import sys
import threading
import time

import sh

import config
import do_extract_vlc
import helper
import parameters
import remote
import set_network
import vlc
import vm_settings
from config import DOUBLE_TEST_FOLDER, TCPPROBE_SERVER_LOGFILE
from vm_settings import capture_tcpprobe

# TODO merge with do_test simple

description = """
Stream a video from a server VM to a client VM and record what happens.
 - set the network
 - log parameters on all three machines
"""
epilog = """
See also: config.py, extract_vlc.py, playandlog.py, set_network.py TODO
"""

# TWO_RUNNING_VMS = re.compile('.*\\n.*\\n.*')
THREE_RUNNING_VMS = re.compile('.*\\n.*\\n.*\\n.*')
# REGEX_TESTDESCRIPTION = re.compile('(?<=description )[\w\d. ]+')

PLAY = os.path.join(config.VM_PATH_SCRIPTS, 'python/play.py')  # path to play on the client VM
PLAYANDLOG = os.path.join(config.VM_PATH_SCRIPTS, 'python/playandlog.py')  # path to logplay on the client VM
VMLOG = os.path.join(config.VM_PATH_SCRIPTS, 'python/vmlog.py')  # path to vmlog on client/server VMs


class VMNotRunningException(Exception):
    pass


@contextlib.contextmanager
def triple_network_parameters(rates: list, delays: list, verbose=False):
    """
    Add rate control and delay on the links between the client and server VMs.
    Disable at the end of the context.
    :param rates: rate limits, one for each link
    :param delays: RTT delays to add, one for each link (same delay added on each end of the interface)
    :param verbose: toggle verbose mode
    :return: --
    """
    if len(rates) != len(delays) != len(config.ALL_TRIPLE_LINKS):
        raise ValueError('RTT delays and rates must be lists of {} elements'.format(len(config.ALL_TRIPLE_LINKS)))
    try:
        if verbose:
            print('Set network parameters (rates={}, delays={})...'.format(rates, delays))
        set_network.set_all_triple_links(delays_rtt=delays, rates=rates, verbose=verbose)
        if verbose:
            print('Network parameters set, moving on...')

        yield  # do stuff

    finally:
        # reset network parameters to default
        if verbose:
            print('Reset network parameters...')
        set_network.stop_all_triple_links(verbose=verbose)
        if verbose:
            print('Network parameters reset, moving on...')


def check_three_vms_running(verbose=False):
    """
    Check that the client and server VMs are running on the host.
    :param verbose: toggle verbose mode
    :return: --
    :raise VMNotRunningException: if one or both VMs are not running (based on VirtualBox output)
    """
    running_vms = helper.run_command(command='vboxmanage list runningvms',
                                     err_msg='could not check if VMs are running.',
                                     must_succeed=True).stdout
    if not re.search(config.CLIENT_VM, running_vms):
        raise VMNotRunningException('could not find the CLIENT VM.')
    if not re.search(config.SERVER_VM, running_vms):
        raise VMNotRunningException('could not find the SERVER VM.')
    if not re.search(config.RIVAL_VM, running_vms):
        raise VMNotRunningException('could not find the RIVAL VM.')
    if not re.search(THREE_RUNNING_VMS, running_vms):
        raise VMNotRunningException('are there three running VMs?')
    if verbose:
        print('Client, server, and rival VMs running...')


def play_and_kill(link, video, seconds=30, verbose=False):
    """
    Stream a few seconds of video to load the video files into the server cache
    and avoid swapping on the host machine.
    :param link:
    :param video:
    :param seconds:
    :param verbose:
    :return:
    """
    assert link in config.ALL_TRIPLE_LINKS
    assert isinstance(video, config.DASHVideo)
    assert video in config.ALL_VIDEOS.values()
    assert helper.check_positive_num(seconds)

    if verbose:
        print('Launch play.py on the client with video "{}" inside play_and_kill... '.format(video.name), end='',
              flush=True)
    sh.ssh(config.CLIENT_SSH, config.VM_PYTHON, PLAY, 'timeout', '--time', seconds,
           video.get_url(link_nr=link), video.max_width, video.max_height,
           _in='/dev/null', _out='/dev/null', _err='/dev/null', _bg=True)
    if verbose:
        print('done.')

    if verbose:
        print('Wait for play_and_kill to exit... ', end='', flush=True)
    time.sleep(seconds)
    if verbose:
        print('done.')


class PlayAndLogThread(threading.Thread):
    def __init__(self, machine, link, video, logic, datetime, folder):
        super().__init__()
        self.machine = machine
        self.link = link
        self.video = video
        self.logic = logic
        self.datetime = datetime
        self.folder = folder

    def run(self):
        # actions
        remote.ssh_execute(machine=self.machine,
                           command='{prog} --link {link} --video {video} --logic {logic} --datetime {dt} {f}'
                           .format(prog=PLAYANDLOG, link=self.link, video=self.video.id, logic=self.logic,
                                   dt=self.datetime, f=self.folder),
                           err_msg='could not launch the remote logplay job on myclient',
                           python=config.VM_PYTHON,
                           sudo=False,
                           must_succeed=True)


def main(rates: list, delays: list, link, mptcp, cong_ctrl=None,
         video=config.VIDEO_DEFAULT, logic=vlc.RATE_LOGIC,
         directory=config.HOST_PATH_TESTS_DOUBLE, do_extract=True, verbose=False):
    """
    :return: the path to the newly created folder with all log files
    """
    if verbose:
        print('main() of', __file__, 'executing')

    if verbose:
        print('Checking arguments...')
    config.check_machine_is_host()
    check_three_vms_running(verbose=verbose)

    assert helper.check_positive_num(rates)
    assert helper.check_positive_num(delays)
    assert link in config.ALL_TRIPLE_LINKS
    assert mptcp == 0 or mptcp == 1
    if cong_ctrl:
        assert cong_ctrl in config.CONG_CTRL_ALGOS
    elif mptcp == 0:
        cong_ctrl = config.CONG_CTRL_TCP_DEFAULT
    elif mptcp == 1:
        cong_ctrl = config.CONG_CTRL_MPTCP_DEFAULT
    assert isinstance(video, config.DASHVideo)
    assert video in config.ALL_VIDEOS.values()
    assert logic in vlc.ADAPTIVE_LOGICS
    if verbose:
        print('Checking arguments... done.')

    datetime = str(time.strftime('%y%m%d-%H%M'))  # changes each time script is run

    # set network parameters (tc on host and tcp/mptcp on VMs)
    with triple_network_parameters(rates=rates, delays=delays):
        # set mptcp on/off on the VMs
        vm_settings.set_mptcp(machine=config.CLIENT_SSH, mptcp=mptcp)
        vm_settings.set_mptcp(machine=config.SERVER_SSH, mptcp=mptcp)
        vm_settings.set_mptcp(machine=config.RIVAL_SSH, mptcp=mptcp)

        # set congestion control on the VMs
        vm_settings.set_cong_ctrl(machine=config.CLIENT_SSH, cong_ctrl=cong_ctrl)
        vm_settings.set_cong_ctrl(machine=config.SERVER_SSH, cong_ctrl=cong_ctrl)
        vm_settings.set_cong_ctrl(machine=config.RIVAL_SSH, cong_ctrl=cong_ctrl)

        # flush ip tcp_metrics on the VMs
        vm_settings.flush_tcp_metrics(machine=config.CLIENT_SSH)
        vm_settings.flush_tcp_metrics(machine=config.SERVER_SSH)
        vm_settings.flush_tcp_metrics(machine=config.RIVAL_SSH)

        # create and enter directory where results are stored
        # '{dt}_double_{video.id}_{logic}_{mptcp}_{cong_ctrl}_{rates[0]:.2f}Mbps{delays[0]}ms_{rates[1]:.2f}Mbps{delays[1]}ms'
        folder = DOUBLE_TEST_FOLDER.format(dt=datetime, video=video, logic=logic, mptcp=('m' if mptcp else 't'),
                                           cong_ctrl=cong_ctrl, rates=rates, delays=delays)
        folder_path = helper.create_folder_in_path(path=directory, folder=folder)
        with helper.cd(folder_path):
            # save parameters before on host
            params = parameters.get_machine_param_object()  # should return HostParams()
            assert isinstance(params, parameters.HostParameters)
            params.create_file()
            params.store_start()

            # save parameters before on server
            remote.ssh_execute(machine=config.SERVER_SSH,
                               command='{vmlog} {when} {folder}'
                               .format(vmlog=VMLOG, when=parameters.LOG_BEFORE, folder=folder),
                               err_msg='could not save parameters on myserver before',
                               python=config.VM_PYTHON,
                               sudo=True,
                               must_succeed=True)

            with capture_tcpprobe(machine=config.SERVER_SSH, port=80,
                                  folder=folder, logfile=TCPPROBE_SERVER_LOGFILE.format(dt=datetime)):
                # Launch 2 threads to tell the client and the rival to play video and log all parameters there
                kwargs_client_thread = {'machine': config.CLIENT_SSH,
                                        'command': '{prog} --link {link} --video {video} --logic {logic} --datetime {dt} {f}'.format(
                                                prog=PLAYANDLOG, link=link, video=video.id, logic=logic,
                                                dt=datetime, f=folder),
                                        'err_msg': 'could not launch the remote logplay job on myclient',
                                        'python': config.VM_PYTHON,
                                        'sudo': False,
                                        'must_succeed': True}
                kwargs_rival_thread = kwargs_client_thread.copy()
                kwargs_rival_thread['machine'] = config.RIVAL_SSH
                kwargs_rival_thread['err_msg'] = 'could not launch the remote logplay job on myrival'

                client_thread = threading.Thread(target=remote.ssh_execute, name='Thread-client',
                                                 kwargs=kwargs_client_thread)

                rival_thread = threading.Thread(target=remote.ssh_execute, name='Thread-rival',
                                                kwargs=kwargs_rival_thread)
                client_thread.start()
                rival_thread.start()

                # Wait on the threads.
                client_thread.join()
                rival_thread.join()
                # TODO add timeout to threads and raise Exception if still alive

            # get after parameters from server
            remote.ssh_execute(machine=config.SERVER_SSH,
                               command='{vmlog} {when} {folder}'
                               .format(vmlog=VMLOG, when=parameters.LOG_AFTER, folder=folder),
                               err_msg='could not save and send parameters on myserver afterwards',
                               python=config.VM_PYTHON,
                               sudo=True)

            # save after parameters from host
            params.store_end()

            # get log from server, client, rival
            for machine, client_folder in zip([config.SERVER_SSH, config.CLIENT_SSH, config.RIVAL_SSH],
                                              [None, config.CLIENT_HOSTNAME, config.RIVAL_HOSTNAME]):
                if client_folder:
                    local_folder = os.path.join(folder, client_folder)
                    os.mkdir(client_folder)
                else:
                    local_folder = folder
                remote.rsync_from_remote(remote_machine=machine, remote_path=folder,
                                         local_folder=local_folder, local_directory=directory,
                                         err_msg='could not rsync files from {}'.format(machine))

        # optional: extract data to tfelog and csv files
        if do_extract:
            pass
            do_extract_vlc.main(path=folder_path)

    if verbose:
        print('End of maketest, success!')
        print('Folder path is...', folder_path)

    return folder_path  # DO NOT REMOVE


if __name__ == '__main__':
    print(__file__, 'is __main__')

    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('rates',
                        type=float,
                        nargs=len(config.ALL_TRIPLE_LINKS),
                        help='bandwidth(s) in Mbps')
    parser.add_argument('delays',
                        type=float,
                        nargs=len(config.ALL_TRIPLE_LINKS),
                        help='RTT delay(s) per link in ms')
    parser.add_argument('link',
                        type=int,
                        choices=list(config.ALL_TRIPLE_LINKS),
                        help='choose primary link')
    parser.add_argument('mptcp',
                        type=int,
                        choices=[0, 1],
                        help='toggle mptcp on the VMs')
    parser.add_argument('--video',
                        type=str,
                        default=config.VIDEO_DEFAULT.id,
                        choices=config.ALL_VIDEOS,
                        help='which video to play (see config.py)')
    parser.add_argument('--logic',
                        type=str,
                        default=vlc.RATE_LOGIC,
                        choices=vlc.ADAPTIVE_LOGICS,
                        help='choose adaptation logic for VLC')
    parser.add_argument('--cong-ctrl',
                        type=str,
                        choices=config.CONG_CTRL_ALGOS,
                        help='choose congestion control algorithm on the VMs')
    parser.add_argument('--no-extract',
                        action='store_true',
                        default=False,
                        help='choose not to extract data')
    parser.add_argument('--no-qoe',
                        action='store_true',
                        default=False,
                        help='choose not to extract QoE (requires extract to be True)')
    # parser.add_argument('--test',
    #                     action='store_true',
    #                     default=False,
    #                     help='choose to play a 30 second test video')
    parser.add_argument('--no-play-kill',
                        action='store_true',
                        default=False,
                        help='choose not to play and kill the video before the test')
    parser.add_argument('-v', '--version',
                        action='version',
                        version=__file__ + ' version 21st November 2016')
    args = parser.parse_args()
    print(__file__, 'arguments are...', str(args))

    for rate in args.rates:
        if not helper.check_positive_num(rate):
            raise ValueError('found a rate that is not > 0')
    for delay in args.delays:
        if not helper.check_positive_num(delay):
            raise ValueError('found a delay that is not > 0')

    args.video = config.ALL_VIDEOS.get(args.video)
    if not args.cong_ctrl:
        args.cong_ctrl = config.CONG_CTRL_MPTCP_DEFAULT if args.mptcp == 1 else config.CONG_CTRL_TCP_DEFAULT

    if not args.no_play_kill:
        # Play the video for x seconds to load it into the server's buffer (avoid swapping the server VM on the host).
        play_and_kill(link=args.link, video=args.video, seconds=30, verbose=True)

    new_folder = main(rates=args.rates, delays=args.delays, link=args.link, mptcp=args.mptcp, cong_ctrl=args.cong_ctrl,
                      video=args.video, logic=args.logic,
                      do_extract=(not args.no_extract), # do_qoe=(not args.no_extract and not args.no_qoe),
                      verbose=True)
    if helper.os.path.exists(new_folder):
        sys.exit(0)
    else:
        sys.exit(1)
