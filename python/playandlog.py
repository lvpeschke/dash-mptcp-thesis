#!/usr/bin/env python3.5
#
# Script to play a DASH video with VLC and log the test setup and results.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse  # command line arguments and exception handling
import contextlib
import logging
import os  # os functionality
import subprocess
import sys
import time

import psutil

import config
import helper
import parameters
import play
import vlc

description = """
Play a DASH video with VLC and log parameters.
A folder is created containing a .vlclog and various other log files.
Default: play video {video_id} with VLC logic {logic} over link {link}.
""".format(video_id=config.VIDEO_DEFAULT.id, logic=vlc.RATE_LOGIC, link=config.LINK_DEFAULT)
epilog = """
See also: config.py, play.py, parameters.py, vlc.py
"""


@contextlib.contextmanager
def tcpdump(logfile, nets, size=config.TCPDUMP_SNAPLEN):
    """
    Run tcpdump as a background process until the end of the context.
    :param logfile: name of the logfile
    :param nets: list of subnets to filter
    :param size: bytes of data from each packet rather than the default of 65535 bytes
    :return: --
    :raise: ContextException if any subprocess fails
    """
    command_list = ['sudo', '/usr/sbin/tcpdump',
                    'net', ' or '.join(nets), # selected network addr
                    '-i', 'any', # all interfaces
                    '-n', # no DNS resolution, faster
                    '-s', str(size), # limit capture size
                    '-w', logfile]
    logging.info('tcpdump command... %s', ' '.join(command_list))
    p1 = None
    try:
        # Launch a background process.
        p1 = subprocess.Popen(command_list)
        time.sleep(2)  # Give the process time to launch.
        logging.info('tcpdump pid... %d', p1.pid)

    except (ValueError, OSError):
        raise

    else:
        yield  # do stuff

    finally:
        # Terminate the background process gracefully.
        if p1 is None:
            raise ValueError('tcpdump process is None')
        elif psutil.pid_exists(p1.pid):
            command = 'sudo kill {pid}'.format(pid=p1.pid)
            p2 = helper.run_command(command=command,
                                    err_msg='kill: could not kill tcpdump pid={}'.format(p1.pid),
                                    must_succeed=True)
            logging.info('kill tcpdump returned... %d %d %s', p1.pid, p2.returncode, str(p2.stdout))
        else:
            logging.info('killed tcpdump in some other way...')


def main(folder, datetime, url, width, height, logic, logfile, segment_time):
    """ Play a DASH video with VLC and log parameters to file.
    A new folder is created.
    :param folder: where to store the log files
    :param datetime: timestamp
    :param url: video url
    :param width: --adaptive-width option value
    :param height: --adaptive-height option value
    :param logic: --adaptive-logic option value
    :param logfile: name of the main logfile
    :param segment_time: length of the video segments (seconds)
    :return: --
    """
    assert helper.check_positive_num(width)
    assert helper.check_positive_num(height)
    assert helper.check_positive_num(segment_time)
    assert logic in vlc.ADAPTIVE_LOGICS

    logging.info('Start of playandlog main, arguments checked...')

    os.mkdir(folder)  # can raise a FileExistsError
    with helper.cd(folder):
        # Create the log file and save all parameters before playback.
        # params = parameters.ClientParameters()
        params = parameters.get_machine_param_object()
        assert isinstance(params, parameters.ClientParameters)
        params.create_file()
        params.store_start()
        params.store_vlc_before(url=url, segment_time=segment_time, width=width, height=height, logic=logic)

        # Record network traffic and play the video.
        pcapfile = config.TCPDUMP_LOGFILE.format(dt=datetime, machine=os.uname()[1], ext=helper.DOT_PCAP)
        # pcapfile = '{dt}_{machine}_any{ext}'.format(dt=datetime, machine=os.uname()[1], ext=common.DOT_PCAP)
        with tcpdump(logfile=pcapfile, nets=[v.net for v in config.ALL_TRIPLE_LINKS.values()]):
            play.run_vlc_log(datetime=datetime,
                             url=url, width=width, height=height, logic=logic, cvlc=True,
                             logfile=logfile, verbosity=0)

        # Save all parameters after playback.
        params.store_vlc_after()
        params.store_end()
        logging.info('Video played and parameters stored on client...')

    logging.info('End of playandlog main, success!')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('folder',
                        type=str,
                        help='name of the log file folder')
    parser.add_argument('--datetime',
                        type=str,
                        default=str(time.strftime('%y%m%d-%H%M')),
                        help='timestamp')
    parser.add_argument('--video',
                        type=str,
                        default=config.VIDEO_DEFAULT.id,
                        choices=config.ALL_VIDEOS,
                        help='which video to play')
    parser.add_argument('--logic',
                        type=str,
                        default=vlc.LOGIC_DEFAULT,
                        choices=vlc.ADAPTIVE_LOGICS,
                        help='--adaptive-logic for VLC')
    parser.add_argument('--link',
                        type=int,
                        choices=config.ALL_TRIPLE_LINKS.keys(),
                        default=config.LINK_DEFAULT,
                        help='choose primary link')
    parser.add_argument('-v', '--version',
                        action='version',
                        version=__file__ + ' version 21 December 2016')

    args = parser.parse_args()
    logging.info('Call from shell... %s arguments are... %s', __file__, str(args))

    args.video = config.ALL_VIDEOS.get(args.video)
    sys.exit(main(folder=args.folder,
                  datetime=args.datetime,
                  url=args.video.get_url(args.link),
                  width=args.video.max_width,
                  height=args.video.max_height,
                  logic=args.logic,
                  logfile=args.video.id,
                  segment_time=args.video.segment_size))
