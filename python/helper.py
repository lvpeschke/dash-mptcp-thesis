# Collection of useful functions, exceptions, and constants.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import contextlib
import os
import shutil
import subprocess
import logging

ADDITION_SEP = '_'

MICRO = 10 ** -6
BYTES_TO_BITS = 8

# File formats
DOT_MPD = '.mpd'
DOT_VLCLOG = '.vlclog'
DOT_TFE = '.tfe'
DOT_CSV = '.csv'
DOT_TSHARK = '.tshark'
DOT_PCAP = '.pcap'
DOT_PNG = '.png'
DOT_PDF = '.pdf'
DOT_SVG = '.svg'


class ArgumentLengthMismatchException(Exception):
    pass


class EmptyFileException(OSError):
    pass


class TooManyFilesException(OSError):
    pass


@contextlib.contextmanager
def cd(nowdir):
    """Enter directory and exit cleanly.

    Context to perform actions in a specific directory,
    leaving it afterwards or upon error.
    From https://stackoverflow.com/questions/431684/how-do-i-cd-in-python
    :param nowdir: directory to enter
    """
    prevdir = os.getcwd()
    try:
        os.chdir(os.path.abspath(os.path.expanduser(nowdir)))
        logging.info('Enter path... %s', nowdir)

        yield  # do what comes inside 'with' statement

    finally:
        os.chdir(prevdir)
        logging.info('Return to path... %s', prevdir)


def create_folder_in_path(path, folder, override=False):
    """Create a new folder at the specified path.

    :param path: full path from /
    :param folder: new folder name
    :param override: if true, override an existing folder (delete + create)
    :return: the full path to the new folder
    :raise ArgumentException: if the path does not exist
    :raise OSError: if the folder already exists in the path and override is False
    """
    if not os.path.isdir(path):
        raise FileNotFoundError('path {path} does not exist'.format(path=path))

    folder_path = os.path.join(path, folder)
    if os.path.isdir(folder_path) and override:
        shutil.rmtree(folder_path)
    os.makedirs(folder_path, exist_ok=False)  # will raise an exception if folder already exists
    logging.info('New folder... %s', folder_path)
    return folder_path


def create_file_from_file(filename, dotextension, addition=None, override=False):
    """Create an empty file having the name basename(filename)[_addition].extension.

    :param filename: initial file
    :param dotextension: new extension for the second file (including dot)
    :param addition: to add to the file name
    :param override: if true, override an existing file
    :return: the new filename with extension
    :raise FileExistsError: if the file already exists and override is False
    """
    name, _ = os.path.splitext(os.path.basename(filename))
    if addition:
        newfile = name + ADDITION_SEP + addition + dotextension
    else:
        newfile = name + dotextension
    try:
        with open(newfile, 'x'):   # create file
            logging.info('New file... %s', newfile)
    except FileExistsError as err:
        if override:
            os.remove(newfile)
            with open(newfile, 'x'):
                logging.info('Overridden file... %s', newfile)
        else:
            raise err

    return newfile


def create_file_with_override(filename, newline=None, override=False):
    """Safely create a file.

    :param filename: new filename
    :param newline: type of newline
    :param override: if True, truncate an existing file
    :return: the input filename
    :raise FileExistError: if override=False and filename exists
    """
    if override:
        with open(filename, 'w', newline=newline):  # create file, truncate if exists
            logging.info('New (possibly overridden) file... %s', filename)
    else:
        with open(filename, 'x', newline=newline):  # create file, fail if exists
            logging.info('New file... %s', filename)
    return filename


def run_command(command, err_msg, must_succeed=True, stdout=subprocess.PIPE):
    """
    Run a shell command with the options shell=True, universal_newlines=True, stderr=subprocess.stdout,
    check=True. Creates a try/catch environment and prints the error message in case of problems.
    See https://docs.python.org/3.5/library/subprocess.html#subprocess.run.
    :param command: the shell command to run (list or string)
    :param err_msg: the error message to print if the return code is != 0
    :param stdout:
    :param must_succeed: raise error if the return code is != 0, catch otherwise
    :return: a CompletedProcess instance (holds: args, returncode, stdout, stderr) upon success,
             a CalledProcessError (holds: returncode, cmd, output, stdout, stderr) upon failure
    """
    try:
        output = subprocess.run(args=command,
                                stdout=stdout, stderr=subprocess.STDOUT,
                                shell=True, universal_newlines=True, check=True)
        return output
    except subprocess.CalledProcessError as error:  # raised if returncode != 0
        logging.error(err_msg)
        if must_succeed:
            raise error
        else:
            logging.warning('%s %d %s', error.cmd, error.returncode, error.stderr)
            return error


def convert_to_num(number):
    """Convert a str or list of str to a int/float or a list of int/float.

    :param number: str or list of str representing numbers
    :return: an int or a float, or a list of int or float
    :raise ValueError: if the input is neither an int nor a float
    """
    try:
        if isinstance(number, list):
            return [int(n) for n in number]
        else:
            return int(number)
    except ValueError:
        if isinstance(number, list):
            return [float(n) for n in number]
        else:
            return float(number)


def check_positive_num(number):
    """Check if the input is a positive int or float, or list of int/float.

    :param number: the input to check
    :return: True if all of the input is a positive number
    """
    try:
        isnumber = convert_to_num(number)
        if isinstance(isnumber, list):
            return all(i >= 0 and i != float('nan') and i != float('inf') for i in isnumber)
        else:
            return isnumber >= 0 and isnumber != float('nan') and isnumber != float('inf')
    except ValueError:
        return False


def p2f(x):
    """Convert percentage string (no % symbol) to float.
    :param x:
    :return:
    """
    # return float(x.strip('%')) / 100
    return float(x) / 100
