#!/usr/bin/env python3.5
#
# Script to plot bandwidth estimations of different setups.
#
# Copyright (C) 2017  Lena Victoria Peschke (lena.peschke@student.uclouvain.be)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import itertools
import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import config
import helper
import vlc
from calibration import CQR

description = """
Plot TCP calibration results to file.
"""
epilog = """
See also: calibration.py, config.py, do_calibration.py, vlc.py
"""

# -- LaTeX settings -- #
plt.rc('font', **{'family': 'serif', 'serif': ['Palatino']}) # 'size': 13
plt.rc('text', usetex=True)
plt.rc('legend', fontsize='large')
plt.rc('xtick', labelsize='large')
plt.rc('ytick', labelsize='large')
plt.rc('axes', labelsize='x-large')


def plot_calibration_results(data_df0: pd.DataFrame, plot_dir, filename,
                             override=False, verbose=False):
    assert isinstance(data_df0, pd.DataFrame)
    assert os.path.isdir(plot_dir)

    unique_bws = data_df0[CQR.col_bw].unique()
    mean_bitrates_success_rate = [data_df0[data_df0[CQR.col_bw] == bw][CQR.col_bitrate_success_rate].mean()
                                  for bw in unique_bws]
    mean_drops = [data_df0[data_df0[CQR.col_bw] == bw][CQR.col_drops].mean() for bw in unique_bws]
    mean_downs = [data_df0[data_df0[CQR.col_bw] == bw][CQR.col_downs].mean() for bw in unique_bws]

    data = {r'bandwidth': unique_bws,
            r'bitrate success rate': mean_bitrates_success_rate,
            r'quality drops': mean_drops,
            r'quality down switches': mean_downs}

    df = pd.DataFrame(data=data).sort_values([r'bandwidth'])

    if verbose:
        print('* CSV file(s) loaded into df:\n', data_df0, '\n')
        print('* Result df:\n', df, '\n')

    with helper.cd(plot_dir):
        # Check if files exist
        file = filename + helper.DOT_CSV
        figure = filename + helper.DOT_PDF
        # if not override and helper.check_is_files([file, figure]):
        if not override and any((os.path.isfile(f)) for f in [file, figure]):
            print('calibration file(s) exist(s) and will not be overridden...')
            return

        # Print csv to file
        df.to_csv(path_or_buf=file)

        # Plot pdf to file
        plt.figure()
        ax1 = plt.gca()  # gca stands for 'get current axis'
        ax2 = ax1.twinx()
        ax1.set_clip_on(False)
        ax2.set_clip_on(False)

        bitrate_color = '#0064B4'  # '#1796E6'  # blue
        drops_color = '#B55100'  # '#E56717'  # orange
        # ninety_five = 'red'

        ax1.axhline(y=0.95, color=bitrate_color, linestyle='--')
        # ax1.axhline(y=0.95, color=ninety_five, linestyle='--')
        lns1 = ax1.scatter(x=df[r'bandwidth'], y=df[r'bitrate success rate'],
                           # label=r'max. bitrate',
                           marker='o', color=bitrate_color, edgecolor=bitrate_color, clip_on=False)
        lns2 = ax2.scatter(x=df[r'bandwidth'], y=df[r'quality drops'],
                           # label=r'quality drops',
                           marker='x', color=drops_color, edgecolor=drops_color, clip_on=False)

        lns = [lns1, lns2]
        labels = [l.get_label() for l in lns]
        ax1.legend(lns, labels, loc='best', frameon=True)
        ax1.set_xlabel(r'configured bandwidth (Mbps)')
        ax1.set_ylabel(r'success rate', color=bitrate_color)
        ax2.set_ylabel(r'amount', color=drops_color)

        ax1.set_ylim(0, 1)
        ax1.set_yticks(np.arange(0, 1.05, 0.05))
        for tl in ax1.get_yticklabels():
            tl.set_color(bitrate_color)
        ax2.set_ylim(0, 100)
        ax2.set_yticks(np.arange(0, 101, 10))
        for tl in ax2.get_yticklabels():
            tl.set_color(drops_color)

        ax1.set_xlim(xmin=0)
        ax2.set_xlim(xmin=0)
        if int(df[r'bandwidth'].max()) < 15:
            ax1.set_xlim(xmax=15)
            ax2.set_xlim(xmax=15)
            ax1.set_xticks(np.arange(0, 15 + 1, 1))
            ax2.set_xticks(np.arange(0, 15 + 1, 1))
        elif int(df[r'bandwidth'].max()) < 100:
            ax1.set_xlim(xmax=100)
            ax2.set_xlim(xmax=100)
            ax1.set_xticks(np.arange(0, 100 + 1, 5))
            ax2.set_xticks(np.arange(0, 100 + 1, 5))
        elif int(df[r'bandwidth'].max()) < 220:
            ax1.set_xlim(xmax=220)
            ax2.set_xlim(xmax=220)
            ax1.set_xticks(np.arange(0, 220 + 1, 20))
            ax2.set_xticks(np.arange(0, 220 + 1, 20))

        ax1.grid()
        ax2.grid()

        plt.savefig(figure, format='pdf', bbox_inches='tight', transparent=True)
        plt.close()


def main(directory, videos, logics, cong_ctrls, delays, folder=config.HOST_FOLDER_PLOTS,
         override=False, verbose=False):

    assert (isinstance(video, config.DASHVideo) for video in videos)
    assert (video in config.ALL_VIDEOS.values() for video in videos)
    assert (logic in vlc.ADAPTIVE_LOGICS for logic in logics)
    assert (cong_ctrl in config.CONG_CTRL_ALGOS for cong_ctrl in cong_ctrls)

    with helper.cd(directory):
        # check if plot_dir needs to be created (do not override)
        if os.path.isdir(folder):
            plot_dir = folder
        else:
            plot_dir = helper.create_folder_in_path(path=os.getcwd(), folder=folder, override=override)

        # try all possible combinations of video, logic, cong_ctrl, delay (silent fail)
        for video, logic, cong_ctrl, delay in itertools.product(videos, logics, cong_ctrls, delays):
            # Per RTT delay, create graph with:
            # - configured bandwidth
            # - iperf bandwidth (nope, would require more measurements)
            # - max. bitrate success rate
            # - number of drops

            # load cqr data if available
            cqr_df = CQR.load_data_df(directory=directory, video=video, logic=logic,
                                      delay=delay, cong_ctrl=cong_ctrl, verbose=verbose)
            if cqr_df is None:
                continue

            assert isinstance(cqr_df, pd.DataFrame)

            filename = 'calibration_{video}_{logic}_{cc}_{delay}'.format(video=video.id, logic=logic,
                                                                         cc=cong_ctrl, delay=delay)
            plot_calibration_results(data_df0=cqr_df, plot_dir=plot_dir, filename=filename, override=override,
                                     verbose=verbose)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('directory',
                        type=str,
                        # default=config.HOST_PATH_TESTS_CALIBRATION,
                        help='directory where to find the files')
    parser.add_argument('--folder',
                        type=str,
                        default=config.HOST_FOLDER_PLOTS,
                        help='folder for the results')
    parser.add_argument('--videos',
                        nargs='+',
                        default=config.ALL_VIDEOS.keys(),
                        help='which video was calibrated (see config.py)')
    parser.add_argument('--logics',
                        nargs='+',
                        default=vlc.ADAPTIVE_LOGICS,
                        help='adaptation logic for VLC')
    parser.add_argument('--delays',
                        nargs='+',
                        default=config.RTT_DELAYS,
                        help='RTT delay on the link (ms)')
    parser.add_argument('--cong-ctrls',
                        nargs='+',
                        default=config.CONG_CTRL_ALGOS,
                        help='choose congestion control algorithm on the VMs')
    parser.add_argument('--override',
                        action='store_true',
                        default=False,
                        help='override already existing result folder(s)')
    parser.add_argument('-v', '--version',
                        action='version',
                        version=__file__ + 'version 24th November 2016')

    args = parser.parse_args()

    args.videos = [config.ALL_VIDEOS.get(v) for v in args.videos]
    sys.exit(main(directory=args.directory, folder=args.folder,
                  videos=args.videos, logics=args.logics,
                  delays=args.delays, cong_ctrls=args.cong_ctrls,
                  override=args.override, verbose=True))
