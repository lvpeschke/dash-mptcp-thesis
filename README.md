# Combining DASH and Multipath TCP for video streaming: scripts #

The scripts in this repository belong to the Master's thesis Combining DASH and Multipath TCP for video streaming.


## Abstract ##
HTTP adaptive streaming (HAS) is widely deployed on today’s Internet.
It tailors a video streaming session to the end-user’s needs by dynamically adapting the quality to the network conditions.
Dynamic Adaptive Streaming over HTTP (DASH) is a recent standard attempting to unify similar approaches to HAS.
Multipath TCP (MPTCP) is an extension of TCP that is able to bundle several connections and present it as if it were one to the application layer.
It could improve video streaming when multiple network interfaces are available, such as with mobile devices.
How transparently replacing TCP with MPTCP in DASH video sessions changes the streaming performance has not yet been clearly assessed.
This thesis analyses how combining DASH with MPTCP affects the quality of experience (QoE) perceived by the end-user under equivalent network conditions as TCP.
While not providing statistically relevant results, it gives insights into factors improving or diminishing performance.
Several network setups with two paths between the client and the server are compared against each other and TCP.
In ideal network conditions, without any loss or congestion, MPTCP mostly performs similar to TCP.
This can be explained through application and transport layer analysis. When two competing clients are present on the network, MPTCP tends to offers less stability and a lower network utilisation than TCP.

## People and institutions ##

* Author: Lena Victoria Peschke
* Supervisor: Prof. Olivier Bonaventure
* Faculty: [Ecole Polytechnique de Louvain](https://www.uclouvain.be/epl)
* University: [Université Catholique de Louvain](https://www.uclouvain.be/)


## Requirements ##

The code requires a host machine with two VMs installed with [VirtualBox](https://www.virtualbox.org/).
The client VM has VLC installed from [https://github.com/lvpeschke/vlc](https://github.com/lvpeschke/vlc).
A second client VM can be added.
and the server VM runs the [Apache2](https://httpd.apache.org/) and can serve the [Big Buck Bunny video dataset](http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/BigBuckBunny/).

All three machine need [Python 3.5](https://www.python.org).


## Where to execute which ##

### Python ###

| Host | Client VM | Server VM |
|---|---|---|
| set_network.py | vmlog.py | vmlog.py |
| test_network.py | play.py | |
| do_*.py | playandlog.py | |
| execute_*.py | | |
| plot_*.py | | |
| vhf_study.py | | |


### Bash ###

| Host | Client VM | Server VM |
|---|---|---|
| virtualintfs.sh	| routing_policy.sh | routing_policy.sh	|
| startvm.sh |  | capture_tcpdump.sh |
| stopvm.sh | | capture_tcpprobe.sh |

## License ##

The code in this repository is licensed under GPLv3.